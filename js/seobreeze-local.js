jQuery(document).ready(function($) {
	$('#use_multiple_locations').click( function() {
		if( $(this).is(':checked') ) {
			
			$('#use_multiple_locations').attr('disabled', true);
			$('#single-location-settings').slideUp( function() {
				$('#multiple-locations-settings').slideDown();
				$('#opening-hours-container').slideUp( function() {
					$('#use_multiple_locations').removeAttr('disabled');
				});
				$('#sl-settings').slideDown(); 
			});
		}
		else {
			
			$('#use_multiple_locations').attr('disabled', true);
			$('#multiple-locations-settings').slideUp( function() {
				$('#single-location-settings').slideDown();
				$('#opening-hours-container').slideDown(  function() {
					$('#use_multiple_locations').removeAttr('disabled');
				});
				$('#sl-settings').slideUp(); 
			});
		}
	});
        /*Upoad Image for local seo start*/
        $(document).on('click', ".upload_que_img",function(e){
        var que_id = $(this).attr('id');

        e.preventDefault();

          //Extend the wp.media object
          custom_uploader = wp.media.frames.file_frame = wp.media({
              title: 'Select Image',
              button: {
                  text: 'Select Image'
              },
              multiple: false
          });

          //When a file is selected, grab the URL and set it as the text field's value
          custom_uploader.on('select', function()
          {
                  attachment 			=  custom_uploader.state().get('selection').first().toJSON();
                  var id 				=  attachment.id;
                  var url 			=  attachment.url;
            console.log(url);
            $("#media_src_"+que_id).attr("src",url);
            $("#media_url_"+que_id).val(url);
          });

          //Open the uploader dialog
          custom_uploader.open();

      });

      //Remove question image
      $(document).on('click', ".remove_que_img",function(e){
        var que_id = $(this).attr('id');
        if (confirm('Do you want to remove this question image?')) {
            $("#media_src_"+que_id).attr("src","");
            $("#media_url_"+que_id).val("");
          }
      });
      /*Upoad Image for local seo End*/
	/*Opening hours slide code start*/
	$('#hide_opening_hours').click( function() {
		if( $(this).is(':checked') ) {
			$('#opening-hours-inner').slideUp();
		}
		else {
			$('#opening-hours-inner').slideDown();
		}
	});
	/*Opening hours slide code end*/
	/*Change display hours if 24 hours format is enabled*/
	$('#opening_hours_24h').click( function() {
		$('#opening-hours-container select').each(function() {
			$(this).find('option').each(function() {
				if($('#opening_hours_24h').is(':checked')) {
					// Use 24 hour
					if($(this).val() != 'closed') {
						$(this).text($(this).val());
					}
				} else {
					// Use 12 hour
					if($(this).val() != 'closed') {
						// Split the string between hours and minutes
						var time = $(this).val().split(':');

						// use parseInt to remove leading zeroes.
						var hour = parseInt(time[0]);
						var minutes = time[1];
						var suffix = 'AM';

						// if the hours number is greater than 12, subtract 12.
						if(hour >= 12) {
							if(hour > 12) {
								hour = hour - 12;
							}
							suffix = 'PM';
						}
						if(hour == 0) {
							hour = 12;
						}

						$(this).text(hour + ':' + minutes + ' ' + suffix);
					}
				}
			});
		})
	});
	/*End*/
	/*Enable multiple opening*/
	$('#multiple_opening_hours').click( function() {
		if( $(this).is(':checked') ) {
                    
			$('.opening-hours .opening-hours-second').slideDown();
		}
		else {
                    
                        $('.opening-hours .opening-hours-second').slideUp();
		}
	});
        
        $('#multiple_opening_hours').click( function() {
		if( $(this).is(':checked') ) {
                    
			$('.opening-hours .opening-hours-second').slideDown();
		}
		else {
                    
                        $('.opening-hours .opening-hours-second').slideUp();
		}
	});
        
        $('#seobreeze_multiple_opening_hours').click( function() {
		if( $(this).is(':checked') ) {
                    
			$('.opening-hours .opening-hours-second').slideDown();
		}
		else {
                    
                        $('.opening-hours .opening-hours-second').slideUp();
		}
	});
	/*End*/
	
});
