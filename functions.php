<?php
if( !function_exists('get_local_business_types')){
    function get_local_business_types() {
            $location_busines =  array(
                                    'Organization'                => 'Organization',
                                    'Corporation'                 => 'Corporation',
                                    'GovernmentOrganization'      => 'Government Organization',
                                    'NGO'                         => 'NGO',
                                    'EducationalOrganization'     => 'Educational Organization',
                                    'CollegeOrUniversity'         => '&mdash; College or University',
                                    'ElementarySchool'            => '&mdash; Elementary School',
                                    'HighSchool'                  => '&mdash; High School',
                                    'MiddleSchool'                => '&mdash; Middle School',
                                    'Preschool'                   => '&mdash; Preschool',
                                    'School'                      => '&mdash; School',
                                    'PerformingGroup'             => 'Performing Group',
                                    'DanceGroup'                  => '&mdash; Dance Group',
                                    'MusicGroup'                  => '&mdash; Music Group',
                                    'TheaterGroup'                => '&mdash; Theater Group',
                                    'SportsTeam'                  => 'Sports Team',
                                    'LocalBusiness'               => 'Local Business',
                                    'AnimalShelter'               => 'Animal Shelter',
                                    'AutomotiveBusiness'          => 'Automotive Business',
                                    'AutoBodyShop'                => '&mdash; Auto Body Shop',
                                    'AutoDealer'                  => '&mdash; Auto Dealer',
                                    'AutoPartsStore'              => '&mdash; Auto Parts Store',
                                    'AutoRental'                  => '&mdash; Auto Rental',
                                    'AutoRepair'                  => '&mdash; Auto Repair',
                                    'AutoWash'                    => '&mdash; Auto Wash',
                                    'GasStation'                  => '&mdash; Gas Station',
                                    'MotorcycleDealer'            => '&mdash; Motorcycle Dealer',
                                    'MotorcycleRepair'            => '&mdash; Motorcycle Repair',
                                    'ChildCare'                   => 'Child Care',
                                    'DryCleaningOrLaundry'        => 'Dry Cleaning or Laundry',
                                    'EmergencyService'            => 'Emergency Service',
                                    'FireStation'                 => '&mdash; Fire Station',
                                    'Hospital'                    => '&mdash; Hospital',
                                    'PoliceStation'               => '&mdash; Police Station',
                                    'EmploymentAgency'            => 'Employment Agency',
                                    'EntertainmentBusiness'       => 'Entertainment Business',
                                    'AdultEntertainment'          => '&mdash; Adult Entertainment',
                                    'AmusementPark'               => '&mdash; Amusement Park',
                                    'ArtGallery'                  => '&mdash; Art Gallery',
                                    'Casino'                      => '&mdash; Casino',
                                    'ComedyClub'                  => '&mdash; Comedy Club',
                                    'MovieTheater'                => '&mdash; Movie Theater',
                                    'NightClub'                   => '&mdash; Night Club',
                                    'FinancialService'            => 'Financial Service',
                                    'AccountingService'           => '&mdash; Accounting Service',
                                    'AutomatedTeller'             => '&mdash; Automated Teller',
                                    'BankOrCreditUnion'           => '&mdash; Bank or Credit Union',
                                    'InsuranceAgency'             => '&mdash; Insurance Agency',
                                    'FoodEstablishment'           => 'Food Establishment',
                                    'Bakery'                      => '&mdash; Bakery',
                                    'BarOrPub'                    => '&mdash; Bar or Pub',
                                    'Brewery'                     => '&mdash; Brewery',
                                    'CafeOrCoffeeShop'            => '&mdash; Cafe or Coffee Shop',
                                    'FastFoodRestaurant'          => '&mdash; Fast Food Restaurant',
                                    'IceCreamShop'                => '&mdash; Ice Cream Shop',
                                    'Restaurant'                  => '&mdash; Restaurant',
                                    'Winery'                      => '&mdash; Winery',
                                    'GovernmentOffice'            => 'Government Office',
                                    'PostOffice'                  => '&mdash; Post Office',
                                    'HealthAndBeautyBusiness'     => 'Health And Beauty Business',
                                    'BeautySalon'                 => '&mdash; Beauty Salon',
                                    'DaySpa'                      => '&mdash; Day Spa',
                                    'HairSalon'                   => '&mdash; Hair Salon',
                                    'HealthClub'                  => '&mdash; Health Club',
                                    'NailSalon'                   => '&mdash; Nail Salon',
                                    'TattooParlor'                => '&mdash; Tattoo Parlor',
                                    'HomeAndConstructionBusiness' => 'Home And Construction Business',
                                    'Electrician'                 => '&mdash; Electrician',
                                    'GeneralContractor'           => '&mdash; General Contractor',
                                    'HVACBusiness'                => '&mdash; HVAC Business',
                                    'HousePainter'                => '&mdash; House Painter',
                                    'Locksmith'                   => '&mdash; Locksmith',
                                    'MovingCompany'               => '&mdash; Moving Company',
                                    'Plumber'                     => '&mdash; Plumber',
                                    'RoofingContractor'           => '&mdash; Roofing Contractor',
                                    'InternetCafe'                => 'Internet Cafe',
                                    'Library'                     => ' Library',
                                    'LodgingBusiness'             => 'Lodging Business',
                                    'BedAndBreakfast'             => '&mdash; Bed And Breakfast',
                                    'Hostel'                      => '&mdash; Hostel',
                                    'Hotel'                       => '&mdash; Hotel',
                                    'Motel'                       => '&mdash; Motel',
                                    'MedicalOrganization'         => 'Medical Organization',
                                    'Dentist'                     => '&mdash; Dentist',
                                    'DiagnosticLab'               => '&mdash; Diagnostic Lab',
                                    'Hospital'                    => '&mdash; Hospital',
                                    'MedicalClinic'               => '&mdash; Medical Clinic',
                                    'Optician'                    => '&mdash; Optician',
                                    'Pharmacy'                    => '&mdash; Pharmacy',
                                    'Physician'                   => '&mdash; Physician',
                                    'VeterinaryCare'              => '&mdash; Veterinary Care',
                                    'ProfessionalService'         => 'Professional Service',
                                    'AccountingService'           => '&mdash; Accounting Service',
                                    'LegalService'                => '&mdash; Legal Service',
                                    'Dentist'                     => '&mdash; Dentist',
                                    'Electrician'                 => '&mdash; Electrician',
                                    'GeneralContractor'           => '&mdash; General Contractor',
                                    'HousePainter'                => '&mdash; House Painter',
                                    'Locksmith'                   => '&mdash; Locksmith',
                                    'Notary'                      => '&mdash; Notary',
                                    'Plumber'                     => '&mdash; Plumber',
                                    'RoofingContractor'           => '&mdash; Roofing Contractor',
                                    'RadioStation'                => 'Radio Station',
                                    'RealEstateAgent'             => 'Real Estate Agent',
                                    'RecyclingCenter'             => 'Recycling Center',
                                    'SelfStorage'                 => 'Self Storage',
                                    'ShoppingCenter'              => 'Shopping Center',
                                    'SportsActivityLocation'      => 'Sports Activity Location',
                                    'BowlingAlley'                => '&mdash; Bowling Alley',
                                    'ExerciseGym'                 => '&mdash; Exercise Gym',
                                    'GolfCourse'                  => '&mdash; Golf Course',
                                    'HealthClub'                  => '&mdash; Health Club',
                                    'PublicSwimmingPool'          => '&mdash; Public Swimming Pool',
                                    'SkiResort'                   => '&mdash; Ski Resort',
                                    'SportsClub'                  => '&mdash; Sports Club',
                                    'StadiumOrArena'              => '&mdash; Stadium or Arena',
                                    'TennisComplex'               => '&mdash; Tennis Complex',
                                    'Store'                       => ' Store',
                                    'AutoPartsStore'              => '&mdash; Auto Parts Store',
                                    'BikeStore'                   => '&mdash; Bike Store',
                                    'BookStore'                   => '&mdash; Book Store',
                                    'ClothingStore'               => '&mdash; Clothing Store',
                                    'ComputerStore'               => '&mdash; Computer Store',
                                    'ConvenienceStore'            => '&mdash; Convenience Store',
                                    'DepartmentStore'             => '&mdash; Department Store',
                                    'ElectronicsStore'            => '&mdash; Electronics Store',
                                    'Florist'                     => '&mdash; Florist',
                                    'FurnitureStore'              => '&mdash; Furniture Store',
                                    'GardenStore'                 => '&mdash; Garden Store',
                                    'GroceryStore'                => '&mdash; Grocery Store',
                                    'HardwareStore'               => '&mdash; Hardware Store',
                                    'HobbyShop'                   => '&mdash; Hobby Shop',
                                    'HomeGoodsStore'              => '&mdash; HomeGoods Store',
                                    'JewelryStore'                => '&mdash; Jewelry Store',
                                    'LiquorStore'                 => '&mdash; Liquor Store',
                                    'MensClothingStore'           => '&mdash; Mens Clothing Store',
                                    'MobilePhoneStore'            => '&mdash; Mobile Phone Store',
                                    'MovieRentalStore'            => '&mdash; Movie Rental Store',
                                    'MusicStore'                  => '&mdash; Music Store',
                                    'OfficeEquipmentStore'        => '&mdash; Office Equipment Store',
                                    'OutletStore'                 => '&mdash; Outlet Store',
                                    'PawnShop'                    => '&mdash; Pawn Shop',
                                    'PetStore'                    => '&mdash; Pet Store',
                                    'ShoeStore'                   => '&mdash; Shoe Store',
                                    'SportingGoodsStore'          => '&mdash; Sporting Goods Store',
                                    'TireShop'                    => '&mdash; Tire Shop',
                                    'ToyStore'                    => '&mdash; Toy Store',
                                    'WholesaleStore'              => '&mdash; Wholesale Store',
                                    'TelevisionStation'           => 'Television Station',
                                    'TouristInformationCenter'    => 'Tourist Information Center',
                                    'TravelAgency'                => 'Travel Agency',
                                    'Airport'                     => 'Airport',
                                    'Aquarium'                    => 'Aquarium',
                                    'Beach'                       => 'Beach',
                                    'BusStation'                  => 'BusStation',
                                    'BusStop'                     => 'BusStop',
                                    'Campground'                  => 'Campground',
                                    'Cemetery'                    => 'Cemetery',
                                    'Crematorium'                 => 'Crematorium',
                                    'EventVenue'                  => 'Event Venue',
                                    'FireStation'                 => 'Fire Station',
                                    'GovernmentBuilding'          => 'Government Building',
                                    'CityHall'                    => '&mdash; City Hall',
                                    'Courthouse'                  => '&mdash; Courthouse',
                                    'DefenceEstablishment'        => '&mdash; Defence Establishment',
                                    'Embassy'                     => '&mdash; Embassy',
                                    'LegislativeBuilding'         => '&mdash; Legislative Building',
                                    'Hospital'                    => 'Hospital',
                                    'MovieTheater'                => 'Movie Theater',
                                    'Museum'                      => 'Museum',
                                    'MusicVenue'                  => 'Music Venue',
                                    'Park'                        => 'Park',
                                    'ParkingFacility'             => 'Parking Facility',
                                    'PerformingArtsTheater'       => 'Performing Arts Theater',
                                    'PlaceOfWorship'              => 'Place Of Worship',
                                    'BuddhistTemple'              => '&mdash; Buddhist Temple',
                                    'CatholicChurch'              => '&mdash; Catholic Church',
                                    'Church'                      => '&mdash; Church',
                                    'HinduTemple'                 => '&mdash; Hindu Temple',
                                    'Mosque'                      => '&mdash; Mosque',
                                    'Synagogue'                   => '&mdash; Synagogue',
                                    'Playground'                  => 'Playground',
                                    'PoliceStation'               => 'PoliceStation',
                                    'RVPark'                      => 'RVPark',
                                    'StadiumOrArena'              => 'StadiumOrArena',
                                    'SubwayStation'               => 'SubwayStation',
                                    'TaxiStand'                   => 'TaxiStand',
                                    'TrainStation'                => 'TrainStation',
                                    'Zoo'                         => 'Zoo',
                                    'Residence'                   => 'Residence',
                                    'ApartmentComplex'            => '&mdash; Apartment Complex',
                                    'GatedResidenceCommunity'     => '&mdash; Gated Residence Community',
                                    'SingleFamilyResidence'       => '&mdash; Single Family Residence',
                            );
            return $location_busines;
    }
}
if( ! function_exists( 'get_country_array' ) ) {
    
    function get_country_array() {
                            $countries = array(
                                    'AX' => __( 'Åland Islands', 'seo-breeze-local' ),
                                    'AF' => __( 'Afghanistan', 'seo-breeze-local' ),
                                    'AL' => __( 'Albania', 'seo-breeze-local' ),
                                    'DZ' => __( 'Algeria', 'seo-breeze-local' ),
                                    'AD' => __( 'Andorra', 'seo-breeze-local' ),
                                    'AO' => __( 'Angola', 'seo-breeze-local' ),
                                    'AI' => __( 'Anguilla', 'seo-breeze-local' ),
                                    'AQ' => __( 'Antarctica', 'seo-breeze-local' ),
                                    'AG' => __( 'Antigua and Barbuda', 'seo-breeze-local' ),
                                    'AR' => __( 'Argentina', 'seo-breeze-local' ),
                                    'AM' => __( 'Armenia', 'seo-breeze-local' ),
                                    'AW' => __( 'Aruba', 'seo-breeze-local' ),
                                    'AU' => __( 'Australia', 'seo-breeze-local' ),
                                    'AT' => __( 'Austria', 'seo-breeze-local' ),
                                    'AZ' => __( 'Azerbaijan', 'seo-breeze-local' ),
                                    'BS' => __( 'Bahamas', 'seo-breeze-local' ),
                                    'BH' => __( 'Bahrain', 'seo-breeze-local' ),
                                    'BD' => __( 'Bangladesh', 'seo-breeze-local' ),
                                    'BB' => __( 'Barbados', 'seo-breeze-local' ),
                                    'BY' => __( 'Belarus', 'seo-breeze-local' ),
                                    'PW' => __( 'Belau', 'seo-breeze-local' ),
                                    'BE' => __( 'Belgium', 'seo-breeze-local' ),
                                    'BZ' => __( 'Belize', 'seo-breeze-local' ),
                                    'BJ' => __( 'Benin', 'seo-breeze-local' ),
                                    'BM' => __( 'Bermuda', 'seo-breeze-local' ),
                                    'BT' => __( 'Bhutan', 'seo-breeze-local' ),
                                    'BO' => __( 'Bolivia', 'seo-breeze-local' ),
                                    'BQ' => __( 'Bonaire, Sint Eustatius and Saba', 'seo-breeze-local' ),
                                    'BA' => __( 'Bosnia and Herzegovina', 'seo-breeze-local' ),
                                    'BW' => __( 'Botswana', 'seo-breeze-local' ),
                                    'BV' => __( 'Bouvet Island', 'seo-breeze-local' ),
                                    'BR' => __( 'Brazil', 'seo-breeze-local' ),
                                    'IO' => __( 'British Indian Ocean Territory', 'seo-breeze-local' ),
                                    'VG' => __( 'British Virgin Islands', 'seo-breeze-local' ),
                                    'BN' => __( 'Brunei', 'seo-breeze-local' ),
                                    'BG' => __( 'Bulgaria', 'seo-breeze-local' ),
                                    'BF' => __( 'Burkina Faso', 'seo-breeze-local' ),
                                    'BI' => __( 'Burundi', 'seo-breeze-local' ),
                                    'KH' => __( 'Cambodia', 'seo-breeze-local' ),
                                    'CM' => __( 'Cameroon', 'seo-breeze-local' ),
                                    'CA' => __( 'Canada', 'seo-breeze-local' ),
                                    'CV' => __( 'Cape Verde', 'seo-breeze-local' ),
                                    'KY' => __( 'Cayman Islands', 'seo-breeze-local' ),
                                    'CF' => __( 'Central African Republic', 'seo-breeze-local' ),
                                    'TD' => __( 'Chad', 'seo-breeze-local' ),
                                    'CL' => __( 'Chile', 'seo-breeze-local' ),
                                    'CN' => __( 'China', 'seo-breeze-local' ),
                                    'CX' => __( 'Christmas Island', 'seo-breeze-local' ),
                                    'CC' => __( 'Cocos (Keeling) Islands', 'seo-breeze-local' ),
                                    'CO' => __( 'Colombia', 'seo-breeze-local' ),
                                    'KM' => __( 'Comoros', 'seo-breeze-local' ),
                                    'CG' => __( 'Congo (Brazzaville)', 'seo-breeze-local' ),
                                    'CD' => __( 'Congo (Kinshasa)', 'seo-breeze-local' ),
                                    'CK' => __( 'Cook Islands', 'seo-breeze-local' ),
                                    'CR' => __( 'Costa Rica', 'seo-breeze-local' ),
                                    'HR' => __( 'Croatia', 'seo-breeze-local' ),
                                    'CU' => __( 'Cuba', 'seo-breeze-local' ),
                                    'CW' => __( 'Curaçao', 'seo-breeze-local' ),
                                    'CY' => __( 'Cyprus', 'seo-breeze-local' ),
                                    'CZ' => __( 'Czech Republic', 'seo-breeze-local' ),
                                    'DK' => __( 'Denmark', 'seo-breeze-local' ),
                                    'DJ' => __( 'Djibouti', 'seo-breeze-local' ),
                                    'DM' => __( 'Dominica', 'seo-breeze-local' ),
                                    'DO' => __( 'Dominican Republic', 'seo-breeze-local' ),
                                    'EC' => __( 'Ecuador', 'seo-breeze-local' ),
                                    'EG' => __( 'Egypt', 'seo-breeze-local' ),
                                    'SV' => __( 'El Salvador', 'seo-breeze-local' ),
                                    'GQ' => __( 'Equatorial Guinea', 'seo-breeze-local' ),
                                    'ER' => __( 'Eritrea', 'seo-breeze-local' ),
                                    'EE' => __( 'Estonia', 'seo-breeze-local' ),
                                    'ET' => __( 'Ethiopia', 'seo-breeze-local' ),
                                    'FK' => __( 'Falkland Islands', 'seo-breeze-local' ),
                                    'FO' => __( 'Faroe Islands', 'seo-breeze-local' ),
                                    'FJ' => __( 'Fiji', 'seo-breeze-local' ),
                                    'FI' => __( 'Finland', 'seo-breeze-local' ),
                                    'FR' => __( 'France', 'seo-breeze-local' ),
                                    'GF' => __( 'French Guiana', 'seo-breeze-local' ),
                                    'PF' => __( 'French Polynesia', 'seo-breeze-local' ),
                                    'TF' => __( 'French Southern Territories', 'seo-breeze-local' ),
                                    'GA' => __( 'Gabon', 'seo-breeze-local' ),
                                    'GM' => __( 'Gambia', 'seo-breeze-local' ),
                                    'GE' => __( 'Georgia', 'seo-breeze-local' ),
                                    'DE' => __( 'Germany', 'seo-breeze-local' ),
                                    'GH' => __( 'Ghana', 'seo-breeze-local' ),
                                    'GI' => __( 'Gibraltar', 'seo-breeze-local' ),
                                    'GR' => __( 'Greece', 'seo-breeze-local' ),
                                    'GL' => __( 'Greenland', 'seo-breeze-local' ),
                                    'GD' => __( 'Grenada', 'seo-breeze-local' ),
                                    'GP' => __( 'Guadeloupe', 'seo-breeze-local' ),
                                    'GT' => __( 'Guatemala', 'seo-breeze-local' ),
                                    'GG' => __( 'Guernsey', 'seo-breeze-local' ),
                                    'GN' => __( 'Guinea', 'seo-breeze-local' ),
                                    'GW' => __( 'Guinea-Bissau', 'seo-breeze-local' ),
                                    'GY' => __( 'Guyana', 'seo-breeze-local' ),
                                    'HT' => __( 'Haiti', 'seo-breeze-local' ),
                                    'HM' => __( 'Heard Island and McDonald Islands', 'seo-breeze-local' ),
                                    'HN' => __( 'Honduras', 'seo-breeze-local' ),
                                    'HK' => __( 'Hong Kong', 'seo-breeze-local' ),
                                    'HU' => __( 'Hungary', 'seo-breeze-local' ),
                                    'IS' => __( 'Iceland', 'seo-breeze-local' ),
                                    'IN' => __( 'India', 'seo-breeze-local' ),
                                    'ID' => __( 'Indonesia', 'seo-breeze-local' ),
                                    'IR' => __( 'Iran', 'seo-breeze-local' ),
                                    'IQ' => __( 'Iraq', 'seo-breeze-local' ),
                                    'IM' => __( 'Isle of Man', 'seo-breeze-local' ),
                                    'IL' => __( 'Israel', 'seo-breeze-local' ),
                                    'IT' => __( 'Italy', 'seo-breeze-local' ),
                                    'CI' => __( 'Ivory Coast', 'seo-breeze-local' ),
                                    'JM' => __( 'Jamaica', 'seo-breeze-local' ),
                                    'JP' => __( 'Japan', 'seo-breeze-local' ),
                                    'JE' => __( 'Jersey', 'seo-breeze-local' ),
                                    'JO' => __( 'Jordan', 'seo-breeze-local' ),
                                    'KZ' => __( 'Kazakhstan', 'seo-breeze-local' ),
                                    'KE' => __( 'Kenya', 'seo-breeze-local' ),
                                    'KI' => __( 'Kiribati', 'seo-breeze-local' ),
                                    'KW' => __( 'Kuwait', 'seo-breeze-local' ),
                                    'KG' => __( 'Kyrgyzstan', 'seo-breeze-local' ),
                                    'LA' => __( 'Laos', 'seo-breeze-local' ),
                                    'LV' => __( 'Latvia', 'seo-breeze-local' ),
                                    'LB' => __( 'Lebanon', 'seo-breeze-local' ),
                                    'LS' => __( 'Lesotho', 'seo-breeze-local' ),
                                    'LR' => __( 'Liberia', 'seo-breeze-local' ),
                                    'LY' => __( 'Libya', 'seo-breeze-local' ),
                                    'LI' => __( 'Liechtenstein', 'seo-breeze-local' ),
                                    'LT' => __( 'Lithuania', 'seo-breeze-local' ),
                                    'LU' => __( 'Luxembourg', 'seo-breeze-local' ),
                                    'MO' => __( 'Macao S.A.R., China', 'seo-breeze-local' ),
                                    'MK' => __( 'Macedonia', 'seo-breeze-local' ),
                                    'MG' => __( 'Madagascar', 'seo-breeze-local' ),
                                    'MW' => __( 'Malawi', 'seo-breeze-local' ),
                                    'MY' => __( 'Malaysia', 'seo-breeze-local' ),
                                    'MV' => __( 'Maldives', 'seo-breeze-local' ),
                                    'ML' => __( 'Mali', 'seo-breeze-local' ),
                                    'MT' => __( 'Malta', 'seo-breeze-local' ),
                                    'MH' => __( 'Marshall Islands', 'seo-breeze-local' ),
                                    'MQ' => __( 'Martinique', 'seo-breeze-local' ),
                                    'MR' => __( 'Mauritania', 'seo-breeze-local' ),
                                    'MU' => __( 'Mauritius', 'seo-breeze-local' ),
                                    'YT' => __( 'Mayotte', 'seo-breeze-local' ),
                                    'MX' => __( 'Mexico', 'seo-breeze-local' ),
                                    'FM' => __( 'Micronesia', 'seo-breeze-local' ),
                                    'MD' => __( 'Moldova', 'seo-breeze-local' ),
                                    'MC' => __( 'Monaco', 'seo-breeze-local' ),
                                    'MN' => __( 'Mongolia', 'seo-breeze-local' ),
                                    'ME' => __( 'Montenegro', 'seo-breeze-local' ),
                                    'MS' => __( 'Montserrat', 'seo-breeze-local' ),
                                    'MA' => __( 'Morocco', 'seo-breeze-local' ),
                                    'MZ' => __( 'Mozambique', 'seo-breeze-local' ),
                                    'MM' => __( 'Myanmar', 'seo-breeze-local' ),
                                    'NA' => __( 'Namibia', 'seo-breeze-local' ),
                                    'NR' => __( 'Nauru', 'seo-breeze-local' ),
                                    'NP' => __( 'Nepal', 'seo-breeze-local' ),
                                    'NL' => __( 'Netherlands', 'seo-breeze-local' ),
                                    'AN' => __( 'Netherlands Antilles', 'seo-breeze-local' ),
                                    'NC' => __( 'New Caledonia', 'seo-breeze-local' ),
                                    'NZ' => __( 'New Zealand', 'seo-breeze-local' ),
                                    'NI' => __( 'Nicaragua', 'seo-breeze-local' ),
                                    'NE' => __( 'Niger', 'seo-breeze-local' ),
                                    'NG' => __( 'Nigeria', 'seo-breeze-local' ),
                                    'NU' => __( 'Niue', 'seo-breeze-local' ),
                                    'NF' => __( 'Norfolk Island', 'seo-breeze-local' ),
                                    'KP' => __( 'North Korea', 'seo-breeze-local' ),
                                    'NO' => __( 'Norway', 'seo-breeze-local' ),
                                    'OM' => __( 'Oman', 'seo-breeze-local' ),
                                    'PK' => __( 'Pakistan', 'seo-breeze-local' ),
                                    'PS' => __( 'Palestinian Territory', 'seo-breeze-local' ),
                                    'PA' => __( 'Panama', 'seo-breeze-local' ),
                                    'PG' => __( 'Papua New Guinea', 'seo-breeze-local' ),
                                    'PY' => __( 'Paraguay', 'seo-breeze-local' ),
                                    'PE' => __( 'Peru', 'seo-breeze-local' ),
                                    'PH' => __( 'Philippines', 'seo-breeze-local' ),
                                    'PN' => __( 'Pitcairn', 'seo-breeze-local' ),
                                    'PL' => __( 'Poland', 'seo-breeze-local' ),
                                    'PT' => __( 'Portugal', 'seo-breeze-local' ),
                                    'QA' => __( 'Qatar', 'seo-breeze-local' ),
                                    'IE' => __( 'Republic of Ireland', 'seo-breeze-local' ),
                                    'RE' => __( 'Reunion', 'seo-breeze-local' ),
                                    'RO' => __( 'Romania', 'seo-breeze-local' ),
                                    'RU' => __( 'Russia', 'seo-breeze-local' ),
                                    'RW' => __( 'Rwanda', 'seo-breeze-local' ),
                                    'ST' => __( 'São Tomé and Príncipe', 'seo-breeze-local' ),
                                    'BL' => __( 'Saint Barthélemy', 'seo-breeze-local' ),
                                    'SH' => __( 'Saint Helena', 'seo-breeze-local' ),
                                    'KN' => __( 'Saint Kitts and Nevis', 'seo-breeze-local' ),
                                    'LC' => __( 'Saint Lucia', 'seo-breeze-local' ),
                                    'SX' => __( 'Saint Martin (Dutch part)', 'seo-breeze-local' ),
                                    'MF' => __( 'Saint Martin (French part)', 'seo-breeze-local' ),
                                    'PM' => __( 'Saint Pierre and Miquelon', 'seo-breeze-local' ),
                                    'VC' => __( 'Saint Vincent and the Grenadines', 'seo-breeze-local' ),
                                    'SM' => __( 'San Marino', 'seo-breeze-local' ),
                                    'SA' => __( 'Saudi Arabia', 'seo-breeze-local' ),
                                    'SN' => __( 'Senegal', 'seo-breeze-local' ),
                                    'RS' => __( 'Serbia', 'seo-breeze-local' ),
                                    'SC' => __( 'Seychelles', 'seo-breeze-local' ),
                                    'SL' => __( 'Sierra Leone', 'seo-breeze-local' ),
                                    'SG' => __( 'Singapore', 'seo-breeze-local' ),
                                    'SK' => __( 'Slovakia', 'seo-breeze-local' ),
                                    'SI' => __( 'Slovenia', 'seo-breeze-local' ),
                                    'SB' => __( 'Solomon Islands', 'seo-breeze-local' ),
                                    'SO' => __( 'Somalia', 'seo-breeze-local' ),
                                    'ZA' => __( 'South Africa', 'seo-breeze-local' ),
                                    'GS' => __( 'South Georgia/Sandwich Islands', 'seo-breeze-local' ),
                                    'KR' => __( 'South Korea', 'seo-breeze-local' ),
                                    'SS' => __( 'South Sudan', 'seo-breeze-local' ),
                                    'ES' => __( 'Spain', 'seo-breeze-local' ),
                                    'LK' => __( 'Sri Lanka', 'seo-breeze-local' ),
                                    'SD' => __( 'Sudan', 'seo-breeze-local' ),
                                    'SR' => __( 'Suriname', 'seo-breeze-local' ),
                                    'SJ' => __( 'Svalbard and Jan Mayen', 'seo-breeze-local' ),
                                    'SZ' => __( 'Swaziland', 'seo-breeze-local' ),
                                    'SE' => __( 'Sweden', 'seo-breeze-local' ),
                                    'CH' => __( 'Switzerland', 'seo-breeze-local' ),
                                    'SY' => __( 'Syria', 'seo-breeze-local' ),
                                    'TW' => __( 'Taiwan', 'seo-breeze-local' ),
                                    'TJ' => __( 'Tajikistan', 'seo-breeze-local' ),
                                    'TZ' => __( 'Tanzania', 'seo-breeze-local' ),
                                    'TH' => __( 'Thailand', 'seo-breeze-local' ),
                                    'TL' => __( 'Timor-Leste', 'seo-breeze-local' ),
                                    'TG' => __( 'Togo', 'seo-breeze-local' ),
                                    'TK' => __( 'Tokelau', 'seo-breeze-local' ),
                                    'TO' => __( 'Tonga', 'seo-breeze-local' ),
                                    'TT' => __( 'Trinidad and Tobago', 'seo-breeze-local' ),
                                    'TN' => __( 'Tunisia', 'seo-breeze-local' ),
                                    'TR' => __( 'Turkey', 'seo-breeze-local' ),
                                    'TM' => __( 'Turkmenistan', 'seo-breeze-local' ),
                                    'TC' => __( 'Turks and Caicos Islands', 'seo-breeze-local' ),
                                    'TV' => __( 'Tuvalu', 'seo-breeze-local' ),
                                    'UG' => __( 'Uganda', 'seo-breeze-local' ),
                                    'UA' => __( 'Ukraine', 'seo-breeze-local' ),
                                    'AE' => __( 'United Arab Emirates', 'seo-breeze-local' ),
                                    'GB' => __( 'United Kingdom (UK)', 'seo-breeze-local' ),
                                    'US' => __( 'United States (US)', 'seo-breeze-local' ),
                                    'UY' => __( 'Uruguay', 'seo-breeze-local' ),
                                    'UZ' => __( 'Uzbekistan', 'seo-breeze-local' ),
                                    'VU' => __( 'Vanuatu', 'seo-breeze-local' ),
                                    'VA' => __( 'Vatican', 'seo-breeze-local' ),
                                    'VE' => __( 'Venezuela', 'seo-breeze-local' ),
                                    'VN' => __( 'Vietnam', 'seo-breeze-local' ),
                                    'WF' => __( 'Wallis and Futuna', 'seo-breeze-local' ),
                                    'EH' => __( 'Western Sahara', 'seo-breeze-local' ),
                                    'WS' => __( 'Western Samoa', 'seo-breeze-local' ),
                                    'YE' => __( 'Yemen', 'seo-breeze-local' ),
                                    'ZM' => __( 'Zambia', 'seo-breeze-local' ),
                                    'ZW' => __( 'Zimbabwe', 'seo-breeze-local' ),
                            );

                            return $countries;
    }
}
if( !function_exists( 'get_unit_system_array') ) {
    
    function get_unit_system_array(){
        $unit_system = array(
            'METRIC' => __( 'Metric', 'seo-breeze-local'),
            'IMPERIAL' => __( 'Imperial', 'seo-breeze-local')
        );
        return $unit_system;
    }
}
if( !function_exists( 'get_map_view_style') ) {
    function get_map_view_style(){
        $map_view_style = array(
				'HYBRID'    => __( 'Hybrid', 'seo-breeze-local' ),
				'SATELLITE' => __( 'Satellite', 'seo-breeze-local' ),
				'ROADMAP'   => __( 'Roadmap', 'seo-breeze-local' ),
				'TERRAIN'   => __( 'Terrain', 'seo-breeze-local' ),
			) ;
        return $map_view_style;
    }
}

if( !function_exists( 'get_address_format_array')){
    
    function get_address_format_array(){
        $address_format_array =  array(
				'address-state-postal'       => '{address} {city}, {state} {zipcode} &nbsp;&nbsp;&nbsp;&nbsp; (New York, NY 12345 )',
				'address-state-postal-comma' => '{address} {city}, {state}, {zipcode} &nbsp;&nbsp;&nbsp;&nbsp; (New York, NY, 12345 )',
				'address-postal-city-state'  => '{address} {zipcode} {city}, {state} &nbsp;&nbsp;&nbsp;&nbsp; (12345 New York, NY )',
				'address-postal'             => '{address} {city} {zipcode} &nbsp;&nbsp;&nbsp;&nbsp; (New York 12345 )',
				'address-postal-comma'       => '{address} {city}, {zipcode} &nbsp;&nbsp;&nbsp;&nbsp; (New York, 12345 )',
				'address-city'               => '{address} {city} &nbsp;&nbsp;&nbsp;&nbsp; (New York)',
				'postal-address'             => '{zipcode} {state} {city} {address} &nbsp;&nbsp;&nbsp;&nbsp; (12345 NY New York)',
			);
        return $address_format_array;
    }
}

if( !function_exists( 'seobreeze_has_multiple_locations' )){
    function seobreeze_has_multiple_locations() {
            $options = get_option( 'use_multiple_locations' );

            return isset( $options ) && $options == '1';
    }       
}
if( !function_exists( 'wpseo_show_hour_options' )){

function wpseo_show_hour_options( $use_24h = false, $default = 9 ) {
	$output = '<option value="closed">' . __( 'Closed', 'seo-breeze-local' ) . '</option>';

	for ( $i = 0; $i < 24; $i++ ) {
		$time                = strtotime( sprintf( '%1$02d', $i ) . ':00' );
		$time_quarter        = strtotime( sprintf( '%1$02d', $i ) . ':15' );
		$time_half           = strtotime( sprintf( '%1$02d', $i ) . ':30' );
		$time_threequarters  = strtotime( sprintf( '%1$02d', $i ) . ':45' );
		$value               = date( 'H:i', $time );
		$value_quarter       = date( 'H:i', $time_quarter );
		$value_half          = date( 'H:i', $time_half );
		$value_threequarters = date( 'H:i', $time_threequarters );

		$time_value               = date( 'g:i A', $time );
		$time_quarter_value       = date( 'g:i A', $time_quarter );
		$time_half_value          = date( 'g:i A', $time_half );
		$time_threequarters_value = date( 'g:i A', $time_threequarters );

		if ( $use_24h ) {
			$time_value               = date( 'H:i', $time );
			$time_quarter_value       = date( 'H:i', $time_quarter );
			$time_half_value          = date( 'H:i', $time_half );
			$time_threequarters_value = date( 'H:i', $time_threequarters );
		}

		$output .= '<option value="' . $value . '"' . selected( $value, $default, false ) . '>' . $time_value . '</option>';
		$output .= '<option value="' . $value_quarter . '" ' . selected( $value_quarter, $default, false ) . '>' . $time_quarter_value . '</option>';
		$output .= '<option value="' . $value_half . '" ' . selected( $value_half, $default, false ) . '>' . $time_half_value . '</option>';
		$output .= '<option value="' . $value_threequarters . '" ' . selected( $value_threequarters, $default, false ) . '>' . $time_threequarters_value . '</option>';
	}

	return $output;
}
}

if( !function_exists('seobreeze_local_show_address')){
function seobreeze_local_show_address( $atts ) {
	$atts = seobreeze_check_falses( shortcode_atts( array(
		'id'                 => '',
		'hide_name'          => false,
		'hide_address'       => false,
		'show_state'         => true,
		'show_country'       => true,
		'show_phone'         => true,
		'show_phone_2'       => true,
		'show_fax'           => true,
		'show_email'         => true,
		'show_url'           => false,
		'show_vat'           => false,
		'show_tax'           => false,
		'show_coc'           => false,
		'show_price_range'   => false,
		'show_logo'          => false,
		'show_opening_hours' => false,
		'hide_closed'        => false,
		'oneline'            => false,
		'comment'            => '',
		'from_sl'            => false,
		'from_widget'        => false,
		'widget_title'       => '',
		'before_title'       => '',
		'after_title'        => '',
		'echo'               => false,
	), $atts, 'seobreeze_local_show_address' ) );

	//$options = get_option( 'wpseo_local' );
        $hide_opening_hours = get_option( 'hide_opening_hours' );
	if ( isset( $hide_opening_hours ) && $hide_opening_hours == 'on' ) {
		$atts['show_opening_hours'] = false;
	}

	$is_postal_address = false;

	if ( seobreeze_has_multiple_locations() ) {
		// Don't show anything if you don't have permission for this location.
		if ( 'publish' != get_post_status( $atts['id'] ) && ! current_user_can( 'edit_posts' ) ) {
			return '';
		}

		if ( get_post_type() == 'seobreeze_locations' ) {
			if ( ( $atts['id'] == '' || $atts['id'] == 'current' ) && ! is_post_type_archive() ) {
				$atts['id'] = get_queried_object_id();
			}

			if ( is_post_type_archive() && ( $atts['id'] == '' || $atts['id'] == 'current' ) ) {
				return '';
			}
		}
		else if ( $atts['id'] == '' ) {
			return is_singular() ? __( 'Please provide a post ID if you want to show an address outside a Locations singular page', 'seo-breeze-local' ) : '';
		}

		// Get the location data if its already been entered.
		$business_name          = get_the_title( $atts['id'] );
		$business_type          = get_post_meta( $atts['id'], '_seobreeze_business_type', true );
		$business_address       = get_post_meta( $atts['id'], '_seobreeze_business_address', true );
		$business_address_2     = get_post_meta( $atts['id'], '_seobreeze_business_address_2', true );
		$business_city          = get_post_meta( $atts['id'], '_seobreeze_business_city', true );
		$business_state         = get_post_meta( $atts['id'], '_seobreeze_business_state', true );
		$business_zipcode       = get_post_meta( $atts['id'], '_seobreeze_business_zipcode', true );
		$business_country       = get_post_meta( $atts['id'], '_seobreeze_business_country', true );
		$business_phone         = get_post_meta( $atts['id'], '_seobreeze_business_phone', true );
		$business_phone_2nd     = get_post_meta( $atts['id'], '_seobreeze_business_phone_2nd', true );
		$business_fax           = get_post_meta( $atts['id'], '_seobreeze_business_fax', true );
		$business_email         = get_post_meta( $atts['id'], '_seobreeze_business_email', true );
		$business_vat           = get_post_meta( $atts['id'], '_seobreeze_business_vat_id', true );
		$business_tax           = get_post_meta( $atts['id'], '_seobreeze_business_tax_id', true );
		$business_coc           = get_post_meta( $atts['id'], '_seobreeze_business_coc_id', true );
		$business_price_range   = get_post_meta( $atts['id'], '_seobreeze_business_price_range', true );
		$business_url           = get_post_meta( $atts['id'], '_seobreeze_business_url', true );
		$business_location_logo = get_post_meta( $atts['id'], 'media_url_seobreeze_business_location_logo', true );
		$is_postal_address      = get_post_meta( $atts['id'], '_seobreeze_is_postal_address', true );
		$is_postal_address      = $is_postal_address == '1';

		if ( empty( $business_url ) ) {
			$business_url = get_permalink( $atts['id'] );
		}
	}
	else {
                $location_name = get_option('location_name');
		$business_name        = isset( $location_name ) ? $location_name : '';
                $business_type = get_option('business_type');
		$business_type        = isset( $business_type ) ? $business_type : '';
                $location_address = get_option( 'location_address' );
		$business_address     = isset( $location_address ) ? $location_address : '';
                $location_address_2 = get_option( 'location_address_2' );
		$business_address_2   = isset( $location_address_2 ) ? $location_address_2 : '';
                $location_city = get_option('location_city');
		$business_city        = isset( $location_city ) ? $location_city : '';
                $location_state = get_option( 'location_state' );
		$business_state       = isset( $location_state ) ? $location_state : '';
                $location_zipcode = get_option('location_zipcode');
		$business_zipcode     = isset( $location_zipcode ) ? $location_zipcode : '';
                $location_country = get_option( 'location_country' );
		$business_country     = isset( $location_country ) ? $location_country : '';
                $location_phone = get_option( 'location_phone' );
		$business_phone       = isset( $location_phone ) ? $location_phone : '';
                $location_phone_2nd = get_option( 'location_phone_2nd' );
		$business_phone_2nd   = isset( $location_phone_2nd ) ? $location_phone_2nd : '';
                $location_fax = get_option( 'location_fax' );
		$business_fax         = isset( $location_fax ) ? $location_fax : '';
                $location_email = get_option( 'location_email' );
		$business_email       = isset( $location_email ) ? $location_email : '';
                $location_url = get_option( 'location_url' );
		$business_url         = isset( $location_url ) ? $location_url : '';
                $location_vat_id = get_option( 'location_vat_id' );
		$business_vat         = isset( $location_vat_id ) ? $location_vat_id : '';
                $location_tax_id = get_option( 'location_tax_id' );
		$business_tax         = isset( $location_tax_id ) ? $location_tax_id : '';
                $location_coc_id = get_option( 'location_coc_id' );
		$business_coc         = isset( $location_coc_id ) ? $location_coc_id : '';
                $location_price_range = get_option( 'location_price_range' );
		$business_price_range = isset( $location_price_range ) ? $location_price_range : '';
	}

	if ( '' == $business_type ) {
		$business_type = 'LocalBusiness';
	}

	/*
	* This array can be used in a filter to change the order and the labels of contact details
	*/
	$business_contact_details = array(
		array(
			'key'   => 'phone',
			'label' => __( 'Phone', 'seo-breeze-local' ),
		),
		array(
			'key'   => 'phone_2',
			'label' => __( 'Secondary phone', 'seo-breeze-local' ),
		),
		array(
			'key'   => 'fax',
			'label' => __( 'Fax', 'seo-breeze-local' ),
		),
		array(
			'key'   => 'email',
			'label' => __( 'Email', 'seo-breeze-local' ),
		),
		array(
			'key'   => 'url',
			'label' => __( 'URL', 'seo-breeze-local' ),
		),
		array(
			'key'   => 'vat',
			'label' => __( 'VAT ID', 'seo-breeze-local' ),
		),
		array(
			'key'   => 'tax',
			'label' => __( 'Tax ID', 'seo-breeze-local' ),
		),
		array(
			'key'   => 'coc',
			'label' => __( 'Chamber of Commerce ID', 'seo-breeze-local' ),
		),
		array(
			'key'   => 'price_range',
			'label' => __( 'Price range', 'seo-breeze-local' ),
		),
	);


	$tag_title_open  = '';
	$tag_title_close = '';
	if ( ! $atts['oneline'] ) {
		if ( ! $atts['from_widget'] ) {
			$tag_name        = apply_filters( 'seobreeze_local_location_title_tag_name', 'h3' );
			$tag_title_open  = '<' . esc_html( $tag_name ) . '>';
			$tag_title_close = '</' . esc_html( $tag_name ) . '>';
		}
		else if ( $atts['from_widget'] && $atts['widget_title'] == '' ) {
			$tag_title_open  = $atts['before_title'];
			$tag_title_close = $atts['after_title'];
		}
	}

	$output = '<div id="seobreeze_location-' . esc_attr( $atts['id'] ) . '" class="seobreeze-location" itemscope itemtype="http://schema.org/' . ( ( $is_postal_address ) ? 'PostalAddress' : esc_attr( $business_type ) ) . '">';

	// Show URL as hidden meta, when URL is not visible, since it's required by Schema.org markup.
	if ( false === $atts['show_url'] ) {
		$output .= '<meta itemprop="url" content="' . esc_url( $business_url ) . '">';
	}

	// Add featured image as image itemprop.
	if ( seobreeze_has_multiple_locations() ) {
		if ( true === has_post_thumbnail( $atts['id'] ) ) {
			$business_image = esc_url( get_the_post_thumbnail_url( $atts['id'] ) );
		}
	}
        $bus_image = get_option( 'media_url_business_image' );
	if ( ! isset( $business_image_output ) && isset( $bus_image ) ) {
		$business_image = wp_get_attachment_image_url( $bus_image, 'full' );
	}

	if ( isset( $business_image ) ) {
		$output .= '<meta itemprop="image" content="' . $business_image . '">';
	}

	if ( false == $atts['hide_name'] ) {
		$output .= $tag_title_open . ( ( $atts['from_sl'] ) ? '<a href="' . esc_url( $business_url ) . '">' : '' ) . '<span class="seobreeze-business-name" itemprop="name">' . esc_html( $business_name ) . '</span>' . ( ( $atts['from_sl'] ) ? '</a>' : '' ) . $tag_title_close;
	}

	if ( $atts['show_logo'] ) {
		
		if ( ! empty( $business_location_logo ) ) {
			$output .= '<figure itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">';
			$output .= '<img itemprop="url" src="' . $business_location_logo . '" alt="' . esc_attr( get_post_meta( seobreeze_local_get_attachment_id_from_src( $business_location_logo ), '_wp_attachment_image_alt', true ) ) . '"/>';
			$output .= '</figure>';
		}
	}

	$output .= '<' . ( ( $atts['oneline'] ) ? 'span' : 'div' ) . ' ' . ( ( $is_postal_address ) ? '' : 'itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"' ) . ' class="wpseo-address-wrapper">';

	// Output city/state/zipcode in right format.
        $address_format_option = get_option( 'address_format' );
	$address_format        = ! empty( $address_format_option ) ? $address_format_option : 'address-state-postal';
	$format                = new Seobreeze_Local_Address_Format();
	$address_format_output = $format->get_address_format( $address_format, array(
		'show_logo'          => ! empty( $business_location_logo ) ? true : false,
		'business_address'   => $business_address,
		'business_address_2' => $business_address_2,
		'oneline'            => $atts['oneline'],
		'business_zipcode'   => $business_zipcode,
		'business_city'      => $business_city,
		'business_state'     => $business_state,
		'show_state'         => $atts['show_state'],
		'escape_output'      => false,
		'use_tags'           => true,
	) );

	if ( ! empty( $address_format_output ) && false === $atts['hide_address'] ) {
		$output .= $address_format_output;
	}


	if ( $atts['show_country'] && ! empty( $business_country ) ) {
		$output .= ( $atts['oneline'] ) ? ', ' : ' ';
	}

	if ( $atts['show_country'] && ! empty( $business_country ) ) {
		$output .= '<' . ( ( $atts['oneline'] ) ? 'span' : 'div' ) . '  class="country-name" itemprop="addressCountry">' . get_country( $business_country ) . '</' . ( ( $atts['oneline'] ) ? 'span' : 'div' ) . '>';
	}
	$output .= '</' . ( ( $atts['oneline'] ) ? 'span' : 'div' ) . '>';

	$details_output = '';
	foreach ( $business_contact_details as $order => $details ) {

		if ( 'phone' == $details['key'] && $atts['show_phone'] && ! empty( $business_phone ) ) {
			/* translators: %s extends to the label for phone */
			$details_output .= sprintf( '<span class="wpseo-phone">%s: <a href="' . esc_url( 'tel:' . preg_replace( '/[^0-9+]/', '', $business_phone ) ) . '" class="tel"><span itemprop="telephone">' . esc_html( $business_phone ) . '</span></a></span>' . ( ( $atts['oneline'] ) ? ' ' : '<br/>' ), esc_html( $details['label'] ) );
		}
		if ( 'phone_2' == $details['key'] && $atts['show_phone_2'] && ! empty( $business_phone_2nd ) ) {
			/* translators: %s extends to the label for 2nd phone */
			$details_output .= sprintf( '<span class="wpseo-phone2nd">%s: <a href="' . esc_url( 'tel:' . preg_replace( '/[^0-9+]/', '', $business_phone_2nd ) ) . '" class="tel">' . esc_html( $business_phone_2nd ) . '</a></span>' . ( ( $atts['oneline'] ) ? ' ' : '<br/>' ), esc_html( $details['label'] ) );
		}
		if ( 'fax' == $details['key'] && $atts['show_fax'] && ! empty( $business_fax ) ) {
			/* translators: %s extends to the label for fax */
			$details_output .= sprintf( '<span class="wpseo-fax">%s: <span class="tel" itemprop="faxNumber">' . esc_html( $business_fax ) . '</span></span>' . ( ( $atts['oneline'] ) ? ' ' : '<br/>' ), esc_html( $details['label'] ) );
		}
		if ( 'email' == $details['key'] && $atts['show_email'] && ! empty( $business_email ) ) {
			/* translators: %s extends to the label for e-mail */
			$details_output .= sprintf( '<span class="wpseo-email">%s: <a href="' . esc_url( 'mailto:' . antispambot( $business_email ) ) . '" itemprop="email">' . antispambot( esc_html( $business_email ) ) . '</a></span>' . ( ( $atts['oneline'] ) ? ' ' : '<br/>' ), esc_html( $details['label'] ) );
		}
		if ( 'url' == $details['key'] && $atts['show_url'] ) {
			/* translators: %s extends to the label for business url */
			$details_output .= sprintf( '<span class="wpseo-url">%s: <a href="' . esc_url( $business_url ) . '" itemprop="url">' . esc_html( $business_url ) . '</a></span>' . ( ( $atts['oneline'] ) ? ' ' : '<br/>' ), esc_html( $details['label'] ) );
		}
		if ( 'vat' == $details['key'] && $atts['show_vat'] && ! empty( $business_vat ) ) {
			/* translators: %s extends to the label for businss VAT number */
			$details_output .= sprintf( '<span class="wpseo-vat">%s: <span itemprop="vatID">' . esc_html( $business_vat ) . '</span></span>' . ( ( $atts['oneline'] ) ? ' ' : '<br/>' ), esc_html( $details['label'] ) );
		}
		if ( 'tax' == $details['key'] && $atts['show_tax'] && ! empty( $business_tax ) ) {
			/* translators: %s extends to the label for business tax number */
			$details_output .= sprintf( '<span class="wpseo-tax">%s: <span itemprop="taxID">' . esc_html( $business_tax ) . '</span></span>' . ( ( $atts['oneline'] ) ? ' ' : '<br/>' ), esc_html( $details['label'] ) );
		}
		if ( 'coc' == $details['key'] && $atts['show_coc'] && ! empty( $business_coc ) ) {
			/* translators: %s extends to the label for business COC number*/
			$details_output .= sprintf( '<span class="wpseo-vat">%s: ' . esc_html( $business_coc ) . '</span>' . ( ( $atts['oneline'] ) ? ' ' : '<br/>' ), esc_html( $details['label'] ) );
		}
		if ( 'price_range' == $details['key'] && $atts['show_price_range'] && ! empty( $business_price_range ) ) {
			/* translators: %s extends to the label for business Price Range */
			$details_output .= sprintf( '<span class="wpseo-price-range">%s: <span itemprop="priceRange">' . esc_html( $business_price_range ) . '</span></span>' . ( ( $atts['oneline'] ) ? ' ' : '<br/>' ), esc_html( $details['label'] ) );
		}
	}

	if ( '' != $details_output && true == $atts['oneline'] ) {
		$output .= ' - ';
	}

	$output .= $details_output;

	if ( $atts['show_opening_hours'] ) {
		$args = array(
			'id'          => ( seobreeze_has_multiple_locations() ) ? $atts['id'] : '',
			'hide_closed' => $atts['hide_closed'],
		);
		$output .= '<br/>' . seobreeze_local_show_opening_hours( $args, true, false ) . '<br/>';
	}
	$output .= '</div>';

	if ( $atts['comment'] != '' ) {
		$output .= '<div class="wpseo-extra-comment">' . wpautop( html_entity_decode( $atts['comment'] ) ) . '</div>';
	}

	if ( $atts['echo'] ) {
		echo $output;
	}

	return $output;
}
}

if( !function_exists('seobreeze_check_falses')){
function seobreeze_check_falses( $atts ) {
	if ( ! is_array( $atts ) ) {
		return $atts;
	}

	foreach ( $atts as $key => $value ) {
		if ( $value === 'false' || $value === 'off' || $value === 'no' || $value === '0' ) {
			$atts[ $key ] = false;
		}
		else if ( $value === 'true' || $value === 'on' || $value === 'yes' || $value === '1' ) {
			$atts[ $key ] = true;
		}
	}

	return $atts;
}
}

if( !function_exists( 'seobreeze_local_show_opening_hours' ) ){
function seobreeze_local_show_opening_hours( $atts, $show_schema = true, $standalone = true ) {
	$atts = seobreeze_check_falses( shortcode_atts( array(
		'id'          => '',
		'hide_closed' => false,
		'echo'        => false,
		'comment'     => '',
		'show_days'   => array(),
	), $atts, 'wpseo_local_opening_hours' ) );

	//$options = get_option( 'wpseo_local' );
        $hide_opening_hours = get_option('hide_opening_hours');
	if ( isset( $hide_opening_hours ) && $hide_opening_hours == '1' ) {
		return false;
	}

	if ( seobreeze_has_multiple_locations() ) {
		// Don't show anything if you don't have permission for this location.
		if ( 'publish' != get_post_status( $atts['id'] ) && ! current_user_can( 'edit_posts' ) ) {
			return '';
		}

		if ( get_post_type() == 'seobreeze_locations' ) {
			if ( ( $atts['id'] == '' || $atts['id'] == 'current' ) && ! is_post_type_archive() ) {
				$atts['id'] = get_queried_object_id();
			}

			if ( is_post_type_archive() && ( $atts['id'] == '' || $atts['id'] == 'current' ) ) {
				return '';
			}
		}
	}
	else {
		$atts['id'] = '';
	}


	// Output meta tags with required address information when using this as stand alone.
	$business_type = $business_name = $business_address = $business_phone = null;
	if ( true == $standalone ) {
		if ( true == seobreeze_has_multiple_locations() ) {
			$business_name    = get_the_title( $atts['id'] );
			$business_type    = get_post_meta( $atts['id'], '_seobreeze_business_type', true );
			$business_address = get_post_meta( $atts['id'], '_seobreeze_business_address', true );
			$business_phone   = get_post_meta( $atts['id'], '_seobreeze_business_phone', true );
		}
		else {
                        $location_name = get_option( 'location_name' );
			$business_name    = isset( $location_name ) ? $location_name : '';
                        $business_type = get_option( 'business_type' );
			$business_type    = isset( $business_type ) ? $business_type : '';
                        $business_address = get_option( 'business_address' );
			$business_address = isset( $business_address ) ? $business_address : '';
                        $business_phone = get_option( 'business_phone' );
			$business_phone   = isset( $business_phone ) ? $business_phone : '';
		}

		if ( '' == $business_type ) {
			$business_type = 'LocalBusiness';
		}
	}

	$output = '';
	// Output meta tags with required address information when using this as stand alone.
	if ( true == $standalone ) {
		$output .= '<div class="seobreeze-opening-hours-wrapper" itemscope itemtype="http://schema.org/' . esc_attr( $business_type ) . '">';
		$output .= '<meta itemprop="name" content="' . esc_attr( $business_name ) . '">';
		$output .= '<meta itemprop="address" content="' . esc_attr( $business_address ) . '">';
		$output .= '<meta itemprop="telephone" content="' . esc_attr( $business_phone ) . '">';

		// Add featured image as image itemprop.
		if ( seobreeze_has_multiple_locations() ) {
			if ( true === has_post_thumbnail( $atts['id'] ) ) {
				$business_image = esc_url( get_the_post_thumbnail_url( $atts['id'] ) );
			}
		}

		if ( ! isset( $business_image_output ) ) {
			$business_image = wp_get_attachment_image_url( get_option( 'media_url_business_image' ), 'full' );
		}

		if ( isset( $business_image ) ) {
			$output .= '<meta itemprop="image" content="' . $business_image . '">';
		}
	}
	$output .= '<table class="wpseo-opening-hours">';

	// Make the array itterable (Is that a word?).
	$days = new ArrayIterator( array(
		'sunday'    => __( 'Sunday', 'seo-breeze-local' ),
		'monday'    => __( 'Monday', 'seo-breeze-local' ),
		'tuesday'   => __( 'Tuesday', 'seo-breeze-local' ),
		'wednesday' => __( 'Wednesday', 'seo-breeze-local' ),
		'thursday'  => __( 'Thursday', 'seo-breeze-local' ),
		'friday'    => __( 'Friday', 'seo-breeze-local' ),
		'saturday'  => __( 'Saturday', 'seo-breeze-local' ),
	) );

	// Make sure it can be looped infinite times.
	$days = new InfiniteIterator( $days );
	$days = new LimitIterator( $days, get_option( 'start_of_week' ), 7 );

	if ( ! is_array( $atts['show_days'] ) ) {
		$show_days = explode( ',', $atts['show_days'] );
	}
	else {
		$show_days = (array) $atts['show_days'];
	}

	// Loop through the days array where start_of_week is the first key, with a max of 7.
	foreach ( $days as $key => $day ) {

		// Check if the opening hours for this day should be shown.
		if ( is_array( $show_days ) && ! empty( $show_days ) && ! in_array( $key, $show_days ) ) {
			continue;
		}
                $multiple_hours_setting = get_option( 'multiple_opening_hours' );
		$multiple_opening_hours = isset( $multiple_hours_setting ) && $multiple_hours_setting == '1';
		$day_abbr               = ucfirst( substr( $key, 0, 2 ) );

		if ( seobreeze_has_multiple_locations() ) {
			$field_name        = '_seobreeze_opening_hours_' . $key;
			$value_from        = get_post_meta( $atts['id'], $field_name . '_from', true );
			$value_to          = get_post_meta( $atts['id'], $field_name . '_to', true );
			$value_second_from = get_post_meta( $atts['id'], $field_name . '_second_from', true );
			$value_second_to   = get_post_meta( $atts['id'], $field_name . '_second_to', true );

			$multiple_opening_hours = get_post_meta( $atts['id'], '_wpseo_multiple_opening_hours', true );
			$multiple_opening_hours = ! empty( $multiple_opening_hours );
		}
		else {
			$field_name        = 'opening_hours_' . $key;
			$value_from        = isset( $options[ $field_name . '_from' ] ) ? esc_attr( $options[ $field_name . '_from' ] ) : '';
			$value_to          = isset( $options[ $field_name . '_to' ] ) ? esc_attr( $options[ $field_name . '_to' ] ) : '';
			$value_second_from = isset( $options[ $field_name . '_second_from' ] ) ? esc_attr( $options[ $field_name . '_second_from' ] ) : '';
			$value_second_to   = isset( $options[ $field_name . '_second_to' ] ) ? esc_attr( $options[ $field_name . '_second_to' ] ) : '';
		}

		if ( ( $value_from == 'closed' || $value_to == 'closed' ) && $atts['hide_closed'] ) {
			continue;
		}

		$value_from_formatted        = $value_from;
		$value_to_formatted          = $value_to;
		$value_second_from_formatted = $value_second_from;
		$value_second_to_formatted   = $value_second_to;

		if ( ! isset( $options['opening_hours_24h'] ) || $options['opening_hours_24h'] != 'on' ) {
			$value_from_formatted        = date( 'g:i A', strtotime( $value_from ) );
			$value_to_formatted          = date( 'g:i A', strtotime( $value_to ) );
			$value_second_from_formatted = date( 'g:i A', strtotime( $value_second_from ) );
			$value_second_to_formatted   = date( 'g:i A', strtotime( $value_second_to ) );
		}

		$output .= '<tr>';
		$output .= '<td class="day">' . $day . '</td>';
		$output .= '<td class="time">';

		$output_time = '';
		if ( $value_from != 'closed' && $value_to != 'closed' ) {
			$output_time .= '<time ' . ( ( $show_schema ) ? 'itemprop="openingHours"' : '' ) . ' datetime="' . $day_abbr . ' ' . $value_from . '-' . $value_to . '" content="' . $day_abbr . ' ' . $value_from . '-' . $value_to . '">' . $value_from_formatted . ' - ' . $value_to_formatted . '</time>';
		}
		else {
			$output_time .= __( 'Closed', 'seo-breeze-local' );
		}

		if ( $multiple_opening_hours ) {
			if ( $value_from != 'closed' && $value_to != 'closed' && $value_second_from != 'closed' && $value_second_to != 'closed' ) {
				$output_time .= '<span class="openingHoursAnd"> ' . __( 'and', 'seo-breeze-local' ) . ' </span> ';
				$output_time .= '<time ' . ( ( $show_schema ) ? 'itemprop="openingHours"' : '' ) . ' datetime="' . $day_abbr . ' ' . $value_second_from . '-' . $value_second_to . '" content="' . $day_abbr . ' ' . $value_second_from . '-' . $value_second_to . '">' . $value_second_from_formatted . ' - ' . $value_second_to_formatted . '</time>';
			}
			else {
				$output_time .= '';
			}
		}

		$output_time = apply_filters( 'wpseo_opening_hours_time', $output_time, $day, $value_from, $value_to, $atts );
		$output .= $output_time;
		$output .= '</td>';
		$output .= '</tr>';
	}

	$output .= '</table>';

	if ( true == $standalone ) {
		$output .= '</div>'; // .wpseo-opening-hours-wrapper
	}

	if ( $atts['comment'] != '' ) {
		$output .= '<div class="seobreeze-extra-comment">' . wpautop( html_entity_decode( $atts['comment'] ) ) . '</div>';
	}

	if ( $atts['echo'] ) {
		echo $output;
	}

	return $output;
}
}

if(!function_exists('get_country')){
function get_country( $country_code = '' ) {
        $countries = get_country_array();

        if ( $country_code == '' || ! array_key_exists( $country_code, $countries ) ) {
                return false;
        }

        return $countries[ $country_code ];
}
}

if( !function_exists("seobreeze_local_get_attachment_id_from_src")){
function seobreeze_local_get_attachment_id_from_src( $src ) {
	global $wpdb;
	$id = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid = %s", array(
		$src,
	) ) );

	return $id;
}
}

if( ! function_exists( 'seobreeze_local_get_excerpt' ) ){
function seobreeze_local_get_excerpt( $post_id ) {
	global $post;

	$original_post = $post;
	$post          = get_post( $post_id );
	setup_postdata( $post );

	$output = get_the_excerpt();

	// Set back original $post;.
	$post = $original_post;
	wp_reset_postdata();

	return $output;
}
}

if( !function_exists( 'seobreeze_seo_local_is_location_open' ) ){
function seobreeze_seo_local_is_location_open( $post = null ) {
	$timezone_repository = new Seobreeze_Local_Timezone_Repository();
	return $timezone_repository->is_location_open( $post );
}
}

if( !function_exists( 'seobreeze_wpseo_local_get_api_key_server' ) ){

function seobreeze_wpseo_local_get_api_key_server() {
	$api_key_server = '';
	if ( defined( 'SEOBREEZE_LOCAL_API_KEY_SERVER' ) ) {
		$api_key_server = SEOBREEZE_LOCAL_API_KEY_SERVER;
	}
	else {
		$options = get_option( 'api_key' );
		if ( isset( $options ) ) {
			$api_key_server = $options;
		}
	}

	return esc_attr( $api_key_server );
}
}

if(!function_exists( 'seobreeze_geocode_address' )){
function seobreeze_geocode_address( $address ) {
	$geocode_url = 'https://maps.google.com/maps/api/geocode/json?address=' . urlencode( $address ) . '&oe=utf8&sensor=false';
	$api_key     = seobreeze_wpseo_local_get_api_key_server();
	if ( ! empty( $api_key ) ) {
		$geocode_url .= '&key=' . $api_key;
	}

	$response = wp_remote_get( $geocode_url );

	if ( is_wp_error( $response ) || $response['response']['code'] != 200 || empty( $response['body'] ) ) {
		return new WP_Error( 'wpseo-no-response', "Didn't receive a response from Maps API" );
	}

	$response_body = json_decode( $response['body'] );

	if ( 'OK' != $response_body->status ) {
		$error_code = 'wpseo-zero-results';
		if ( $response_body->status == 'OVER_QUERY_LIMIT' ) {
			$error_code = 'wpseo-query-limit';
		}

		return new WP_Error( $error_code, $response_body->status );
	}

	return $response_body;
}
}

if( !function_exists( 'seobreeze_local_show_map' ) ){
function seobreeze_local_show_map( $atts ) {
	global $map_counter, $wpseo_enqueue_geocoder, $wpseo_map;

	
	$tax_query = array();

	// Backwards compatibility for scrollable / zoomable functions.
	if ( is_array( $atts ) && ! array_key_exists( 'zoomable', $atts ) ) {
		$atts['zoomable'] = ( isset( $atts['scrollable'] ) ) ? $atts['scrollable'] : true;
	}
        $map_view_style = get_option( 'map_view_style' );
        $show_route_label = get_option( 'show_route_label' );
	$atts = seobreeze_check_falses( shortcode_atts( array(
		'id'                      => '',
		'term_id'                 => '',
		'center'                  => '',
		'width'                   => 400,
		'height'                  => 300,
		'zoom'                    => -1,
		'show_route'              => true,
		'show_state'              => true,
		'show_country'            => false,
		'show_url'                => false,
		'show_email'              => false,
		'default_show_infowindow' => false,
		'map_style'               => ( isset( $map_view_style ) ) ? $map_view_style : 'ROADMAP',
		'scrollable'              => true,
		'draggable'               => true,
		'marker_clustering'       => false,
		'show_route_label'        => ( isset( $show_route_label ) && ! empty( $show_route_label ) ) ? $show_route_label : __( 'Show route', 'seo-breeze-local' ),
		'from_sl'                 => false,
		'show_category_filter'    => false,
		'echo'                    => false,
	), $atts, 'seobreeze_local_show_map' ) );

	if ( ! isset( $map_counter ) ) {
		$map_counter = 0;
	}
	else {
		$map_counter++;
	}

	// Check if zoom is set to true or false by the wpseo_check_falses function. If so, turn them back into 0 or 1.
	if ( true === $atts['zoom'] ) {
		$atts['zoom'] = 1;
	}
	else if ( false === $atts['zoom'] ) {
		$atts['zoom'] = 0;
	}

	$location_array     = $lats = $longs = array();
	$location_array_str = '';

	$default_custom_marker = '';
	$all_categories        = array();
        
        $custom_marker = get_option( 'custom_marker' );
	if ( isset( $custom_marker ) && intval( $custom_marker ) && empty( $default_custom_marker ) ) {
		$default_custom_marker = wp_get_attachment_url( $custom_marker );
	}
        $location_name = get_option( 'location_name' );
        $location_email = get_option( 'location_email' );
        $location_address = get_option( 'location_address' );
        $location_address_2 = get_option( 'location_address_2' );
        $location_city = get_option( 'location_city' );
        $location_state = get_option( 'location_state' );
        $location_zipcode = get_option( 'location_zipcode' );
        $location_country = get_option( 'location_country' );
        $location_phone = get_option( 'location_phone' );
        $location_phone_2nd = get_option( 'location_phone_2nd' );
        $location_coords_lat = get_option( 'location_coords_lat' );
        $location_coords_long = get_option( 'location_coords_long' );
        $custom_marker = get_option( 'custom_marker' );
        $custom_marker = get_option( 'custom_marker' );
        $custom_marker = get_option( 'custom_marker' );
        $custom_marker = get_option( 'custom_marker' );
        
	if ( ! seobreeze_has_multiple_locations() ) {
		$atts['id'] = '';
                    
		$location_array[] = array(
			'location_name'      => ( isset( $location_name ) ) ? $location_name : '',
			'location_url'       => get_base_url(),
			'location_email'     => ( isset( $location_email ) ) ? $location_email : '',
			'location_address'   => ( isset( $location_address ) ) ? $location_address : '',
			'location_address_2' => ( isset( $location_address_2 ) ) ? $location_address_2 : '',
			'location_city'      => ( isset( $location_city ) ) ? $location_city : '',
			'location_state'     => ( isset( $location_state ) ) ? $location_state : '',
			'location_zipcode'   => ( isset( $location_zipcode ) ) ? $location_zipcode : '',
			'location_country'   => ( isset( $location_country ) ) ? $location_country : '',
			'location_phone'     => ( isset( $location_phone ) ) ? $location_phone : '',
			'location_phone_2nd' => ( isset( $location_phone_2nd ) ) ? $location_phone_2nd : '',
			'coordinates_lat'    => ( isset( $location_coords_lat ) ) ? $location_coords_lat : '',
			'coordinates_long'   => ( isset( $location_coords_long ) ) ? $location_coords_long : '',
			'custom_marker'      => $default_custom_marker,
			'categories'         => array(),
		);

	}
	else {
		if ( get_post_type() == 'seobreeze_locations' ) {
			if ( ( $atts['id'] == '' || $atts['id'] == 'current' ) && ! is_post_type_archive() ) {
				$atts['id'] = get_queried_object_id();
			}

			if ( is_post_type_archive() && ( $atts['id'] == '' || $atts['id'] == 'current' ) ) {
				return '';
			}
		}
		else if ( $atts['id'] != 'all' && empty( $atts['id'] ) ) {
			return ( true == is_singular( 'seobreeze_locations' ) ) ? __( 'Please provide a post ID when using this shortcode outside a Locations singular page', 'seo-breeze-local' ) : '';
		}

		// Define tax_query when term_id is selected.
		if ( $atts['id'] == 'all' && $atts['term_id'] != '' ) {
			$tax_query[] = array(
				'taxonomy' => 'seobreeze_locations_category',
				'field'    => 'term_id',
				'terms'    => $atts['term_id'],
			);
		}

		$location_ids = explode( ',', $atts['id'] );
		if ( $atts['id'] == 'all' || ( $atts['id'] != 'all' && count( $location_ids ) > 1 ) ) {
			$args = array(
				'post_type'      => 'seobreeze_locations',
				'posts_per_page' => ( $atts['id'] == 'all' ) ? -1 : count( $location_ids ),
				'post_status'    => ( is_user_logged_in() && current_user_can( 'edit_posts' ) ? 'any' : 'publish' ),
				'fields'         => 'ids',
				'meta_query'     => array(
					array(
						'key'     => '_seobreeze_business_address',
						'value'   => '',
						'compare' => '!=',
					),
				),
				'tax_query'      => $tax_query,
			);

			if ( count( $location_ids ) > 1 ) {
				$args['post__in'] = $location_ids;
			}

			$location_ids = get_posts( $args );
		}

		foreach ( $location_ids as $location_id ) {
			$custom_marker       = seobreeze_local_get_custom_marker( $location_id, 'seobreeze_locations_category' );
			$location_categories = array();

			// Put all categories in an array, to be passed on to the map later on and for the categories filter.
			$categories = get_the_terms( $location_id, 'seobreeze_locations_category' );
			if ( ! is_wp_error( $categories ) && ! empty( $categories ) ) {
				foreach ( $categories as $category ) {
					$all_categories[ $category->slug ] = $category->name;
					$location_categories[]             = $category->slug;
				}
			}

			$tmp_array = array(
				'location_id'        => $location_id,
				'location_name'      => get_the_title( $location_id ),
				'location_url'       => get_post_meta( $location_id, '_seobreeze_business_url', true ),
				'location_email'     => get_post_meta( $location_id, '_seobreeze_business_email', true ),
				'location_address'   => get_post_meta( $location_id, '_seobreeze_business_address', true ),
				'location_address_2' => get_post_meta( $location_id, '_seobreeze_business_address_2', true ),
				'location_city'      => get_post_meta( $location_id, '_seobreeze_business_city', true ),
				'location_state'     => get_post_meta( $location_id, '_seobreeze_business_state', true ),
				'location_zipcode'   => get_post_meta( $location_id, '_seobreeze_business_zipcode', true ),
				'location_country'   => get_post_meta( $location_id, '_seobreeze_business_country', true ),
				'location_phone'     => get_post_meta( $location_id, '_seobreeze_business_phone', true ),
				'location_phone_2nd' => get_post_meta( $location_id, '_seobreeze_business_phone_2nd', true ),
				'coordinates_lat'    => get_post_meta( $location_id, '_seobreeze_coordinates_lat', true ),
				'coordinates_long'   => get_post_meta( $location_id, '_seobreeze_coordinates_long', true ),
				'custom_marker'      => ( $custom_marker != '' ) ? esc_url( $custom_marker ) : esc_url( $default_custom_marker ),
				'categories'         => $location_categories,
			);

			if ( empty( $tmp_array['location_url'] ) ) {
				$tmp_array['location_url'] = get_permalink( $location_id );
			}

			$location_array[] = $tmp_array;
		}
	}

	// Convert possible comma's in the lat/long to points.
	foreach ( $location_array as $key => $location ) {
		$location_array[ $key ]['coordinates_lat']  = str_replace( ',', '.', $location['coordinates_lat'] );
		$location_array[ $key ]['coordinates_long'] = str_replace( ',', '.', $location['coordinates_long'] );
	}

	$noscript_output = '<ul>';
	foreach ( $location_array as $key => $location ) {

		if ( $location['coordinates_lat'] != '' && $location['coordinates_long'] != '' ) {
			$full_address = seobreeze_local_show_address( array(
				'id'                 => isset( $location['location_id'] ) ? $location['location_id'] : '',
				'hide_name'          => true,
				'business_address'   =>  $location['location_address'] ,
				'business_address_2' => $location['location_address_2'] ,
				'oneline'            => false,
				'business_zipcode'   => $location['location_zipcode'],
				'business_city'      => $location['location_city'],
				'business_state'     => $location['location_state'],
				'show_state'         => $atts['show_state'],
				'show_country'       => false,
				'show_phone'         => false,
				'escape_output'      => true,
				'use_tags'           => true,
			) );

			$location_array_str .= "location_data.push( {
				'name': '" .  $location['location_name']  . "',
				'url': '" .  $location['location_url'] . "',
				'address': '" . $full_address . "',
				'country': '" . get_country( $location['location_country'] ) . "',
				'show_country': " . ( ( $atts['show_country'] ) ? 'true' : 'false' ) . ",
				'url': '" . esc_url( $location['location_url'] ) . "',
				'show_url': " . ( ( $atts['show_url'] ) ? 'true' : 'false' ) . ",
				'email': '" .  $location['location_email']  . "',
				'show_email': " . ( ( $atts['show_email'] ) ? 'true' : 'false' ) . ",
				'phone': '" . $location['location_phone'] . "',
				'phone_2nd': '" .  $location['location_phone_2nd']  . "',
				'lat': " . $location['coordinates_lat']  . ",
				'long': " . $location['coordinates_long']  . ",
				'custom_marker': '" .  $location['custom_marker'] . "',
				'categories': '" . join( ', ', $location['categories'] ) . "', 
			} );\n";
		}

		$noscript_output .= '<li>';
		if ( $location['location_url'] != get_permalink() ) {
			$noscript_output .= '<a href="' . $location['location_url'] . '">';
		}
		$noscript_output .= $location['location_name'];
		if ( $location['location_url'] != get_permalink() ) {
			$noscript_output .= '</a>';
		}
		$noscript_output .= '</li>';
		$noscript_output .= '<li><a href="mailto:' . antispambot( $location['location_email'] ) . '">' . antispambot( $location['location_email'] ) . '</a></li>';

		$full_address = $location['location_address'] . ', ' . $location['location_city'] . ( ( strtolower( $location['location_country'] ) == 'us' ) ? ', ' . $location['location_state'] : '' ) . ', ' . $location['location_zipcode'] . ', ' . get_country( $location['location_country'] );

		$location_array[ $key ]['full_address'] = $full_address;

		$lats[]  = $location['coordinates_lat'];
		$longs[] = $location['coordinates_long'];
	}
	$noscript_output .= '</ul>';

	$map                    = '';
	$wpseo_enqueue_geocoder = true;

	if ( ! is_array( $lats ) || empty( $lats ) || ! is_array( $longs ) || empty( $longs ) ) {
		return;
	}

	if ( $atts['center'] === '' ) {
		$center_lat  = ( min( $lats ) + ( ( max( $lats ) - min( $lats ) ) / 2 ) );
		$center_long = ( min( $longs ) + ( ( max( $longs ) - min( $longs ) ) / 2 ) );
	}
	else {
		$center_lat  = get_post_meta( $atts['center'], '_seobreeze_coordinates_lat', true );
		$center_long = get_post_meta( $atts['center'], '_seobreeze_coordinates_long', true );
	}

	// Default to zoom 10 if there's only one location as a center + bounds would zoom in far too much.
	if ( -1 == $atts['zoom'] && 1 === count( $location_array ) ) {
		$atts['zoom'] = 10;
	}

	if ( $location_array_str != '' ) {

		$wpseo_map .= '<script type="text/javascript">
			var map_' . $map_counter . ';
			var directionsDisplay_' . $map_counter . ';

			function wpseo_map_init' . ( ( $map_counter != 0 ) ? '_' . $map_counter : '' ) . '() {
				var location_data = new Array();' . PHP_EOL . $location_array_str . '
				map_' . $map_counter . ' = wpseo_show_map( location_data, ' . $map_counter . ', ' . $center_lat . ', ' . $center_long . ', ' . $atts['zoom'] . ', "' . $atts['map_style'] . '", "' . $atts['scrollable'] . '", "' . $atts['draggable'] . '", "' . $atts['default_show_infowindow'] . '", "' . is_admin() . '", "' . $atts['marker_clustering'] . '" );
				directionsDisplay_' . $map_counter . ' = wpseo_get_directions(map_' . $map_counter . ', location_data, ' . $map_counter . ', "' . $atts['show_route'] . '");
			}

			if( window.addEventListener )
				window.addEventListener( "load", wpseo_map_init' . ( ( $map_counter != 0 ) ? '_' . $map_counter : '' ) . ', false );
			else if(window.attachEvent )
				window.attachEvent( "onload", wpseo_map_init' . ( ( $map_counter != 0 ) ? '_' . $map_counter : '' ) . ');
		</script>' . PHP_EOL;

		// Override(reset) the setting for images inside the map.
		$map .= '<div id="map_canvas' . ( ( $map_counter != 0 ) ? '_' . $map_counter : '' ) . '" class="wpseo-map-canvas" style="max-width: 100%; width: ' . $atts['width'] . 'px; height: ' . $atts['height'] . 'px;">' . $noscript_output . '</div>';

		$route_tag = apply_filters( 'wpseo_local_location_route_title_name', 'h3' );

		if ( $atts['show_route'] && ( ( $atts['id'] != 'all' && strpos( $atts['id'], ',' ) === false ) || $atts['from_sl'] ) ) {
			$map .= '<div id="wpseo-directions-wrapper"' . ( ( $atts['from_sl'] ) ? ' style="display: none;"' : '' ) . '>';
			$map .= '<' . esc_html( $route_tag ) . ' id="wpseo-directions" class="wpseo-directions-heading">' . __( 'Route', 'seo-breeze-local' ) . '</' . esc_html( $route_tag ) . '>';
			$map .= '<form action="" method="post" class="wpseo-directions-form" id="wpseo-directions-form' . ( ( $map_counter != 0 ) ? '_' . $map_counter : '' ) . '" onsubmit="wpseo_calculate_route( map_' . $map_counter . ', directionsDisplay_' . $map_counter . ', ' . $location_array[0]['coordinates_lat'] . ', ' . $location_array[0]['coordinates_long'] . ', ' . $map_counter . '); return false;">';
			$map .= '<p>';
			$map .= __( 'Your location', 'seo-breeze-local' ) . ': <input type="text" size="20" id="origin' . ( ( $map_counter != 0 ) ? '_' . $map_counter : '' ) . '" value="' . ( ! empty( $_REQUEST['wpseo-sl-search'] ) ? esc_attr( $_REQUEST['wpseo-sl-search'] ) : '' ) . '" />';
			$map .= '<input type="submit" class="wpseo-directions-submit" value="' . $atts['show_route_label'] . '">';
			$map .= '<span id="wpseo-noroute" style="display: none;">' . __( 'No route could be calculated.', 'seo-breeze-local' ) . '</span>';
			$map .= '</p>';
			$map .= '</form>';
			$map .= '<div id="directions' . ( ( $map_counter != 0 ) ? '_' . $map_counter : '' ) . '"></div>';
			$map .= '</div>';
		}

		// Show the filter if categories are set, there's more than 1 and if the filter is enabled.
		if ( isset( $all_categories ) && count( $all_categories ) > 1 && $atts['show_category_filter'] ) {
			$map .= '<select id="filter-by-location-category-' . $map_counter . '" class="location-category-filter" onchange="filterMarkers(this.value, ' . $map_counter . ')">';
			$map .= '<option value="">' . __( 'All categories', 'seo-breeze-local' ) . '</option>';
			foreach ( $all_categories as $category_slug => $category_name ) {
				$map .= '<option value="' . $category_slug . '">' . $category_name . '</option>';
			}
			$map .= '</select>';
		}
	}

	if ( $atts['echo'] ) {
		echo $map;
	}

	return $map;
}
}

if( ! function_exists( 'seobreeze_local_get_custom_marker' ) ){
function seobreeze_local_get_custom_marker( $post_id = null, $taxonomy = '' ) {

	$custom_marker = '';

	if ( ! empty( $post_id ) && ! empty( $taxonomy ) ) {
		if ( '' != get_post_meta( $post_id, 'media_url_seobreeze_custom_marker', true ) ) {
			$custom_marker = get_post_meta( $post_id, 'media_url_seobreeze_custom_marker', true );
		}
		
	}
	else {
		$options = get_option( 'custom_marker' );
		if ( isset( $options ) && intval( $options ) ) {
			$custom_marker = wp_get_attachment_url( $options );
		}
	}

	return $custom_marker;
}
}

if( ! function_exists( 'seobreeze_enqueue_geocoder' ) ){

function seobreeze_enqueue_geocoder() {
	global $wpseo_enqueue_geocoder, $wpseo_map;

	if ( is_admin() && 'seobreeze_locations' == get_post_type() ) {
		global $wpseo_enqueue_geocoder;

		$wpseo_enqueue_geocoder = true;
	}

	if ( $wpseo_enqueue_geocoder ) {
                $detect_location = get_option( 'detect_location' );
                $default_country = get_option( 'default_country' );
		//$options         = get_option( 'wpseo_local' );
		$detect_location = isset( $detect_location ) && $detect_location == '1';
		$default_country = isset( $default_country ) ? $default_country : '';
		if ( '' != $default_country ) {
			$default_country = get_country( $default_country );
		}

		// Load frontend scripts.
		$script_name = 'seobreeze-local.js';
		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$script_name = 'seobreeze-local.js';
		}
		wp_enqueue_script( 'wpseo-local-frontend', plugins_url('/seobreeze-local/js/seobreeze-local-frontend.js'), '', '1.0', true );

		wp_localize_script( 'wpseo-local-frontend', 'wpseo_local_data', array(
			'ajaxurl'                   => 'admin-ajax.php',
			'adminurl'                  => admin_url(),
			'has_multiple_locations'    => seobreeze_has_multiple_locations(),
			'unit_system'               => ! empty( $options['unit_system'] ) ? $options['unit_system'] : 'METRIC',
			'default_country'           => $default_country,
			'detect_location'           => $detect_location,
			'marker_cluster_image_path' => apply_filters( 'wpseo_local_marker_cluster_image_path', esc_url( trailingslashit( plugin_dir_url( dirname( __FILE__ ) ) ) . 'seobreeze-local/images/m1.png' ) ),
		) );

		// Load Maps API script.
		$locale = get_locale();
		$locale = explode( '_', $locale );

		// Check if it might be a language spoken in more than one country.
		if ( isset( $locale[1] ) && in_array( $locale[0], array(
				'en',
				'de',
				'es',
				'it',
				'pt',
				'ro',
				'ru',
				'sv',
				'nl',
				'zh',
				'fr',
			) )
		) {
			$language = $locale[0] . '-' . $locale[1];
		}
		else if ( isset( $locale[1] ) ) {
			$language = $locale[1];
		}
		else {
			$language = $locale[0];
		}

		// Build Google Maps embedding URL.
		$google_maps_url = '//maps.google.com/maps/api/js';
		$api_key_browser = seobreeze_local_get_api_key_browser();
		if ( ! empty( $api_key_browser ) ) {
			$google_maps_url = add_query_arg( array(
				'key' => $api_key_browser,
			), $google_maps_url );
		}

		if ( ! empty( $language ) ) {
			$google_maps_url = add_query_arg( array(
				'language' => esc_attr( strtolower( $language ) ),
			), $google_maps_url );
		}

		wp_enqueue_script( 'maps-geocoder', $google_maps_url, array(), null, true );

		echo '<style type="text/css">.wpseo-map-canvas img { max-width: none !important; }</style>' . PHP_EOL;
	}

	echo $wpseo_map;
}
}

if( !function_exists( 'seobreeze_local_get_api_key_browser' ) ){
function seobreeze_local_get_api_key_browser() {
	$api_key_browser = '';
	if ( defined( 'WPSEO_LOCAL_API_KEY_BROWSER' ) ) {
		$api_key_browser = WPSEO_LOCAL_API_KEY_BROWSER;
	}
	else {
		$options = get_option( 'api_key_browser' );
		if ( isset( $options ) ) {
			$api_key_browser = $options;
		}
	}

	return esc_attr( $api_key_browser );
}
}

if( !function_exists( 'seobreeze_local_show_all_locations') ){
    
    function seobreeze_local_show_all_locations( $atts ) {
	$atts = seobreeze_check_falses( shortcode_atts( array(
		'number'             => -1,
		'term_id'            => '',
		'orderby'            => 'menu_order title',
		'order'              => 'ASC',
		'show_state'         => true,
		'show_country'       => true,
		'show_phone'         => true,
		'show_phone_2'       => true,
		'show_fax'           => true,
		'show_email'         => true,
		'show_url'           => false,
		'show_opening_hours' => false,
		'hide_closed'        => false,
		'oneline'            => false,
		'echo'               => false,
		'comment'            => '',
	), $atts, 'seobreeze_local_show_all_locations' ) );

	// Don't show any data when post_type is not activated. This function/shortcode makes no sense for single location.
	if ( ! seobreeze_has_multiple_locations() ) {
		return '';
	}

	$output    = '';
	$tax_query = array();

	if ( '' != $atts['term_id'] ) {
		$tax_query[] = array(
			'taxonomy' => 'seobreeze_locations_category',
			'field'    => 'term_id',
			'terms'    => $atts['term_id'],
		);
	}

	$locations = new WP_Query( array(
		'post_type'      => 'seobreeze_locations',
		'posts_per_page' => $atts['number'],
		'orderby'        => $atts['orderby'],
		'order'          => $atts['order'],
		'fields'         => 'ids',
		'tax_query'      => $tax_query,
	) );

	if ( $locations->post_count > 0 ) :
		$output .= '<div class="wpseo-all-locations">';
		foreach ( $locations->posts as $location_id ) :

			$location = apply_filters( 'seobreeze_all_locations_location', seobreeze_local_show_address( array(
				'id'                 => $location_id,
				'show_state'         => $atts['show_state'],
				'show_country'       => $atts['show_country'],
				'show_phone'         => $atts['show_phone'],
				'show_phone_2'       => $atts['show_phone_2'],
				'show_fax'           => $atts['show_fax'],
				'show_email'         => $atts['show_email'],
				'show_url'           => $atts['show_url'],
				'show_opening_hours' => $atts['show_opening_hours'],
				'hide_closed'        => $atts['hide_closed'],
				'oneline'            => $atts['oneline'],
				'echo'               => false,
			) ) );

			$output .= $location;

		endforeach;

		if ( $atts['comment'] != '' ) {
			$output .= '<div class="wpseo-extra-comment">' . wpautop( html_entity_decode( $atts['comment'] ) ) . '</div>';
		}

		$output .= '</div>';

	else :
		echo '<p>' . __( 'There are no locations to show.', 'seo-breeze-local' ) . '</p>';
	endif;

	if ( $atts['echo'] ) {
		echo $output;
	}

	return $output;
}
}

if( !function_exists( 'seobreeze_local_show_openinghours_shortcode_cb' ) ){

function seobreeze_local_show_openinghours_shortcode_cb( $atts ) {
	return seobreeze_local_show_opening_hours( $atts );
}

}

if( !function_exists( 'seobreeze_may_use_current_location' ) ){

function seobreeze_may_use_current_location() {
	$options = get_option( 'detect_location' );

	return isset( $options ) && $options == '1';
}

}

if( ! function_exists( 'seobreeze_local_show_logo' )){
    
    function seobreeze_local_show_logo( $atts ) {
	$atts = seobreeze_check_falses( shortcode_atts( array(
		'id' => get_the_ID(),
	), $atts ) );

	$output = '';

	if ( 'seobreeze_locations' !== get_post_type( $atts['id'] ) ) {
		return '';
	}

	$location_logo = get_post_meta( $atts['id'], 'media_url_seobreeze_business_location_logo', true );

	if ( '' === $location_logo ) {
		
		$location_logo = get_option( 'company_logo' );
	}

	if ( '' !== $location_logo ) {
		$output = '<img src="' . esc_url( $location_logo ) . '" alt="' . esc_attr( $location_logo_alt = get_post_meta(  $atts['id'], 'media_url_seobreeze_business_location_logo', true ) ) . '">';
	}

	if ( ! empty( $output ) ) {
		return $output;
	}
}
}