<?php

if ( ! class_exists( 'Seobreeze_Local_Frontend' ) ) {

	/**
	 * Class Seobreeze_Local_Frontend
	 *
	 * Handles all frontend functionality.
	 */
	class Seobreeze_Local_Frontend {

		/**
		 * @var array $options Stores the options for this plugin.
		 */
		var $options = array();

		/**
		 * @var boolean $options Whether to load external stylesheet or not.
		 */
		var $load_styles = false;

		/**
		 * Constructor.
		 */
		function __construct() {
			

			// Create shortcode functionality. Functions are defined in includes/functions.php because they're also used by some widgets.
			add_shortcode( 'seobreeze_address', 'seobreeze_local_show_address' );
			add_shortcode( 'seobreeze_all_locations', 'seobreeze_local_show_all_locations' );
			add_shortcode( 'seobreeze_map', 'seobreeze_local_show_map' );
			add_shortcode( 'seobreeze_opening_hours', 'seobreeze_local_show_openinghours_shortcode_cb' );
			add_shortcode( 'seobreeze_local_show_logo', 'seobreeze_local_show_logo' );

			add_action( 'wpseo_opengraph', array( $this, 'opengraph_location' ) );
			add_filter( 'wpseo_opengraph_type', array( $this, 'opengraph_type' ) );
			add_filter( 'wpseo_opengraph_title', array( $this, 'opengraph_title_filter' ) );

			// Genesis 2.0 specific, this filters the Schema.org output Genesis 2.0 comes with.
			add_filter( 'genesis_attr_body', array( $this, 'genesis_contact_page_schema' ), 20, 1 );
			add_filter( 'genesis_attr_entry', array( $this, 'genesis_empty_schema' ), 20, 1 );
			add_filter( 'genesis_attr_entry-title', array( $this, 'genesis_itemprop_name' ), 20, 1 );
		}

		/**
		 * Filter the Genesis page schema and force it to ContactPage for Location pages
		 *
		 * @since 1.1.7
		 *
		 * @link  https://yoast.com/schema-org-genesis-2-0/
		 * @link  http://schema.org/ContactPage
		 *
		 * @param array $attr The Schema.org attributes.
		 *
		 * @return array $attr
		 */
		function genesis_contact_page_schema( $attr ) {
			if ( is_singular( 'wpseo_locations' ) ) {
				$attr['itemtype']  = 'http://schema.org/ContactPage';
				$attr['itemprop']  = '';
				$attr['itemscope'] = 'itemscope';
			}

			return $attr;
		}

		/**
		 * Filter the Genesis schema for an attribute and empty them
		 *
		 * @since 1.1.7
		 *
		 * @link  https://yoast.com/schema-org-genesis-2-0/
		 *
		 * @param array $attr The Schema.org attributes.
		 *
		 * @return array $attr
		 */
		function genesis_empty_schema( $attr ) {
			$attr['itemtype']  = '';
			$attr['itemprop']  = '';
			$attr['itemscope'] = '';

			return $attr;
		}

		/**
		 * Filter the Genesis schema for an attribute itemprop and set it to name
		 *
		 * @since 1.1.7
		 *
		 * @link  https://yoast.com/schema-org-genesis-2-0/
		 *
		 * @param array $attr The Schema.org attributes.
		 *
		 * @return array $attr
		 */
		function genesis_itemprop_name( $attr ) {
			$attr['itemprop'] = 'name';

			return $attr;
		}

		/**
		 * Output opengraph location tags.
		 *
		 * @link  https://developers.facebook.com/docs/reference/opengraph/object-type/business.business
		 * @link  https://developers.facebook.com/docs/reference/opengraph/object-type/restaurant.restaurant
		 *
		 * @since 1.0
		 */
		function opengraph_location() {
			if ( is_singular( 'wpseo_locations' ) || ( 'on' == WPSEO_Meta::get_value( 'opengraph-local' ) && ! wpseo_has_multiple_locations() ) ) {


				$options            = get_option( 'wpseo_local' );
				$hide_opening_hours = isset( $options['hide_opening_hours'] ) && $options['hide_opening_hours'] == 'on';
				$location_data      = wpseo_get_location_details( get_the_ID() );

				echo '<meta property="place:location:latitude" content="' . esc_attr( $location_data['business_coords_lat'] ) . '"/>' . "\n";
				echo '<meta property="place:location:longitude" content="' . esc_attr( $location_data['business_coords_long'] ) . '"/>' . "\n";
				echo '<meta property="business:contact_data:street_address" content="' . esc_attr( $location_data['business_address'] ) . '"/>' . "\n";
				echo '<meta property="business:contact_data:locality" content="' . esc_attr( $location_data['business_city'] ) . '"/>' . "\n";
				echo '<meta property="business:contact_data:country" content="' . WPSEO_Local_Frontend::get_country( $location_data['business_country'] ) . '"/>' . "\n";
				echo '<meta property="business:contact_data:postal_code" content="' . esc_attr( $location_data['business_zipcode'] ) . '"/>' . "\n";
				echo '<meta property="business:contact_data:website" content="' . trailingslashit( WPSEO_Sitemaps_Router::get_base_url( '' ) ) . '"/>' . "\n";

				if ( ! empty( $location_data['business_state'] ) ) {
					echo '<meta property="business:contact_data:region" content="' . esc_attr( $location_data['business_state'] ) . '"/>' . "\n";
				}
				if ( ! empty( $location_data['business_email'] ) ) {
					echo '<meta property="business:contact_data:email" content="' . esc_attr( $location_data['business_email'] ) . '"/>' . "\n";
				}
				if ( ! empty( $location_data['business_phone'] ) ) {
					echo '<meta property="business:contact_data:phone_number" content="' . esc_attr( $location_data['business_phone'] ) . '"/>' . "\n";
				}
				if ( ! empty( $location_data['business_fax'] ) ) {
					echo '<meta property="business:contact_data:fax_number" content="' . esc_attr( $location_data['business_fax'] ) . '"/>' . "\n";
				}

				// Opening Hours.
				if ( false == $hide_opening_hours ) {
					$days = array( 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' );
					foreach ( $days as $day ) {
						$field_name = '_wpseo_opening_hours_' . $day;

						$start = get_post_meta( get_the_ID(), $field_name . '_from', true );
						if ( ! $start || empty( $start ) ) {
							continue;
						}

						$end = get_post_meta( get_the_ID(), $field_name . '_to', true );
						if ( $start == 'closed' ) {
							$end = 'closed';
						}
						echo '<meta property="business:hours:day" content="' . esc_attr( $day ) . '"/>' . "\n";
						echo '<meta property="business:hours:start" content="' . esc_attr( $start ) . '"/>' . "\n";
						echo '<meta property="business:hours:end" content="' . esc_attr( $end ) . '"/>' . "\n";
					}
				}
			}
		}

		/**
		 * Change the OpenGraph type when current post type is a location.
		 *
		 * @link https://developers.facebook.com/docs/reference/opengraph/object-type/business.business
		 * @link https://developers.facebook.com/docs/reference/opengraph/object-type/restaurant.restaurant
		 *
		 * @param string $type The OpenGraph type to be altered.
		 *
		 * @return string
		 */
		function opengraph_type( $type ) {
			global $post;

			if ( ! empty( $post ) && ( ( isset( $post->post_type ) && $post->post_type == 'wpseo_locations' ) || ( 'on' == WPSEO_Meta::get_value( 'opengraph-local', $post->ID ) && ! wpseo_has_multiple_locations() ) ) ) {
				$business_type = get_post_meta( $post->ID, '_wpseo_business_type', true );
				switch ( $business_type ) {
					case 'BarOrPub':
					case 'Winery':
					case 'Restaurant':
						$type = 'restaurant.restaurant';
						break;
					default:
						$type = 'business.business';
						break;
				}
			}

			return $type;
		}

		/**
		 * Filter the OG title output
		 *
		 * @param string $title The title to be filtered.
		 *
		 * @return string
		 */
		function opengraph_title_filter( $title ) {
			if ( 'on' == WPSEO_Meta::get_value( 'opengraph-local' ) && ! wpseo_has_multiple_locations() ) {
				return get_bloginfo( 'name' );
			}
			else if ( wpseo_has_multiple_locations() && is_singular( 'wpseo_locations' ) ) {
				return get_the_title( get_the_ID() );
			}

			return $title;
		}

		/**
		 * Return the country name based on country code
		 *
		 * @since 0.1
		 *
		 * @param string $country_code Two char country code.
		 *
		 * @return string Country name.
		 */
		public static function get_country( $country_code = '' ) {
			$countries = WPSEO_Local_Frontend::get_country_array();

			if ( $country_code == '' || ! array_key_exists( $country_code, $countries ) ) {
				return false;
			}

			return $countries[ $country_code ];
		}

		/**
		 * Retrieves array of all countries and their ISO country code.
		 *
		 * @return array Array of countries.
		 */
		public static function get_country_array() {
			$countries = array(
				'AX' => __( 'Åland Islands', 'seo-breeze-local' ),
				'AF' => __( 'Afghanistan', 'seo-breeze-local' ),
				'AL' => __( 'Albania', 'seo-breeze-local' ),
				'DZ' => __( 'Algeria', 'seo-breeze-local' ),
				'AD' => __( 'Andorra', 'seo-breeze-local' ),
				'AO' => __( 'Angola', 'seo-breeze-local' ),
				'AI' => __( 'Anguilla', 'seo-breeze-local' ),
				'AQ' => __( 'Antarctica', 'seo-breeze-local' ),
				'AG' => __( 'Antigua and Barbuda', 'seo-breeze-local' ),
				'AR' => __( 'Argentina', 'seo-breeze-local' ),
				'AM' => __( 'Armenia', 'seo-breeze-local' ),
				'AW' => __( 'Aruba', 'seo-breeze-local' ),
				'AU' => __( 'Australia', 'seo-breeze-local' ),
				'AT' => __( 'Austria', 'seo-breeze-local' ),
				'AZ' => __( 'Azerbaijan', 'seo-breeze-local' ),
				'BS' => __( 'Bahamas', 'seo-breeze-local' ),
				'BH' => __( 'Bahrain', 'seo-breeze-local' ),
				'BD' => __( 'Bangladesh', 'seo-breeze-local' ),
				'BB' => __( 'Barbados', 'seo-breeze-local' ),
				'BY' => __( 'Belarus', 'seo-breeze-local' ),
				'PW' => __( 'Belau', 'seo-breeze-local' ),
				'BE' => __( 'Belgium', 'seo-breeze-local' ),
				'BZ' => __( 'Belize', 'seo-breeze-local' ),
				'BJ' => __( 'Benin', 'seo-breeze-local' ),
				'BM' => __( 'Bermuda', 'seo-breeze-local' ),
				'BT' => __( 'Bhutan', 'seo-breeze-local' ),
				'BO' => __( 'Bolivia', 'seo-breeze-local' ),
				'BQ' => __( 'Bonaire, Sint Eustatius and Saba', 'seo-breeze-local' ),
				'BA' => __( 'Bosnia and Herzegovina', 'seo-breeze-local' ),
				'BW' => __( 'Botswana', 'seo-breeze-local' ),
				'BV' => __( 'Bouvet Island', 'seo-breeze-local' ),
				'BR' => __( 'Brazil', 'seo-breeze-local' ),
				'IO' => __( 'British Indian Ocean Territory', 'seo-breeze-local' ),
				'VG' => __( 'British Virgin Islands', 'seo-breeze-local' ),
				'BN' => __( 'Brunei', 'seo-breeze-local' ),
				'BG' => __( 'Bulgaria', 'seo-breeze-local' ),
				'BF' => __( 'Burkina Faso', 'seo-breeze-local' ),
				'BI' => __( 'Burundi', 'seo-breeze-local' ),
				'KH' => __( 'Cambodia', 'seo-breeze-local' ),
				'CM' => __( 'Cameroon', 'seo-breeze-local' ),
				'CA' => __( 'Canada', 'seo-breeze-local' ),
				'CV' => __( 'Cape Verde', 'seo-breeze-local' ),
				'KY' => __( 'Cayman Islands', 'seo-breeze-local' ),
				'CF' => __( 'Central African Republic', 'seo-breeze-local' ),
				'TD' => __( 'Chad', 'seo-breeze-local' ),
				'CL' => __( 'Chile', 'seo-breeze-local' ),
				'CN' => __( 'China', 'seo-breeze-local' ),
				'CX' => __( 'Christmas Island', 'seo-breeze-local' ),
				'CC' => __( 'Cocos (Keeling) Islands', 'seo-breeze-local' ),
				'CO' => __( 'Colombia', 'seo-breeze-local' ),
				'KM' => __( 'Comoros', 'seo-breeze-local' ),
				'CG' => __( 'Congo (Brazzaville)', 'seo-breeze-local' ),
				'CD' => __( 'Congo (Kinshasa)', 'seo-breeze-local' ),
				'CK' => __( 'Cook Islands', 'seo-breeze-local' ),
				'CR' => __( 'Costa Rica', 'seo-breeze-local' ),
				'HR' => __( 'Croatia', 'seo-breeze-local' ),
				'CU' => __( 'Cuba', 'seo-breeze-local' ),
				'CW' => __( 'Curaçao', 'seo-breeze-local' ),
				'CY' => __( 'Cyprus', 'seo-breeze-local' ),
				'CZ' => __( 'Czech Republic', 'seo-breeze-local' ),
				'DK' => __( 'Denmark', 'seo-breeze-local' ),
				'DJ' => __( 'Djibouti', 'seo-breeze-local' ),
				'DM' => __( 'Dominica', 'seo-breeze-local' ),
				'DO' => __( 'Dominican Republic', 'seo-breeze-local' ),
				'EC' => __( 'Ecuador', 'seo-breeze-local' ),
				'EG' => __( 'Egypt', 'seo-breeze-local' ),
				'SV' => __( 'El Salvador', 'seo-breeze-local' ),
				'GQ' => __( 'Equatorial Guinea', 'seo-breeze-local' ),
				'ER' => __( 'Eritrea', 'seo-breeze-local' ),
				'EE' => __( 'Estonia', 'seo-breeze-local' ),
				'ET' => __( 'Ethiopia', 'seo-breeze-local' ),
				'FK' => __( 'Falkland Islands', 'seo-breeze-local' ),
				'FO' => __( 'Faroe Islands', 'seo-breeze-local' ),
				'FJ' => __( 'Fiji', 'seo-breeze-local' ),
				'FI' => __( 'Finland', 'seo-breeze-local' ),
				'FR' => __( 'France', 'seo-breeze-local' ),
				'GF' => __( 'French Guiana', 'seo-breeze-local' ),
				'PF' => __( 'French Polynesia', 'seo-breeze-local' ),
				'TF' => __( 'French Southern Territories', 'seo-breeze-local' ),
				'GA' => __( 'Gabon', 'seo-breeze-local' ),
				'GM' => __( 'Gambia', 'seo-breeze-local' ),
				'GE' => __( 'Georgia', 'seo-breeze-local' ),
				'DE' => __( 'Germany', 'seo-breeze-local' ),
				'GH' => __( 'Ghana', 'seo-breeze-local' ),
				'GI' => __( 'Gibraltar', 'seo-breeze-local' ),
				'GR' => __( 'Greece', 'seo-breeze-local' ),
				'GL' => __( 'Greenland', 'seo-breeze-local' ),
				'GD' => __( 'Grenada', 'seo-breeze-local' ),
				'GP' => __( 'Guadeloupe', 'seo-breeze-local' ),
				'GT' => __( 'Guatemala', 'seo-breeze-local' ),
				'GG' => __( 'Guernsey', 'seo-breeze-local' ),
				'GN' => __( 'Guinea', 'seo-breeze-local' ),
				'GW' => __( 'Guinea-Bissau', 'seo-breeze-local' ),
				'GY' => __( 'Guyana', 'seo-breeze-local' ),
				'HT' => __( 'Haiti', 'seo-breeze-local' ),
				'HM' => __( 'Heard Island and McDonald Islands', 'seo-breeze-local' ),
				'HN' => __( 'Honduras', 'seo-breeze-local' ),
				'HK' => __( 'Hong Kong', 'seo-breeze-local' ),
				'HU' => __( 'Hungary', 'seo-breeze-local' ),
				'IS' => __( 'Iceland', 'seo-breeze-local' ),
				'IN' => __( 'India', 'seo-breeze-local' ),
				'ID' => __( 'Indonesia', 'seo-breeze-local' ),
				'IR' => __( 'Iran', 'seo-breeze-local' ),
				'IQ' => __( 'Iraq', 'seo-breeze-local' ),
				'IM' => __( 'Isle of Man', 'seo-breeze-local' ),
				'IL' => __( 'Israel', 'seo-breeze-local' ),
				'IT' => __( 'Italy', 'seo-breeze-local' ),
				'CI' => __( 'Ivory Coast', 'seo-breeze-local' ),
				'JM' => __( 'Jamaica', 'seo-breeze-local' ),
				'JP' => __( 'Japan', 'seo-breeze-local' ),
				'JE' => __( 'Jersey', 'seo-breeze-local' ),
				'JO' => __( 'Jordan', 'seo-breeze-local' ),
				'KZ' => __( 'Kazakhstan', 'seo-breeze-local' ),
				'KE' => __( 'Kenya', 'seo-breeze-local' ),
				'KI' => __( 'Kiribati', 'seo-breeze-local' ),
				'KW' => __( 'Kuwait', 'seo-breeze-local' ),
				'KG' => __( 'Kyrgyzstan', 'seo-breeze-local' ),
				'LA' => __( 'Laos', 'seo-breeze-local' ),
				'LV' => __( 'Latvia', 'seo-breeze-local' ),
				'LB' => __( 'Lebanon', 'seo-breeze-local' ),
				'LS' => __( 'Lesotho', 'seo-breeze-local' ),
				'LR' => __( 'Liberia', 'seo-breeze-local' ),
				'LY' => __( 'Libya', 'seo-breeze-local' ),
				'LI' => __( 'Liechtenstein', 'seo-breeze-local' ),
				'LT' => __( 'Lithuania', 'seo-breeze-local' ),
				'LU' => __( 'Luxembourg', 'seo-breeze-local' ),
				'MO' => __( 'Macao S.A.R., China', 'seo-breeze-local' ),
				'MK' => __( 'Macedonia', 'seo-breeze-local' ),
				'MG' => __( 'Madagascar', 'seo-breeze-local' ),
				'MW' => __( 'Malawi', 'seo-breeze-local' ),
				'MY' => __( 'Malaysia', 'seo-breeze-local' ),
				'MV' => __( 'Maldives', 'seo-breeze-local' ),
				'ML' => __( 'Mali', 'seo-breeze-local' ),
				'MT' => __( 'Malta', 'seo-breeze-local' ),
				'MH' => __( 'Marshall Islands', 'seo-breeze-local' ),
				'MQ' => __( 'Martinique', 'seo-breeze-local' ),
				'MR' => __( 'Mauritania', 'seo-breeze-local' ),
				'MU' => __( 'Mauritius', 'seo-breeze-local' ),
				'YT' => __( 'Mayotte', 'seo-breeze-local' ),
				'MX' => __( 'Mexico', 'seo-breeze-local' ),
				'FM' => __( 'Micronesia', 'seo-breeze-local' ),
				'MD' => __( 'Moldova', 'seo-breeze-local' ),
				'MC' => __( 'Monaco', 'seo-breeze-local' ),
				'MN' => __( 'Mongolia', 'seo-breeze-local' ),
				'ME' => __( 'Montenegro', 'seo-breeze-local' ),
				'MS' => __( 'Montserrat', 'seo-breeze-local' ),
				'MA' => __( 'Morocco', 'seo-breeze-local' ),
				'MZ' => __( 'Mozambique', 'seo-breeze-local' ),
				'MM' => __( 'Myanmar', 'seo-breeze-local' ),
				'NA' => __( 'Namibia', 'seo-breeze-local' ),
				'NR' => __( 'Nauru', 'seo-breeze-local' ),
				'NP' => __( 'Nepal', 'seo-breeze-local' ),
				'NL' => __( 'Netherlands', 'seo-breeze-local' ),
				'AN' => __( 'Netherlands Antilles', 'seo-breeze-local' ),
				'NC' => __( 'New Caledonia', 'seo-breeze-local' ),
				'NZ' => __( 'New Zealand', 'seo-breeze-local' ),
				'NI' => __( 'Nicaragua', 'seo-breeze-local' ),
				'NE' => __( 'Niger', 'seo-breeze-local' ),
				'NG' => __( 'Nigeria', 'seo-breeze-local' ),
				'NU' => __( 'Niue', 'seo-breeze-local' ),
				'NF' => __( 'Norfolk Island', 'seo-breeze-local' ),
				'KP' => __( 'North Korea', 'seo-breeze-local' ),
				'NO' => __( 'Norway', 'seo-breeze-local' ),
				'OM' => __( 'Oman', 'seo-breeze-local' ),
				'PK' => __( 'Pakistan', 'seo-breeze-local' ),
				'PS' => __( 'Palestinian Territory', 'seo-breeze-local' ),
				'PA' => __( 'Panama', 'seo-breeze-local' ),
				'PG' => __( 'Papua New Guinea', 'seo-breeze-local' ),
				'PY' => __( 'Paraguay', 'seo-breeze-local' ),
				'PE' => __( 'Peru', 'seo-breeze-local' ),
				'PH' => __( 'Philippines', 'seo-breeze-local' ),
				'PN' => __( 'Pitcairn', 'seo-breeze-local' ),
				'PL' => __( 'Poland', 'seo-breeze-local' ),
				'PT' => __( 'Portugal', 'seo-breeze-local' ),
				'QA' => __( 'Qatar', 'seo-breeze-local' ),
				'IE' => __( 'Republic of Ireland', 'seo-breeze-local' ),
				'RE' => __( 'Reunion', 'seo-breeze-local' ),
				'RO' => __( 'Romania', 'seo-breeze-local' ),
				'RU' => __( 'Russia', 'seo-breeze-local' ),
				'RW' => __( 'Rwanda', 'seo-breeze-local' ),
				'ST' => __( 'São Tomé and Príncipe', 'seo-breeze-local' ),
				'BL' => __( 'Saint Barthélemy', 'seo-breeze-local' ),
				'SH' => __( 'Saint Helena', 'seo-breeze-local' ),
				'KN' => __( 'Saint Kitts and Nevis', 'seo-breeze-local' ),
				'LC' => __( 'Saint Lucia', 'seo-breeze-local' ),
				'SX' => __( 'Saint Martin (Dutch part)', 'seo-breeze-local' ),
				'MF' => __( 'Saint Martin (French part)', 'seo-breeze-local' ),
				'PM' => __( 'Saint Pierre and Miquelon', 'seo-breeze-local' ),
				'VC' => __( 'Saint Vincent and the Grenadines', 'seo-breeze-local' ),
				'SM' => __( 'San Marino', 'seo-breeze-local' ),
				'SA' => __( 'Saudi Arabia', 'seo-breeze-local' ),
				'SN' => __( 'Senegal', 'seo-breeze-local' ),
				'RS' => __( 'Serbia', 'seo-breeze-local' ),
				'SC' => __( 'Seychelles', 'seo-breeze-local' ),
				'SL' => __( 'Sierra Leone', 'seo-breeze-local' ),
				'SG' => __( 'Singapore', 'seo-breeze-local' ),
				'SK' => __( 'Slovakia', 'seo-breeze-local' ),
				'SI' => __( 'Slovenia', 'seo-breeze-local' ),
				'SB' => __( 'Solomon Islands', 'seo-breeze-local' ),
				'SO' => __( 'Somalia', 'seo-breeze-local' ),
				'ZA' => __( 'South Africa', 'seo-breeze-local' ),
				'GS' => __( 'South Georgia/Sandwich Islands', 'seo-breeze-local' ),
				'KR' => __( 'South Korea', 'seo-breeze-local' ),
				'SS' => __( 'South Sudan', 'seo-breeze-local' ),
				'ES' => __( 'Spain', 'seo-breeze-local' ),
				'LK' => __( 'Sri Lanka', 'seo-breeze-local' ),
				'SD' => __( 'Sudan', 'seo-breeze-local' ),
				'SR' => __( 'Suriname', 'seo-breeze-local' ),
				'SJ' => __( 'Svalbard and Jan Mayen', 'seo-breeze-local' ),
				'SZ' => __( 'Swaziland', 'seo-breeze-local' ),
				'SE' => __( 'Sweden', 'seo-breeze-local' ),
				'CH' => __( 'Switzerland', 'seo-breeze-local' ),
				'SY' => __( 'Syria', 'seo-breeze-local' ),
				'TW' => __( 'Taiwan', 'seo-breeze-local' ),
				'TJ' => __( 'Tajikistan', 'seo-breeze-local' ),
				'TZ' => __( 'Tanzania', 'seo-breeze-local' ),
				'TH' => __( 'Thailand', 'seo-breeze-local' ),
				'TL' => __( 'Timor-Leste', 'seo-breeze-local' ),
				'TG' => __( 'Togo', 'seo-breeze-local' ),
				'TK' => __( 'Tokelau', 'seo-breeze-local' ),
				'TO' => __( 'Tonga', 'seo-breeze-local' ),
				'TT' => __( 'Trinidad and Tobago', 'seo-breeze-local' ),
				'TN' => __( 'Tunisia', 'seo-breeze-local' ),
				'TR' => __( 'Turkey', 'seo-breeze-local' ),
				'TM' => __( 'Turkmenistan', 'seo-breeze-local' ),
				'TC' => __( 'Turks and Caicos Islands', 'seo-breeze-local' ),
				'TV' => __( 'Tuvalu', 'seo-breeze-local' ),
				'UG' => __( 'Uganda', 'seo-breeze-local' ),
				'UA' => __( 'Ukraine', 'seo-breeze-local' ),
				'AE' => __( 'United Arab Emirates', 'seo-breeze-local' ),
				'GB' => __( 'United Kingdom (UK)', 'seo-breeze-local' ),
				'US' => __( 'United States (US)', 'seo-breeze-local' ),
				'UY' => __( 'Uruguay', 'seo-breeze-local' ),
				'UZ' => __( 'Uzbekistan', 'seo-breeze-local' ),
				'VU' => __( 'Vanuatu', 'seo-breeze-local' ),
				'VA' => __( 'Vatican', 'seo-breeze-local' ),
				'VE' => __( 'Venezuela', 'seo-breeze-local' ),
				'VN' => __( 'Vietnam', 'seo-breeze-local' ),
				'WF' => __( 'Wallis and Futuna', 'seo-breeze-local' ),
				'EH' => __( 'Western Sahara', 'seo-breeze-local' ),
				'WS' => __( 'Western Samoa', 'seo-breeze-local' ),
				'YE' => __( 'Yemen', 'seo-breeze-local' ),
				'ZM' => __( 'Zambia', 'seo-breeze-local' ),
				'ZW' => __( 'Zimbabwe', 'seo-breeze-local' ),
			);

			return $countries;
		}
	}
}
