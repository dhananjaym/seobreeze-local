<?php

if ( ! defined( 'ABSPATH' ) ) {
	die;
}
if ( ! class_exists( 'Seo_Breeze_Local_Core' ) ) {
    
    class Seo_Breeze_Local_Core{
        
        public function __construct() {
            global $wp_rewrite;
           if( seobreeze_has_multiple_locations() ){
                
                add_action( 'init', array( $this, 'create_custom_post_type' ) );
                add_action( 'init', array( $this, 'create_taxonomies' ) );
                add_action( 'admin_init', array( $this, 'exclude_taxonomy' ) );
                add_filter( 'seobreeze_primary_term_taxonomies', array(
					$this,
					'filter_seobreeze_primary_term_taxonomies',
				), 10, 3 ); 
            } 
            
        } 
        
        public function create_custom_post_type() {
			/* Locations as Custom Post Type */
                        $locations_label_singular = get_option( 'locations_label_singular' );
                        $locations_label_plural = get_option( 'locations_label_plural' );
			$label_singular = ! empty( $locations_label_singular ) ? $locations_label_singular : __( 'Location', 'seo-breeze-local' );
			$label_plural   = ! empty( $locations_label_plural ) ? $locations_label_plural : __( 'Locations', 'seo-breeze-local' );
			$labels         = array(
				'name'               => $label_plural,
				'singular_name'      => $label_singular,
				/* translators: %s extends to the singular label for the location post type */
				'add_new'            => sprintf( __( 'New %s', 'seo-breeze-local' ), $label_singular ),
				/* translators: %s extends to the singular label for the location post type */
				'new_item'           => sprintf( __( 'New %s', 'seo-breeze-local' ), $label_singular ),
				/* translators: %s extends to the singular label for the location post type */
				'add_new_item'       => sprintf( __( 'Add New %s', 'seo-breeze-local' ), $label_singular ),
				/* translators: %s extends to the singular label for the location post type */
				'edit_item'          => sprintf( __( 'Edit %s', 'seo-breeze-local' ), $label_singular ),
				/* translators: %s extends to the singular label for the location post type */
				'view_item'          => sprintf( __( 'View %s', 'seo-breeze-local' ), $label_singular ),
				/* translators: %s extends to the plural label for the location post type */
				'search_items'       => sprintf( __( 'Search %s', 'seo-breeze-local' ), $label_plural ),
				/* translators: %s extends to the plural label for the location post type */
				'not_found'          => sprintf( __( 'No %s found', 'seo-breeze-local' ), $label_plural ),
				/* translators: %s extends to the plural label for the location post type */
				'not_found_in_trash' => sprintf( __( 'No %s found in trash', 'seo-breeze-local' ), $label_plural ),
			);
                        $locations_slug = get_option( 'locations_slug' );
			$slug = ! empty( $locations_slug) ? $locations_slug : 'locations';

			$args_cpt = array(
				'labels'          => $labels,
				'public'          => true,
				'show_ui'         => true,
				'capability_type' => 'post',
				'hierarchical'    => false,
				'rewrite'         => array( 'slug' => esc_attr( $slug ) ),
				'has_archive'     => esc_attr( $slug ),
				'query_var'       => true,
				'supports'        => array(
					'title',
					'editor',
					'excerpt',
					'author',
					'thumbnail',
					'revisions',
					'custom-fields',
					'page-attributes',
					'publicize'
				),
			);

			$obj = register_post_type( 'seobreeze_locations', $args_cpt );
		}
                
        public function create_taxonomies() {

                global $wp_query;
                $location_post_type       = get_post_type_object( 'seobreeze_locations' );
                $post_type_singular_label = $location_post_type->labels->singular_name;
                $labels = array(
                        /* translators: %s extends to the singular label for the location category */
                        'name'              => sprintf( __( '%s categories', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'singular_name'     => sprintf( __( '%s category', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'search_items'      => sprintf( __( 'Search %s categories', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'all_items'         => sprintf( __( 'All %s categories', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'parent_item'       => sprintf( __( 'Parent %s category', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'parent_item_colon' => sprintf( __( 'Parent %s category:', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'edit_item'         => sprintf( __( 'Edit %s category', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'update_item'       => sprintf( __( 'Update %s category', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'add_new_item'      => sprintf( __( 'Add New %s category', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'new_item_name'     => sprintf( __( 'New %s category name', 'seo-breeze-local' ), $post_type_singular_label ),
                        /* translators: %s extends to the singular label for the location category */
                        'menu_name'         => apply_filters( 'seobreeze_locations_category_label', sprintf( __( '%s categories', 'seo-breeze-local' ), $post_type_singular_label ) ),
                );
                $locations_taxo_slug = get_option( 'locations_taxo_slug' );
                $slug = ! empty( $locations_taxo_slug) ? $locations_taxo_slug : 'locations-category';

                $args = array(
                        'hierarchical'          => true,
                        'labels'                => $labels,
                        'show_ui'               => true,
                        'show_admin_column'     => true,
                        'update_count_callback' => '_update_post_term_count',
                        'query_var'             => true,
                        'rewrite'               => array( 'slug' => esc_attr( $slug ) ),
                );

                // NOTE: when using the seobreeze_locations_category_slug filter, be sure to save the permalinks in order for it to work.
                register_taxonomy( 'seobreeze_locations_category', 'seobreeze_locations', $args );
        }
        public function exclude_taxonomy() {
            add_filter( 'seobreeze_woo_exclude_texonimies', array( $this, 'xml_sitemap_taxonomies' ), 10, 2 );
        }
        
        function xml_sitemap_taxonomies( $taxonomy_arr ) {
                $taxonomy_arr = array('seobreeze_locations_category');
		return $taxonomy_arr;
	}
                
        public function filter_seobreeze_primary_term_taxonomies( $taxonomies, $post_type, $all_taxonomies ) {
			if ( isset( $all_taxonomies['seobreeze_locations_category'] ) ) {
				$taxonomies['seobreeze_locations_category'] = $all_taxonomies['seobreeze_locations_category'];
			}

			return $taxonomies;
		}
        public function get_location_data( $post_id = null ) {
			$locations               = array();
			$locations['businesses'] = array();

			// Define base URL.
			$base_url = get_bloginfo('url');

			if ( seobreeze_has_multiple_locations() ) {
				$args = array(
					'post_type'      => 'seobreeze_locations',
					'posts_per_page' => -1,
					'fields'         => 'ids',
					'post_status'    => ( current_user_can( 'edit_posts' ) ? array( 'publish', 'draft' ) : '' ),
				);
				if ( null != $post_id ) {
					$args['posts_per_page'] = 1;
					$args['post__in']       = array( $post_id );
				}
				$posts = get_posts( $args );

				foreach ( $posts as $post_id ) {
					$business = array(
						'business_name'        => get_the_title( $post_id ),
						'business_type'        => get_post_meta( $post_id, '_seobreeze_business_type', true ),
						'business_address'     => get_post_meta( $post_id, '_seobreeze_business_address', true ),
						'business_address_2'   => get_post_meta( $post_id, '_seobreeze_business_address_2', true ),
						'business_city'        => get_post_meta( $post_id, '_seobreeze_business_city', true ),
						'business_state'       => get_post_meta( $post_id, '_seobreeze_business_state', true ),
						'business_zipcode'     => get_post_meta( $post_id, '_seobreeze_business_zipcode', true ),
						'business_country'     => get_post_meta( $post_id, '_seobreeze_business_country', true ),
						'business_phone'       => get_post_meta( $post_id, '_seobreeze_business_phone', true ),
						'business_phone_2nd'   => get_post_meta( $post_id, '_seobreeze_business_phone_2nd', true ),
						'business_fax'         => get_post_meta( $post_id, '_seobreeze_business_fax', true ),
						'business_email'       => get_post_meta( $post_id, '_seobreeze_business_email', true ),
						'business_url'         => get_post_meta( $post_id, '_seobreeze_business_url', true ),
						'business_description' => seobreeze_local_get_excerpt( $post_id ),
						'coords'               => array(
							'lat'  => get_post_meta( $post_id, '_seobreeze_coordinates_lat', true ),
							'long' => get_post_meta( $post_id, '_seobreeze_coordinates_long', true ),
						),
						'post_id'              => $post_id,
					);

					$is_postal_address             = get_post_meta( $post_id, '_seobreeze_is_postal_address', true );
					$business['is_postal_address'] = $is_postal_address == '1';

					if ( empty( $business['business_url'] ) ) {
						$business['business_url'] = get_permalink( $post_id );
					}

					// If not Business type is chosen, set allback to general Business type.
					if ( '' == $business['business_type'] ) {
						$business['business_type'] = 'LocalBusiness';
					}

					array_push( $locations['businesses'], $business );
				}
			}
			else {
				
                                $location_name = get_option( 'location_name' );
                                $business_type = get_option( 'business_type' );
                                $location_address = get_option( 'location_address' );
                                $location_address_2 = get_option( 'location_address_2' );
                                $location_city = get_option( 'location_city' );
                                $location_state = get_option( 'location_state' );
                                $location_zipcode = get_option( 'location_zipcode' );
                                $location_country = get_option( 'location_country' );
                                $location_phone = get_option( 'location_phone' );
                                $location_phone_2nd = get_option( 'location_phone_2nd' );
                                $location_fax = get_option( 'location_fax' );
                                $location_email = get_option( 'location_email' );
				$business = array(
					'business_name'        => isset( $location_name ) ? $location_name : '',
					'business_type'        => isset( $business_type ) ? $business_type : '',
					'business_address'     => isset( $location_address ) ? $location_address : '',
					'business_address_2'   => isset( $location_address_2 ) ? $location_address_2 : '',
					'business_city'        => isset( $location_city ) ? $location_city : '',
					'business_state'       => isset( $location_state ) ? $location_state : '',
					'business_zipcode'     => isset( $location_zipcode ) ? $location_zipcode : '',
					'business_country'     => isset( $location_country ) ? $location_country : '',
					'business_phone'       => isset( $location_phone ) ? $location_phone : '',
					'business_phone_2nd'   => isset( $location_phone_2nd ) ? $location_phone_2nd : '',
					'business_fax'         => isset( $location_fax ) ? $location_fax : '',
					'business_email'       => isset( $location_email ) ? $location_email : '',
					'business_description' => get_option( 'blogname' ) . ' - ' . get_option( 'blogdescription' ),
					'business_url'         => $base_url,
					'coords'               => array(
						'lat'  => get_option( 'location_coords_lat' ),
						'long' => get_option( location_coords_long ),
					),
				);

				// If not Business type is chosen, set allback to general Business type.
				if ( '' == $business['business_type'] ) {
					$business['business_type'] = 'LocalBusiness';
				}

				array_push( $locations['businesses'], $business );
			}

			$base = $GLOBALS['wp_rewrite']->using_index_permalinks() ? 'index.php/' : '';

			$locations['business_name'] = get_option( 'blogname' );
			$locations['kml_name']      = 'Locations for ' . $locations['business_name'] . '.';
			$locations['kml_url']       = home_url( $base . '/locations.kml' );
			$locations['kml_website']   = $base_url;
			$locations['author']        = get_option( 'blogname' );

			return $locations;
		}
                public function get_geo_data( $location_info, $force_update = false, $post_id = 0 ) {
                        $address_format = get_option( 'address_format' );
			$address_format = ! empty( $address_format ) ? $address_format : 'address-state-postal';
			$format         = new Seobreeze_Local_Address_Format();
			$full_address   = $format->get_address_format( $address_format, array(
				'business_address' => $location_info['_seobreeze_business_address'],
				'oneline'          => false,
				'business_zipcode' => $location_info['_seobreeze_business_zipcode'],
				'business_city'    => $location_info['_seobreeze_business_city'],
				'business_state'   => $location_info['_seobreeze_business_state'],
				'show_state'       => true,
				'escape_output'    => false,
				'use_tags'         => false,
			) );
			$full_address .= ', ' .get_country( '_seobreeze_business_country' );

			$coordinates = array();

			if ( ( $post_id === 0 || empty( $post_id ) ) && isset( $location_info['_seobreeze_post_id'] ) ) {
				$post_id = $location_info['_seobreeze_post_id'];
			}

			if ( $force_update || empty( $location_info['_seobreeze_coords']['lat'] ) || empty( $location_info['_seobreeze_coords']['long'] ) ) {

				$results = seobreeze_geocode_address( $full_address );

				if ( is_wp_error( $results ) ) {
					return false;
				}

				if ( isset( $results->results[0] ) && ! empty( $results->results[0] ) ) {
					$coordinates['lat']  = $results->results[0]->geometry->location->lat;
					$coordinates['long'] = $results->results[0]->geometry->location->lng;

					if ( seobreeze_has_multiple_locations() && $post_id !== 0 ) {
						// Set lat & long.
						update_post_meta( $post_id, '_seobreeze_coordinates_lat', $coordinates['lat'] );
						update_post_meta( $post_id, '_seobreeze_coordinates_long', $coordinates['long'] );
					}
					else {
						

						update_option( 'location_coords_lat', $coordinates['lat'] );
                                                update_option( 'location_coords_long', $coordinates['long'] );
					}
				}
			}
			else {
				$coordinates['lat']  = $location_info['_seobreeze_coords']['lat'];
				$coordinates['long'] = $location_info['_seobreeze_coords']['long'];
			}

			$return_array['coords']       = $coordinates;
			$return_array['full_address'] = $full_address;

			return $return_array;
		}
    }
}