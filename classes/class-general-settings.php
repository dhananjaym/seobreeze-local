<?php

if ( ! defined( 'ABSPATH' ) ) {
	die;
}
if( ! class_exists( 'seobreze_local_seo_settings' ) ) {
    
    class seobreze_local_seo_settings{
        
        var $short_name;
        private $seobreeze_local_core;
        private $wpseo_local_timezone_repository;
        
        public function __construct() {
            $this->get_core();
            $this->get_timezone_repository();
            
            $this->short_name = 'seobreeze-local-seo';
            if(is_admin()){
                add_action( 'admin_menu', array( $this, 'register_settings_page' ), 20 );
                add_action( "admin_print_scripts", array( $this, 'enqueue_css_scripts' ) );
                add_action( 'admin_init', array( $this, 'register_seo_breeze_local_settings' ) );
                add_action( 'admin_enqueue_scripts', array($this,'meta_box_scripts') );
            }
            
            add_action( 'pre_update_option_seobreeze_loc_setting', array( $this, 'update_timezone' ), 10, 2 );
            add_action( 'update_option_location_coords_lat', array( $this, 'update_lat_long' ), 10, 2 );
            add_action( 'update_option_location_coords_long', array( $this, 'update_lat_long' ), 10, 2 );
        }
        private function get_core() {
               
                $this->seobreeze_local_core = new Seo_Breeze_Local_Core();
        }
        function meta_box_scripts() {

	    global $post;

	    wp_enqueue_media( array(
	        'post' => $post->ID,
	    ) );

	}

        public function enqueue_css_scripts(){
            wp_enqueue_script('seo-local-tether-min', plugins_url().'/seobreeze/js/tether.min.js', array('jquery'));
            wp_enqueue_script('seo-local-bootstrap-js', plugins_url().'/seobreeze/js/bootstrap.min.js',array('seo-local-tether-min'));
            wp_enqueue_script('seo-local-js', plugins_url('/seobreeze-local/js/seobreeze-local.js'),array('jquery'));
            wp_enqueue_style('seo-local-bootstrap-css',plugins_url().'/seobreeze/css/bootstrap.min.css');
            wp_enqueue_style('seo-local-module-css',plugins_url().'/seobreeze/seobreeze_module.css',array('seo-local-bootstrap-css'));
            wp_enqueue_style('seo-local-common-css',plugins_url().'/seobreeze/css/common.css');
           // if(is_page('seobreeze-local-seo')){
                wp_enqueue_style('seo-local-css',plugins_url('/seobreeze-local/css/seobreeze-local.css'));
            //}
            
        }
        public function register_settings_page() {
            
            add_submenu_page( 'seobreeze/seobreeze_class.php', __( 'Local SEO Settings', 'seo-breeze-local' ), __( 'Local SEO', 'seo-breeze-local' ), 'manage_options', $this->short_name, array($this,'admin_panel_seo_breeze_local') );
        
        }
        function local_seo_has_multiple_locations() {
            
            $options = get_option( 'use_multiple_locations' );

            return isset($options) && $options == '1';
        }
        function local_seo_has_hours_enabled() {
            
            $options = get_option( 'hide_opening_hours' );

            return isset($options) && $options == '1';
        }
        function local_seo_has_two_opening_hours_enabled() {
            
            $options = get_option( 'multiple_opening_hours' );

            return isset($options) && $options == '1';
        }
        public function register_seo_breeze_local_settings(){
            register_setting( 'seo-breeze-local-settings-group', 'use_multiple_locations' );
            register_setting( 'seo-breeze-local-settings-group', 'locations_slug' );
            register_setting( 'seo-breeze-local-settings-group', 'locations_label_singular' );
            register_setting( 'seo-breeze-local-settings-group', 'locations_label_plural' );
            register_setting( 'seo-breeze-local-settings-group', 'locations_taxo_slug' );
            
            register_setting( 'seo-breeze-local-settings-group', 'location_name' );
            register_setting( 'seo-breeze-local-settings-group', 'business_type' );
            register_setting( 'seo-breeze-local-settings-group', 'location_address' );
            register_setting( 'seo-breeze-local-settings-group', 'location_address_2' );
            register_setting( 'seo-breeze-local-settings-group', 'location_city' );
            register_setting( 'seo-breeze-local-settings-group', 'location_state' );
            register_setting( 'seo-breeze-local-settings-group', 'location_zipcode' );
            register_setting( 'seo-breeze-local-settings-group', 'location_country' );
            register_setting( 'seo-breeze-local-settings-group', 'location_phone' );
            register_setting( 'seo-breeze-local-settings-group', 'location_phone_2nd' );
            register_setting( 'seo-breeze-local-settings-group', 'location_fax' );
            register_setting( 'seo-breeze-local-settings-group', 'location_email' );
            register_setting( 'seo-breeze-local-settings-group', 'location_url' );
            register_setting( 'seo-breeze-local-settings-group', 'location_vat_id' );
            register_setting( 'seo-breeze-local-settings-group', 'location_tax_id' );
            register_setting( 'seo-breeze-local-settings-group', 'location_coc_id' );
            register_setting( 'seo-breeze-local-settings-group', 'location_price_range' );
            
            register_setting( 'seo-breeze-local-settings-group', 'location_coords_lat' );
            register_setting( 'seo-breeze-local-settings-group', 'location_coords_long' );
            register_setting( 'seo-breeze-local-settings-group', 'location_price_range' );
            register_setting( 'seo-breeze-local-settings-group', 'location_price_range' );
            register_setting( 'seo-breeze-local-settings-group', 'location_price_range' );
            register_setting( 'seo-breeze-local-settings-group', 'location_price_range' );
            register_setting( 'seo-breeze-local-settings-group', 'location_price_range' );
                
            register_setting( 'seo-breeze-local-settings-group', 'media_url_business_image' );
              
            register_setting( 'seo-breeze-local-settings-group', 'hide_opening_hours' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_24h' );
            register_setting( 'seo-breeze-local-settings-group', 'multiple_opening_hours' );
            
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_monday_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_monday_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_monday_second_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_monday_second_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_tuesday_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_tuesday_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_tuesday_second_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_tuesday_second_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_wednesday_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_wednesday_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_wednesday_second_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_wednesday_second_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_thursday_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_thursday_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_thursday_second_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_thursday_second_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_friday_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_friday_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_friday_second_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_friday_second_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_saturday_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_saturday_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_saturday_second_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_saturday_second_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_sunday_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_sunday_to' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_sunday_second_from' );
            register_setting( 'seo-breeze-local-settings-group', 'opening_hours_sunday_second_to' );
            register_setting( 'seo-breeze-local-settings-group', 'sl_num_results' );
         
            register_setting( 'seo-breeze-local-settings-group', 'show_route_label' );
            register_setting( 'seo-breeze-local-settings-group', 'default_country' );
            register_setting( 'seo-breeze-local-settings-group', 'address_format' );
            register_setting( 'seo-breeze-local-settings-group', 'map_view_style' );
            register_setting( 'seo-breeze-local-settings-group', 'detect_location' );
            register_setting( 'seo-breeze-local-settings-group', 'unit_system' ); 
            register_setting( 'seo-breeze-local-settings-group', 'media_url_custom_marker' );
            
            register_setting( 'seo-breeze-local-settings-group', 'api_key_browser' ); 
            register_setting( 'seo-breeze-local-settings-group', 'api_key' ); 
            
            register_setting( 'seo-breeze-local-settings-group', 'updatelocalseosetting' );
            
        }

        public function admin_panel_seo_breeze_local(){
            ?>
            <div class="local-seo-wrap-seo-breeze">
            <div id="seobreeze_settings_header">
                <div id="icon-seobreeze" class="icon32"><br></div>
                        <h2>Local SEO</h2>
            </div>
            <div class="seobreeze_options_wrapper seobreeze_settings_left"> 
                
                <form method="post" action="options.php">
                <?php settings_fields( 'seo-breeze-local-settings-group' ); ?>
                <?php do_settings_sections( 'seo-breeze-local-settings-group' ); ?>
                
                <input type="hidden" name="updatelocalseosetting" id="updatelocalseosetting" value="<?php echo rand(50,100);?>">
                
            <ul class="nav nav-tabs" data-tabs="tabs">
                <li class="active" data-toggle="tab" data-target="#local-seo-general-settings"><a>General settings</a></li>
                <li class="" data-toggle="tab" data-target="#api-local-seo-settings"><a>API settings</a></li>
            </ul>
            <div class="tab-content">
                <div id="local-seo-general-settings" class="tab-pane active">
                    <h3 class="hndle"><span>General settings</span></h3>
                    <p class="description">If you have more than one location, you can enable this feature. SEO Breeze will create a new Custom Post Type for you where you can manage your locations. If it's not enabled you can enter your address details below. These fields will be ignored when you enable this option.</p>
                    
                    <label class="textinput-seo-local" for="use_multiple_locations">Use multiple locations:</label>
                    <input type="checkbox" class="seo-local-text" name="use_multiple_locations" id="use_multiple_locations" value="1" <?php checked( get_option('use_multiple_locations'),1 ); ?> />
                    <br class="clear">

                    <div id="multiple-locations-settings" <?php echo ($this->local_seo_has_multiple_locations()) ? "":"style='display: none'"; ?>>
                    <label class="textinput-seo-local" for="locations_slug">Locations slug:</label>
                    <input type="text"  class="seo-local-text" name="locations_slug" value="<?php echo esc_attr( get_option('locations_slug') ); ?>" />
                    <p class="desc label" style="border: 0;margin-bottom: 0; padding-bottom: 0;">The slug for your location pages. Default slug is <code>locations</code>.<br></p>
                    <br class="clear">
                    <label class="textinput-seo-local" for="locations_label_singular">Locations label singular:</label>                    
                    <input type="text"  class="seo-local-text" name="locations_label_singular" value="<?php echo esc_attr( get_option('locations_label_singular') ); ?>" />
                    <p class="desc label" style="border: 0; margin-bottom: 0; padding-bottom: 0;">The singular label for your location pages. Default label is <code>Location</code>.<br></p>
                    <br class="clear">
                    <label class="textinput-seo-local" for="locations_label_plural">Locations label plural:</label>
                    <input type="text"  class="seo-local-text" name="locations_label_plural" value="<?php echo esc_attr( get_option('locations_label_plural') ); ?>" />
                    <p class="desc label" style="border: 0; margin-bottom: 0; padding-bottom: 0;">The plural label for your location pages. Default label is <code>Locations</code>.<br></p>
                    <br class="clear">
                    <label class="textinput-seo-local" for="locations_taxo_slug">Locations category slug:</label>
                    <input type="text"  class="seo-local-text" name="locations_taxo_slug" value="<?php echo esc_attr( get_option('locations_taxo_slug') ); ?>" />
                    <p class="desc label" style="border: 0; margin-bottom: 0; padding-bottom: 0;">The slug for your location categories. Default slug is <code>locations-category</code>.<br></p>
                    <br class="clear">
                    </div>
                    <div id="single-location-settings" <?php echo ($this->local_seo_has_multiple_locations()) ? "style='display: none'":""; ?>>
                    
                    <label class="textinput-seo-local" for="location_name">Locations Name:</label>
                    <input type="text"  class="seo-local-text" name="location_name" value="<?php echo esc_attr( get_option('location_name') ); ?>" />
                    <br class="clear">
                    
                    <label class="selectinput" for="business_type">Business Type:</label>
                    <select name="business_type" class="seo-local-text" id="business_type">
                        <?php
                        $business_type_array = get_local_business_types();
                        foreach ($business_type_array as $key => $value) {
                            $checked = ($key == get_option('business_type'))? 'selected="selected"':'';
                            echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                        }
                        ?>
                    </select>
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_address">Business Address:</label>
                    <input type="text"  class="seo-local-text" name="location_address" value="<?php echo esc_attr( get_option('location_address') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_address_2">Business Address Line 2 :</label>
                    <input type="text" class="seo-local-text" name="location_address_2" value="<?php echo esc_attr( get_option('location_address_2') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_city">Business City :</label>
                    <input type="text"  class="seo-local-text" name="location_city" value="<?php echo esc_attr( get_option('location_city') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_state">Business State:</label>
                    <input type="text"  class="seo-local-text" name="location_state" value="<?php echo esc_attr( get_option('location_state') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_zipcode">Business Zip code:</label>
                    <input type="text" class="seo-local-text" name="location_zipcode" value="<?php echo esc_attr( get_option('location_zipcode') ); ?>" />
                    <br class="clear">
                    
                    <label class="selectinput" for="location_country">Business Country:</label>
                    <select name="location_country" class="seo-local-text" id="location_country">
                        <?php
                        $business_country_array = get_country_array();
                        foreach ($business_country_array as $key => $value) {
                            $checked = ($key == get_option('location_country'))? 'selected="selected"':'';
                            echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                        }
                        ?>
                    </select>
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_phone">Business Phone:</label>
                    <input type="text" class="seo-local-text" name="location_phone" value="<?php echo esc_attr( get_option('location_phone') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_phone_2nd">Bussiness Phone 2:</label>
                    <input type="text" class="seo-local-text" name="location_phone_2nd" value="<?php echo esc_attr( get_option('location_phone_2nd') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_fax">Bussiness Fax:</label>
                    <input type="text" class="seo-local-text" name="location_fax" value="<?php echo esc_attr( get_option('location_fax') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_email">Business Email:</label>
                    <input type="text" class="seo-local-text" name="location_email" value="<?php echo esc_attr( get_option('location_email') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_url">Business URL:</label>
                    <input type="text" class="seo-local-text" name="location_url" value="<?php echo esc_attr( get_option('location_url') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_vat_id">VAT ID:</label>
                    <input type="text" class="seo-local-text" name="location_vat_id" value="<?php echo esc_attr( get_option('location_vat_id') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_tax_id">Tax ID:</label>
                    <input type="text" class="seo-local-text" name="location_tax_id" value="<?php echo esc_attr( get_option('location_tax_id') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_coc_id">Chamber of Commerce ID:</label>
                    <input type="text" class="seo-local-text" name="location_coc_id" value="<?php echo esc_attr( get_option('location_coc_id') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_price_range">Price range:</label>
                    <input type="text" class="seo-local-text" name="location_price_range" value="<?php echo esc_attr( get_option('location_price_range') ); ?>" />
                    <br class="clear">
                    
                    <p>You can enter the lat/long coordinates yourself. If you leave them empty they will be calculated automatically. If you want to re-calculate these fields, please make them blank before saving this location.</p>
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_coords_lat">Latitude:</label>
                    <input type="text" class="seo-local-text" name="location_coords_lat" value="<?php echo esc_attr( get_option('location_coords_lat') ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="location_coords_long">Longitude:</label>
                    <input type="text" class="seo-local-text" name="location_coords_long" value="<?php echo esc_attr( get_option('location_coords_long') ); ?>" />
                    <br class="clear">
                    
                    </div>
                   <div id="wpseo-local-business_image" class="wpseo-local-business_image-wrapper">
                    <label class="textinput-seo-local" for="media_url_business_image">Business Image:</label>
                    <div class="image-box">
                        <img width="auto" id="media_src_business_image" height="75px" src="<?php if(get_option("media_url_business_image") != "") echo get_option("media_url_business_image"); else echo plugins_url('/seobreeze-local/images/default.jpg'); ?>">
                        <input type="hidden" id="media_url_business_image" name="media_url_business_image" value="<?php echo get_option("media_url_business_image");?>">
                    </div>
                    <button type="button" id="business_image" class="upload_que_img">Upload</button>
                    <button type="button" id="business_image" class="remove_que_img">Remove</button>
                   </div>
                    <div class="clear"></div>
                    <div id="opening-hours-container" <?php echo ($this->local_seo_has_multiple_locations()) ? "style='display: none'":""; ?>>
                        <div class="loca-seo-heading"><h4>Opening hours</h4></div>
                        
                        <label class="textinput-seo-local" for="hide_opening_hours">Hide opening hours option:</label>
                        <input type="checkbox" name="hide_opening_hours" id="hide_opening_hours" value="1" <?php checked( get_option('hide_opening_hours'),1 ); ?> />
                        <br class="clear">
                        <div id="opening-hours-inner" <?php echo ($this->local_seo_has_hours_enabled()) ? "style='display: none'":""; ?>>
                        <label class="textinput-seo-local" for="opening_hours_24h">Use 24h format:</label>
                        <input type="checkbox" name="opening_hours_24h" id="opening_hours_24h" value="1" <?php checked( get_option('opening_hours_24h'),1 ); ?> />
                        <br class="clear">
                    
                        <label class="textinput-seo-local" for="multiple_opening_hours">I have two sets of opening hours per day:</label>
                        <input type="checkbox" name="multiple_opening_hours" id="multiple_opening_hours" value="1" <?php checked( get_option('multiple_opening_hours'),1 ); ?> />
                        <br class="clear">
                        
                        <p>If a specific day only has one set of opening hours, please set the second set for that day to <strong>closed</strong></p>
                      
                        <?php $this->opening_hours_setting() ?> 
                        </div>
                    </div>
                    <div id="sl-settings"<?php echo ($this->local_seo_has_multiple_locations()) ? "":"style='display: none'"; ?>>
                        <div class="loca-seo-heading"><h4>Store locator settings</h4></div>
                        <label class="textinput-seo-local" for="sl_num_results">Number of results:</label>
                        <input type="text" class="seo-local-text" name="sl_num_results" value="<?php echo esc_attr( get_option('sl_num_results') ); ?>" />
                        <br class="clear">
                    </div>
                    
                    <div id="wpseo-local-advanced">
                        <div class="loca-seo-heading"><h4>Advanced settings</h4></div>
                        <label class="selectinput" for="unit_system">Unit System:</label>
                        <select name="unit_system" class="seo-local-text" id="unit_system">
                            <?php
                            $unit_system_array = get_unit_system_array();
                            foreach ($unit_system_array as $key => $value) {
                                $checked = ($key == get_option('unit_system'))? 'selected="selected"':'';
                                echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                            }
                            ?>
                        </select>
                        <br class="clear">
                        
                        <?php
                        if ( true == is_ssl() ) {?>
                                <label class="textinput-seo-local" for="detect_location">Use multiple locations:</label>
                                <input type="checkbox" name="detect_location" id="detect_location" value="1" <?php checked( get_option('detect_location'),1 ); ?> />
                                <br class="clear">
                        <?php        
                        }
			else {
				echo '<label class="textinput-seo-local" for="detect_location">Location detection:</label>';
				echo '<p class="desc label" style="border:none; margin-bottom: 0;"><em>' . __( 'This option only works on HTTPS websites.', 'seo-breeze-local' ) . '</em></p>';
                                echo '<br class="clear">';
                                
                        }
                        ?>
                        
                        <label class="selectinput" for="map_view_style">Default map style:</label>
                        <select name="map_view_style" class="seo-local-text" id="map_view_style">
                            <?php
                            $get_map_view_style_array = get_map_view_style();
                            foreach ($get_map_view_style_array as $key => $value) {
                                $checked = ($key == get_option('map_view_style'))? 'selected="selected"':'';
                                echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                            }
                            ?>
                        </select>
                        <br class="clear">

                        <label class="selectinput" for="address_format">Address Format:</label>
                        <select name="address_format" class="seo-local-text" id="address_format">
                            <?php
                            $address_format_array = get_address_format_array();
                            foreach ($address_format_array as $key => $value) {
                                $checked = ($key == get_option('address_format'))? 'selected="selected"':'';
                                echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                            }
                            ?>
                        </select>
                        <br class="clear">
                    
                        <label class="selectinput" for="default_country">Default country:</label>
                        <select name="default_country" class="seo-local-text" id="default_country">
                            <option value="" selected="selected"></option>
                            <?php
                            $default_country_array = get_country_array();
                            foreach ($default_country_array as $key => $value) {
                                $checked = ($key == get_option('default_country'))? 'selected="selected"':'';
                                echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                            }
                            ?>
                        </select>
                        <br class="clear">
                                                
                        <label class="textinput-seo-local" for="show_route_label">"Show route" label:</label>
                        <input type="text" class="seo-local-text" name="show_route_label" value="<?php echo esc_attr( get_option('show_route_label') ); ?>" />
                        <br class="clear">
                    
                        <label class="textinput-seo-local" for="media_url_custom_marker">Custom Marker:</label>
                        <div class="image-box">
                            <img width="auto" id="media_src_custom_marker" height="75px" src="<?php if(get_option("media_url_custom_marker") != "") echo get_option("media_url_custom_marker"); else echo plugins_url('/seobreeze-local/images/default.jpg'); ?>">
                            <input type="hidden" id="media_url_custom_marker" name="media_url_custom_marker" value="<?php echo get_option("media_url_custom_marker");?>">
                        </div>
                        <button type="button" id="custom_marker" class="upload_que_img">Upload</button>
                        <button type="button" id="custom_marker" class="remove_que_img">Remove</button>
                        <br class="clear">
                    
                    </div>
                </div>
                
                <div id="api-local-seo-settings" class="tab-pane ">
                    <div class="loca-seo-heading"><h4>API Keys</h4></div>
                    <h6>API key for Google Maps</h6>  
                    <label class="textinput-seo-local" for="api_key_browser">Google Maps API browser key <br>(required):</label>
                    <input type="text" class="seo-local-text" name="api_key_browser" value="<?php echo esc_attr( get_option('api_key_browser') ); ?>" />
                    <br class="clear">
                    <p>A Google Maps browser key is required to show Google Maps and make use of the Store Locator. For more information on how to create and set your Google Maps browser key, open the help center.</p>
                    <br class="clear">
                    
                    <h6>API key for geocoding</h6>
                    <label class="textinput-seo-local" for="api_key">Google Maps API server key <br>(Not Required):</label>
                    <input type="text" class="seo-local-text" name="api_key" value="<?php echo esc_attr( get_option('api_key') ); ?>" />
                    <br class="clear">
                    <p>A Google Maps Geocoding server key will be required to calculate the latitude and longitude of an address. With this key, you can retrieve the geographical location of up to 2,500 addresses per 24 hours. For more information on how to create and set your Google Maps Geocoding server key, open the help center</p>
                    <br class="clear">

                </div>
            </div>
                    
                <?php submit_button(); ?>
                </form>
            </div>
            </div>
           <?php     
        }
        
        public function opening_hours_setting(){
            $week_days = array("monday"=>"Monday","tuesday"=>"Tuesday","wednesday"=>"Wednesday","thursday"=>"Thursday","friday"=>"Friday","saturday"=>"Saturday","sunday"=>"Sunday");
            foreach ( $week_days as $key => $day ) {
				$field_name        = 'opening_hours_' . $key;
                                $val_from =  get_option( $field_name . '_from' );
                                $val_to = get_option( $field_name . '_to' );
                                $val_second_from = get_option( $field_name . '_second_from' );
                                $val_second_to = get_option( $field_name . '_second_to' );
				$value_from        = isset( $val_from ) ? esc_attr( get_option( $field_name . '_from' ) ) : '09:00';
				$value_to          = isset( $val_to ) ? esc_attr( get_option( $field_name . '_to' ) ) : '17:00';
				$value_second_from = isset( $val_second_from ) ? esc_attr( get_option( $field_name . '_second_from' ) ) : '09:00';
				$value_second_to   = isset( $val_second_to ) ? esc_attr( get_option( $field_name . '_second_to' ) ) : '17:00';

                                echo '<div id="opening-hours-'.$key.'" class="opening-hours">';
				echo '<label class="textinput-seo-local">' . $day . ':</label>';
				echo '<select class="openinghours_from" style="width: 100px;" id="' . $field_name . '_from" name="' . $field_name . '_from">';
				echo wpseo_show_hour_options( get_option('opening_hours_24h'), $value_from );
				echo '</select><span id="' . $field_name . '_to_wrapper"> - ';
				echo '<select class="openinghours_to" style="width: 100px;" id="' . $field_name . '_to" name="' . $field_name . '_to">';
				echo wpseo_show_hour_options( get_option("opening_hours_24h"), $value_to );
				echo '</select>';

                                $enable_class =  ($this->local_seo_has_two_opening_hours_enabled()) ? "":"style='display: none'";
                                echo '<div id="opening-hours-second-'.$key.'" class="opening-hours-second " '.$enable_class.'>';
				echo '<select class="openinghours_from_second" style="width: 100px;" id="' . $field_name . '_second_from" name="' . $field_name . '_second_from">';
				echo wpseo_show_hour_options( get_option("opening_hours_24h"), $value_second_from );
				echo '</select><span id="' . $field_name . '_second_to_wrapper"> - ';
				echo '<select class="openinghours_to_second" style="width: 100px;" id="' . $field_name . '_second_to" name="' . $field_name . '_second_to">';
				echo wpseo_show_hour_options( get_option("opening_hours_24h"), $value_second_to );
				echo '</select>';
				echo '</div></div>';
                                echo '<div class="clear"></div>';
            }
        }
        
        private function get_timezone_repository() {
            
            $wpseo_local_timezone_repository       = new Seobreeze_Local_Timezone_Repository();
            $this->wpseo_local_timezone_repository = $wpseo_local_timezone_repository;
	}
        
        public function update_timezone() {
               $timezone = $this->wpseo_local_timezone_repository->get_coords_timezone();

                if ( ! empty( $timezone ) ) {
                        $new_value['location_timezone'] = $timezone;
                }
        }
        public function update_lat_long($new_value, $old_value) {
                $location_address = get_option( 'location_address' );
                $location_city = get_option( 'location_city' );
                $location_state = get_option( 'location_state' );
                $location_zipcode = get_option( 'location_zipcode' );
                $location_country = get_option( 'location_country' );
                $location_coordinates = $this->seobreeze_local_core->get_geo_data( array(
                        '_seobreeze_business_address' => isset( $location_address ) ? esc_attr( $location_address ) : '',
                        '_seobreeze_business_city'    => isset( $location_city ) ? esc_attr( $location_city ) : '',
                        '_seobreeze_business_state'   => isset( $location_state ) ? esc_attr( $location_state ) : '',
                        '_seobreeze_business_zipcode' => isset( $location_zipcode ) ? esc_attr( $location_zipcode ) : '',
                        '_seobreeze_business_country' => isset( $location_country ) ? esc_attr( $location_country ) : '',
                ), true );
                if ( ! empty( $location_coordinates['coords'] ) ) {
                        $new_value['location_coords_lat']  = str_replace( ',', '.', $location_coordinates['coords']['lat'] );
                        $new_value['location_coords_long'] = str_replace( ',', '.', $location_coordinates['coords']['long'] );
                }


                return $new_value;
            
        }
    }
    
}