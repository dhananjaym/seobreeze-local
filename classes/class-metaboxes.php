<?php

if ( ! defined( 'ABSPATH' ) ) {
	die;
}
if ( ! class_exists( 'Seo_Breeze_Local_Metaboxes' ) ) {
    
    class Seo_Breeze_Local_Metaboxes{
        private $seobreeze_local_timezone_repository;
        public function __construct() {
            $this->get_timezone_repository();
            
          add_action( 'add_meta_boxes', array( $this, 'add_location_metaboxes' ) );
          add_action( 'save_post', array( $this, 'seobreeze_locations_save_meta' ), 1, 2 );
          add_action( 'admin_enqueue_scripts', array($this,'meta_box_scripts') );
          
          add_action( 'media_buttons', array( &$this, 'add_media_buttons' ), 20 );
          add_action( 'admin_footer', array( &$this, 'add_mce_popup' ) );
        } 
        function add_location_metaboxes() {
			add_meta_box( 'seobreeze_locations', __( 'Business address details', 'seo-breeze-local' ), array(
				&$this,
				'metabox_locations',
			), 'seobreeze_locations', 'normal', 'high' );
	}
        private function get_timezone_repository() {
            $wpseo_local_timezone_repository       = new Seobreeze_Local_Timezone_Repository();
            $this->seobreeze_local_timezone_repository = $wpseo_local_timezone_repository;
        }
        function metabox_locations(){
            $post_id = get_the_ID();
            echo '<div style="overflow: hidden;" id="seobreeze-local-metabox">';
            
            echo '<input type="hidden" name="locationsmeta_noncename" id="locationsmeta_noncename" value="' . wp_create_nonce( plugin_basename( __FILE__ ) ) . '" />';
            
            // Get the location data if its already been entered.
			$business_type                   = get_post_meta( $post_id, '_seobreeze_business_type', true );
			$business_address                = get_post_meta( $post_id, '_seobreeze_business_address', true );
			$business_address_2              = get_post_meta( $post_id, '_seobreeze_business_address_2', true );
			$business_city                   = get_post_meta( $post_id, '_seobreeze_business_city', true );
			$business_state                  = get_post_meta( $post_id, '_seobreeze_business_state', true );
			$business_zipcode                = get_post_meta( $post_id, '_seobreeze_business_zipcode', true );
			$business_country                = get_post_meta( $post_id, '_seobreeze_business_country', true );
			$business_phone                  = get_post_meta( $post_id, '_seobreeze_business_phone', true );
			$business_phone_2nd              = get_post_meta( $post_id, '_seobreeze_business_phone_2nd', true );
			$business_fax                    = get_post_meta( $post_id, '_seobreeze_business_fax', true );
			$business_email                  = get_post_meta( $post_id, '_seobreeze_business_email', true );
			$notes_1                         = get_post_meta( $post_id, '_seobreeze_business_notes_1', true );
			$notes_2                         = get_post_meta( $post_id, '_seobreeze_business_notes_2', true );
			$notes_3                         = get_post_meta( $post_id, '_seobreeze_business_notes_3', true );
			$business_url                    = get_post_meta( $post_id, '_seobreeze_business_url', true );
			$business_location_logo          = get_post_meta( $post_id, 'media_url_seobreeze_business_location_logo', true );
			$business_location_custom_marker = get_post_meta( $post_id, 'media_url_seobreeze_custom_marker', true );
			$business_vat_id                 = get_post_meta( $post_id, '_seobreeze_business_vat_id', true );
			$business_tax_id                 = get_post_meta( $post_id, '_seobreeze_business_tax_id', true );
			$business_coc_id                 = get_post_meta( $post_id, '_seobreeze_business_coc_id', true );
			$business_price_range            = get_post_meta( $post_id, '_seobreeze_business_price_range', true );
			$coordinates_lat                 = get_post_meta( $post_id, '_seobreeze_coordinates_lat', true );
			$coordinates_long                = get_post_meta( $post_id, '_seobreeze_coordinates_long', true );
			$is_postal_address               = get_post_meta( $post_id, '_seobreeze_is_postal_address', true );
			$multiple_opening_hours          = get_post_meta( $post_id, '_seobreeze_multiple_opening_hours', true );
                        $week_days = array("monday"=>"Monday","tuesday"=>"Tuesday","wednesday"=>"Wednesday","thursday"=>"Thursday","friday"=>"Friday","saturday"=>"Saturday","sunday"=>"Sunday");

                        
?>


                                     
                    <label class="selectinput" for="seobreeze_business_type">Business Type:</label>
                    <select name="_seobreeze_business_type" class="seo-local-text" id="seobreeze_business_type">
                        <?php
                        $business_type_array = get_local_business_types();
                        foreach ($business_type_array as $key => $value) {
                            $checked = ($key == $business_type)? 'selected="selected"':'';
                            echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                        }
                        ?>
                    </select>
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_address">Business Address:</label>
                    <input type="text"  class="seo-local-text" name="_seobreeze_business_address" value="<?php echo esc_html__($business_address); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_address_2">Business Address Line 2 :</label>
                    <input type="text"  class="seo-local-text" name="_seobreeze_business_address_2" value="<?php echo esc_html__( $business_address_2 ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_city">Business City :</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_city" value="<?php echo esc_html__( $business_city ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_state">Business State:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_state" value="<?php echo esc_html__( $business_state ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_zipcode">Business Zip code:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_zipcode" value="<?php echo esc_html__( $business_zipcode ); ?>" />
                    <br class="clear">
                    
                    <label class="selectinput" for="seobreeze_business_country">Business Country:</label>
                    <select name="_seobreeze_business_country"  class="seo-local-text" id="seobreeze_business_country">
                        <?php
                        $business_country_array = get_country_array();
                        foreach ($business_country_array as $key => $value) {
                            $checked = ($key == $business_country)? 'selected="selected"':'';
                            echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                        }
                        ?>
                    </select>
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_phone">Business Phone:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_phone" value="<?php echo esc_html__( $business_phone ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_phone_2nd">Bussiness Phone 2:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_phone_2nd" value="<?php echo esc_html__( $business_phone_2nd ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_fax">Bussiness Fax:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_fax" value="<?php echo esc_html__( $business_fax ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_email">Business Email:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_email" value="<?php echo esc_html__( $business_email ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_url">Business URL:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_url" value="<?php echo esc_html__( $business_url ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="_seobreeze_business_vat_id">VAT ID:</label>
                    <input type="text" class="seo-local-text" name="seobreeze_business_vat_id" value="<?php echo esc_html__( $business_vat_id ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_tax_id">Tax ID:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_tax_id" value="<?php echo esc_html__( $business_tax_id ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_coc_id">Chamber of Commerce ID:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_coc_id" value="<?php echo esc_html__( $business_coc_id ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_price_range">Price range:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_business_price_range" value="<?php echo esc_html__( $business_price_range ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_notes_1">Notes:</label>
                    <textarea name="_seobreeze_business_notes_1"><?php echo $notes_1;?></textarea>
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_notes_2">Notes:</label>
                    <textarea name="_seobreeze_business_notes_2"><?php echo $notes_2;?></textarea>
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_notes_2">Notes:</label>
                    <textarea name="_seobreeze_business_notes_2"><?php echo $notes_2;?></textarea>
                    <br class="clear">
                    
                    <p>You can enter the lat/long coordinates yourself. If you leave them empty they will be calculated automatically. If you want to re-calculate these fields, please make them blank before saving this location.</p>
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_coordinates_lat">Latitude:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_coordinates_lat" value="<?php echo esc_html__( $coordinates_lat ); ?>" />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_coordinates_long">Longitude:</label>
                    <input type="text" class="seo-local-text" name="_seobreeze_coordinates_long" value="<?php echo esc_attr( $coordinates_long ); ?>" />
                    <br class="clear">            
                    
                    <label class="textinput-seo-local" for="seobreeze_is_postal_address">This address is a postal address (not a physical location):</label>
                    <input type="checkbox" name="_seobreeze_is_postal_address" id="seobreeze_is_postal_address" value="1" <?php checked( $is_postal_address,1 ); ?> />
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_business_location_logo">Location Logo:</label>
                    <div class="image-box">
                        <img width="auto" id="media_src_seobreeze_business_location_logo" height="75px" src="<?php if($business_location_logo != "") echo $business_location_logo; else echo plugins_url('/seobreeze-local/images/default.jpg'); ?>">
                        <input type="hidden" id="media_url_seobreeze_business_location_logo" name="media_url_seobreeze_business_location_logo" value="<?php echo $business_location_logo;?>">
                    </div>
                    <button type="button" id="seobreeze_business_location_logo" class="upload_que_img">Upload</button>
                    <button type="button" id="seobreeze_business_location_logo" class="remove_que_img">Remove</button>
                    <br class="clear">
                    
                    <label class="textinput-seo-local" for="seobreeze_custom_marker">custom Marker:</label>
                    <div class="image-box">
                        <img width="auto" id="media_src_seobreeze_custom_marker" height="75px" src="<?php if($business_location_custom_marker != "") echo $business_location_custom_marker; else echo plugins_url('/seobreeze-local/images/default.jpg'); ?>">
                        <input type="hidden" id="media_url_seobreeze_custom_marker" name="media_url_seobreeze_custom_marker" value="<?php echo $business_location_custom_marker;?>">
                    </div>
                    <button type="button" id="seobreeze_custom_marker" class="upload_que_img">Upload</button>
                    <button type="button" id="seobreeze_custom_marker" class="remove_que_img">Remove</button>
                    <br class="clear">
                    
                    <div class="loca-seo-heading"><h4>Opening Hours</h4></div>
                    <label class="textinput-seo-local" for="seobreeze_multiple_opening_hours"> I have two sets of opening hours per day</label>
                    <input type="checkbox" name="_seobreeze_multiple_opening_hours" id="seobreeze_multiple_opening_hours" value="1" <?php checked( $multiple_opening_hours,1 ); ?> />
                    <br class="clear">
                    <p>If a specific day only has one set of opening hours, please set the second set for that day to <strong>Closed.</strong></p>
                    <?php
                    foreach ( $week_days as $key => $day ) {
				$field_name = '_seobreeze_opening_hours_' . $key;
				$value_from = get_post_meta( $post_id, $field_name . '_from', true );
				if ( ! $value_from ) {
					$value_from = '09:00';
				}
				$value_to = get_post_meta( $post_id, $field_name . '_to', true );
				if ( ! $value_to ) {
					$value_to = '17:00';
				}
				$value_second_from = get_post_meta( $post_id, $field_name . '_second_from', true );
				if ( ! $value_second_from ) {
					$value_second_from = '09:00';
				}
				$value_second_to = get_post_meta( $post_id, $field_name . '_second_to', true );
				if ( ! $value_second_to ) {
					$value_second_to = '17:00';
				}

                                echo '<div id="opening-hours-'.$key.'" class="opening-hours">';
				echo '<label class="textinput-seo-local">' . $day . ':</label>';
				echo '<select class="openinghours_from" style="width: 100px;" id="' . $field_name . '_from" name="' . $field_name . '_from">';
				echo wpseo_show_hour_options( get_option('opening_hours_24h'), $value_from );
				echo '</select><span id="' . $field_name . '_to_wrapper"> - ';
				echo '<select class="openinghours_to" style="width: 100px;" id="' . $field_name . '_to" name="' . $field_name . '_to">';
				echo wpseo_show_hour_options( get_option("opening_hours_24h"), $value_to );
				echo '</select>';

                                $enable_class =  (isset( $multiple_opening_hours ) && $multiple_opening_hours == '1') ? "":"style='display: none'";
                                echo '<div id="opening-hours-second-'.$key.'" class="opening-hours-second " '.$enable_class.'>';
				echo '<select class="openinghours_from_second" style="width: 100px;" id="' . $field_name . '_second_from" name="' . $field_name . '_second_from">';
				echo wpseo_show_hour_options( get_option("opening_hours_24h"), $value_second_from );
				echo '</select><span id="' . $field_name . '_second_to_wrapper"> - ';
				echo '<select class="openinghours_to_second" style="width: 100px;" id="' . $field_name . '_second_to" name="' . $field_name . '_second_to">';
				echo wpseo_show_hour_options( get_option("opening_hours_24h"), $value_second_to );
				echo '</select>';
				echo '</div></div>';
                                echo '<div class="clear"></div>';
                                
            }
                    
            
            echo '</div>';
        }
        
        function seobreeze_locations_save_meta( $post_id, $post ) {
			// First check if post type is seobreeze_locations.
			if ( $post->post_type == 'seobreeze_locations' ) {

				
				// Verify this came from the our screen and with proper authorization,
				// because save_post can be triggered at other times.
				if ( false == isset( $_POST['locationsmeta_noncename'] ) || ( isset( $_POST['locationsmeta_noncename'] ) && ! wp_verify_nonce( $_POST['locationsmeta_noncename'], plugin_basename( __FILE__ ) ) ) ) {
					return $post_id;
				}

				// Is the user allowed to edit the post or page?
				if ( ! current_user_can( 'edit_post', $post_id ) ) {
					return $post_id;
				}

				// OK, we're authenticated: we need to find and save the data
				// We'll put it into an array to make it easier to loop though.
				$locations_meta['_seobreeze_business_type']                   = isset( $_POST['_seobreeze_business_type'] ) ? sanitize_text_field( $_POST['_seobreeze_business_type'] ) : 'LocalBusiness';
				$locations_meta['_seobreeze_business_address']                = isset( $_POST['_seobreeze_business_address'] ) ? sanitize_text_field( $_POST['_seobreeze_business_address'] ) : '';
				$locations_meta['_seobreeze_business_address_2']              = isset( $_POST['_seobreeze_business_address_2'] ) ? sanitize_text_field( $_POST['_seobreeze_business_address_2'] ) : '';
				$locations_meta['_seobreeze_business_city']                   = isset( $_POST['_seobreeze_business_city'] ) ? sanitize_text_field( $_POST['_seobreeze_business_city'] ) : '';
				$locations_meta['_seobreeze_business_state']                  = isset( $_POST['_seobreeze_business_state'] ) ? sanitize_text_field( $_POST['_seobreeze_business_state'] ) : '';
				$locations_meta['_seobreeze_business_zipcode']                = isset( $_POST['_seobreeze_business_zipcode'] ) ? sanitize_text_field( $_POST['_seobreeze_business_zipcode'] ) : '';
				$locations_meta['_seobreeze_business_country']                = isset( $_POST['_seobreeze_business_country'] ) ? sanitize_text_field( $_POST['_seobreeze_business_country'] ) : '';
				$locations_meta['_seobreeze_business_phone']                  = isset( $_POST['_seobreeze_business_phone'] ) ? sanitize_text_field( $_POST['_seobreeze_business_phone'] ) : '';
				$locations_meta['_seobreeze_business_phone_2nd']              = isset( $_POST['_seobreeze_business_phone_2nd'] ) ? sanitize_text_field( $_POST['_seobreeze_business_phone_2nd'] ) : '';
				$locations_meta['_seobreeze_business_fax']                    = isset( $_POST['_seobreeze_business_fax'] ) ? sanitize_text_field( $_POST['_seobreeze_business_fax'] ) : '';
				$locations_meta['_seobreeze_business_email']                  = isset( $_POST['_seobreeze_business_email'] ) ? sanitize_email( $_POST['_seobreeze_business_email'] ) : '';
				$locations_meta['media_url_seobreeze_business_location_logo'] = isset( $_POST['media_url_seobreeze_business_location_logo'] ) ? sanitize_text_field( $_POST['media_url_seobreeze_business_location_logo'] ) : '';
				$locations_meta['media_url_seobreeze_custom_marker'] = isset( $_POST['media_url_seobreeze_custom_marker'] ) ? sanitize_text_field( $_POST['media_url_seobreeze_custom_marker'] ) : '';
				$locations_meta['_seobreeze_business_vat_id']                 = isset( $_POST['_seobreeze_business_vat_id'] ) ? sanitize_text_field( $_POST['_seobreeze_business_vat_id'] ) : '';
				$locations_meta['_seobreeze_business_tax_id']                 = isset( $_POST['_seobreeze_business_tax_id'] ) ? sanitize_text_field( $_POST['_seobreeze_business_tax_id'] ) : '';
				$locations_meta['_seobreeze_business_coc_id']                 = isset( $_POST['_seobreeze_business_coc_id'] ) ? sanitize_text_field( $_POST['_seobreeze_business_coc_id'] ) : '';
				$locations_meta['_seobreeze_business_price_range']            = isset( $_POST['_seobreeze_business_price_range'] ) ? sanitize_text_field( $_POST['_seobreeze_business_price_range'] ) : '';
				$locations_meta['_seobreeze_business_notes_1']                = isset( $_POST['_seobreeze_business_notes_1'] ) ? sanitize_text_field( $_POST['_seobreeze_business_notes_1'] ) : '';
				$locations_meta['_seobreeze_business_notes_2']                = isset( $_POST['_seobreeze_business_notes_2'] ) ? sanitize_text_field( $_POST['_seobreeze_business_notes_2'] ) : '';
				$locations_meta['_seobreeze_business_notes_3']                = isset( $_POST['_seobreeze_business_notes_3'] ) ? sanitize_text_field( $_POST['_seobreeze_business_notes_3'] ) : '';
				$locations_meta['_seobreeze_is_postal_address']               = isset( $_POST['_seobreeze_is_postal_address'] ) ? sanitize_text_field( $_POST['_seobreeze_is_postal_address'] ) : '';
				$locations_meta['_seobreeze_multiple_opening_hours']          = isset( $_POST['_seobreeze_multiple_opening_hours'] ) ? $_POST['_seobreeze_multiple_opening_hours'] : '';
        		            $week_days = array("monday"=>"Monday","tuesday"=>"Tuesday","wednesday"=>"Wednesday","thursday"=>"Thursday","friday"=>"Friday","saturday"=>"Saturday","sunday"=>"Sunday");

                                foreach ( $week_days as $key => $day ) {
					$field_name                                     = '_seobreeze_opening_hours_' . $key;
					$locations_meta[ $field_name . '_from' ]        = ( isset( $_POST[ $field_name . '_from' ] ) ) ? $_POST[ $field_name . '_from' ] : '';
					$locations_meta[ $field_name . '_to' ]          = ( isset( $_POST[ $field_name . '_to' ] ) ) ? $_POST[ $field_name . '_to' ] : '';
					$locations_meta[ $field_name . '_second_from' ] = ( isset( $_POST[ $field_name . '_second_from' ] ) ) ? $_POST[ $field_name . '_second_from' ] : '';
					$locations_meta[ $field_name . '_second_to' ]   = ( isset( $_POST[ $field_name . '_second_to' ] ) ) ? $_POST[ $field_name . '_second_to' ] : '';

					if ( $locations_meta[ $field_name . '_from' ] == 'closed' ) {
						$locations_meta[ $field_name . '_to' ] = $locations_meta[ $field_name . '_from' ];
					}
					if ( $locations_meta[ $field_name . '_second_from' ] == 'closed' ) {
						$locations_meta[ $field_name . '_second_to' ] = $locations_meta[ $field_name . '_second_from' ];
					}
				}

				$locations_meta['_seobreeze_business_url'] = ( isset( $_POST['_seobreeze_business_url'] ) && '' != $_POST['_seobreeze_business_url'] ) ? sanitize_text_field( $_POST['_seobreeze_business_url'] ) : get_permalink( $post_id );

				// Put http:// in front of the URL, if it's not there yet.
				if ( ! preg_match( '~^(?:f|ht)tps?://~i', $locations_meta['_seobreeze_business_url'] ) ) {
					$locations_meta['_seobreeze_business_url'] = 'http://' . $locations_meta['_seobreeze_business_url'];
				}

				// If lat/long fields are empty or address is changed calculate them.
				$coords_lat_old  = get_post_meta( $post_id, '_seobreeze_coordinates_lat', true );
				$coords_long_old = get_post_meta( $post_id, '_seobreeze_coordinates_long', true );
				$old_address     = get_post_meta( $post_id, '_seobreeze_business_address', true );
				$new_address     = isset( $_POST['_seobreeze_business_address'] ) ? $_POST['_seobreeze_business_address'] : '';

				if ( empty( $_POST['_seobreeze_coordinates_lat'] ) || empty( $_POST['_seobreeze_coordinates_long'] ) || $new_address != $old_address || $_POST['_seobreeze_coordinates_lat'] != $coords_lat_old || $_POST['_seobreeze_coordinates_long'] != $coords_long_old ) {

					$coords_lat  = $_POST['_seobreeze_coordinates_lat'];
					$coords_long = $_POST['_seobreeze_coordinates_long'];

					if ( empty( $_POST['_seobreeze_coordinates_lat'] ) || empty( $_POST['_seobreeze_coordinates_long'] ) || $new_address != $old_address ) {
					/*	$geodata = $wpseo_local_core->get_geo_data( $locations_meta, true, $post_id );
						if ( $geodata ) {
							$coords_lat  = $geodata['coords']['lat'];
							$coords_long = $geodata['coords']['long'];
						} */
					}

					// Replace comma's into points.
					$coords_lat  = str_replace( ',', '.', $coords_lat );
					$coords_long = str_replace( ',', '.', $coords_long );
					update_post_meta( $post_id, '_seobreeze_coordinates_lat', sanitize_text_field( $coords_lat ) );
					update_post_meta( $post_id, '_seobreeze_coordinates_long', sanitize_text_field( $coords_long ) );
					// Set the timezone for this location.
					$this->seobreeze_local_timezone_repository->set_location_timezone( $post_id );
				}

				// Add values of $locations_meta as custom fields.
				foreach ( $locations_meta as $key => $value ) {
					// Cycle through the $locations_meta array.
					if ( $post->post_type == 'revision' ) {
						// Don't store custom data twice.
						return $post_id;
					}
					if ( ! empty( $value ) ) {
						update_post_meta( $post_id, $key, $value );
					}
					else {
						// Delete if blank.
						delete_post_meta( $post_id, $key );
					}
				}

				
			}

			return true;
		}
        function meta_box_scripts() {

            global $post;
            
            wp_enqueue_media( array(
                'post' => $post->ID,
            ) );
            
                if ( is_post_type_archive('seobreeze_locations') ) {     
                    wp_enqueue_script('seo-local-js', plugins_url('/seobreeze-local/js/seobreeze-local.js'),array('jquery'));
                }

	}
        
        function add_media_buttons() {
			$is_post_edit_page = in_array( basename( $_SERVER['PHP_SELF'] ), array(
				'post.php',
				'page.php',
				'page-new.php',
				'post-new.php',
			) );
			if ( ! $is_post_edit_page ) {
				return;
			}

			if ( ! post_type_supports( get_post_type(), 'editor' ) ) {
				return;
			}

			// Make sure to don't output white space between these buttons.
			echo '<a href="#TB_inline?width=480&height=600&inlineId=wpseo_add_map" class="thickbox button" id="wpseo_add_map_button"><span class="wpseo_media_icon wpseo_icon_map"></span> ' . __( 'Map', 'seo-breeze-local' ) . '</a>';

			echo '<a href="#TB_inline?width=480&inlineId=wpseo_add_address" class="thickbox button" id="wpseo_add_address_button"><span class="wpseo_media_icon wpseo_icon_address"></span> ' . __( 'Address', 'seo-breeze-local' ) . '</a>';

			echo '<a href="#TB_inline?width=480&inlineId=wpseo_add_opening_hours" class="thickbox button" id="wpseo_add_opening_hours_button"><span class="wpseo_media_icon wpseo_icon_opening_hours"></span> ' . __( 'Opening hours', 'seo-breeze-local' ) . '</a>';

			if ( seobreeze_has_multiple_locations() ) {
				echo '<a href="#TB_inline?width=480&height=510&inlineId=wpseo_add_storelocator" class="thickbox button" id="wpseo_add_storelocator_button"><span class="wpseo_media_icon wpseo_icon_storelocator"></span> ' . __( 'Store locator', 'seo-breeze-local' ) . '</a>';
			}
		}
                
                function add_mce_popup() {
			$is_post_edit_page = in_array( basename( $_SERVER['PHP_SELF'] ), array(
				'post.php',
				'page.php',
				'page-new.php',
				'post-new.php',
			) );
			if ( ! $is_post_edit_page ) {
				return;
			}

			if ( ! post_type_supports( get_post_type(), 'editor' ) ) {
				return;
			}
			?>
			<?php // @codingStandardsIgnoreStart ?>
			<script>
				function WPSEO_InsertMap() {
					var wrapper = jQuery('#wpseo_add_map');
					var location_id = jQuery("#wpseo_map_location_id").val();
					var term_id = jQuery("#wpseo_map_term_id").val();
					var center_id = jQuery("#wpseo_map_center_location_id").val();

					<?php if ( seobreeze_has_multiple_locations() ) { ?>
					if (location_id == '') {
						alert("<?php _e( 'Please select a location', 'seo-breeze-local' ); ?>");
						return;
					}
					<?php } ?>

					var map_style = jQuery('input[name=wpseo_map_style]:checked', '.wpseo_map_style').val()
					var width = jQuery("#wpseo_map_width").val();
					var height = jQuery("#wpseo_map_height").val();
					var zoom = jQuery("#wpseo_map_zoom").val();
					var scrollable = jQuery("#wpseo_map_scrollable").is(":checked") ? ' scrollable="1"' : ' scrollable="0"';
					var draggable = jQuery("#wpseo_map_draggable").is(":checked") ? ' draggable="1"' : ' draggable="0"';
					var marker_clustering = jQuery("#wpseo_map_marker_clustering").is(":checked") ? ' marker_clustering="1"' : ' marker_clustering="0"';
					var show_route = jQuery("#wpseo_map_show_route").is(":checked") ? ' show_route="1"' : ' show_route="0"';
					var show_state = jQuery("#wpseo_map_show_state").is(":checked") ? ' show_state="1"' : ' show_state="0"';
					var show_country = jQuery("#wpseo_map_show_country").is(":checked") ? ' show_country="1"' : '';
					var show_url = jQuery("#wpseo_map_show_url").is(":checked") ? ' show_url="1"' : '';
					var show_email = jQuery("#wpseo_map_show_email").is(":checked") ? ' show_email="1"' : '';
					var show_category_filter = jQuery("#wpseo_map_show_category_filter").is(":checked") ? ' show_category_filter="1"' : '';

					var id = '';
					if (location_id != 'undefined' && typeof location_id != 'undefined') {
						id = "id=\"" + location_id + "\" ";
					}

					var term = '';
					if (term_id != 'undefined' && typeof term_id != 'undefined' && term_id != '') {
						term = "term_id=\"" + term_id + "\" ";
					}

					var center = ''
					if (center_id != 'undefined' && typeof center_id != 'undefined' && center_id != '') {
						center = "center=\"" + center_id + "\" ";
					}

					var default_show_infowindow = '';
					if (location_id != 'all' && jQuery("#wpseo_show_popup_default").is(":checked")) {
						default_show_infowindow = " default_show_infowindow=\"1\"";
					}

					if (location_id != 'all') {
						marker_clustering = "";
					}

					window.send_to_editor("[seobreeze_map " + id + term + center + " width=\"" + width + "\" height=\"" + height + "\" zoom=\"" + zoom + "\" map_style=\"" + map_style + "\"" + scrollable + draggable + marker_clustering + show_route + show_state + show_country + show_url + show_email + show_category_filter + default_show_infowindow + "]");
				}
				function WPSEO_InsertAddress() {
					var location_id = jQuery("#wpseo_address_location_id").val();
					var term_id = jQuery("#wpseo_address_term_id").val();

					<?php if ( seobreeze_has_multiple_locations() ) { ?>
					if (location_id == '') {
						alert("<?php _e( 'Please select a location', 'seo-breeze-local' ); ?>");
						return;
					}
					<?php } ?>
					var hide_name = jQuery("#wpseo_hide_name").is(":checked") ? ' hide_name="1"' : '';
					var hide_address = jQuery("#wpseo_hide_address").is(":checked") ? ' hide_address="1"' : '';
					var oneline = jQuery("#wpseo_oneline").is(":checked") ? ' oneline="1"' : '';
					var show_state = jQuery("#wpseo_show_state").is(":checked") ? ' show_state="1"' : ' show_state="0"';
					var show_country = jQuery("#wpseo_show_country").is(":checked") ? ' show_country="1"' : ' show_country="0"';
					var show_phone = jQuery("#wpseo_show_phone").is(":checked") ? ' show_phone="1"' : ' show_phone="0"';
					var show_phone_2 = jQuery("#wpseo_show_phone_2nd").is(":checked") ? ' show_phone_2="1"' : ' show_phone_2="0"';
					var show_fax = jQuery("#wpseo_show_fax").is(":checked") ? ' show_fax="1"' : ' show_fax="0"';
					var show_email = jQuery("#wpseo_show_email").is(":checked") ? ' show_email="1"' : ' show_email="0"';
					var show_url = jQuery("#wpseo_show_url").is(":checked") ? ' show_url="1"' : '';
					var show_logo = jQuery("#wpseo_show_logo").is(":checked") ? ' show_logo="1"' : ' show_logo="0"';
					var show_vat = jQuery("#wpseo_show_vat_id").is(":checked") ? ' show_vat="1"' : '';
					var show_tax = jQuery("#wpseo_show_tax_id").is(":checked") ? ' show_tax="1"' : '';
					var show_coc = jQuery("#wpseo_show_coc_id").is(":checked") ? ' show_coc="1"' : '';
					var show_price_range = jQuery("#wpseo_show_price_range").is(":checked") ? ' show_price_range="1"' : '';
					var show_opening_hours = jQuery("#wpseo_show_opening_hours").is(":checked") ? ' show_opening_hours="1"' : '';
					var hide_closed = jQuery("#wpseo_hide_closed").is(":checked") ? ' hide_closed="1"' : '';
					var comment_string = jQuery("#wpseo_comment").val();
					var orderby = '';
					var order = '';

					var id = '';
					if (location_id != 'undefined' && typeof location_id != 'undefined') {
						id = "id=\"" + location_id + "\" ";
					}

					var term = '';
					if (term_id != 'undefined' && typeof term_id != 'undefined' && term_id != '') {
						term = "term_id=\"" + term_id + "\" ";
					}

					var shortcode_name = 'seobreeze_address';
					if (location_id == 'all') {
						shortcode_name = 'seobreeze_all_locations';

						orderby = ' orderby=' + jQuery("#wpseo_address_all_locations_orderby").val();
						order = ' order=' + jQuery("#wpseo_address_all_locations_order").val();
					}

					var comment = '';
					if (comment_string != '') {
						comment = ' comment="' + comment_string + '"';
					}

					window.send_to_editor("[" + shortcode_name + " " + id + term + hide_name + hide_address + oneline + show_state + show_country + show_phone + show_phone_2 + show_fax + show_email + show_url + show_vat + show_tax + show_coc + show_price_range + show_logo + show_opening_hours + hide_closed + comment + orderby + order + "]");
				}
				function WPSEO_InsertOpeningHours() {
					var wrapper = jQuery('#wpseo_add_opening_hours');

					var location_id = jQuery("#wpseo_oh_location_id").val();
					if (location_id == '') {
						alert("<?php _e( 'Please select a location', 'seo-breeze-local' ); ?>");
						return;
					}

					var id = '';
					if (location_id != 'undefined' && typeof location_id != 'undefined') {
						id = "id=\"" + location_id + "\" ";
					}
					var show_days = new Array();

					if (jQuery("#wpseo_oh_show_sunday").is(":checked")) {
						show_days.push(jQuery("#wpseo_oh_show_sunday").val())
					}
					if (jQuery("#wpseo_oh_show_monday").is(":checked")) {
						show_days.push(jQuery("#wpseo_oh_show_monday").val())
					}
					if (jQuery("#wpseo_oh_show_tuesday").is(":checked")) {
						show_days.push(jQuery("#wpseo_oh_show_tuesday").val())
					}
					if (jQuery("#wpseo_oh_show_wednesday").is(":checked")) {
						show_days.push(jQuery("#wpseo_oh_show_wednesday").val())
					}
					if (jQuery("#wpseo_oh_show_thursday").is(":checked")) {
						show_days.push(jQuery("#wpseo_oh_show_thursday").val())
					}
					if (jQuery("#wpseo_oh_show_friday").is(":checked")) {
						show_days.push(jQuery("#wpseo_oh_show_friday").val())
					}
					if (jQuery("#wpseo_oh_show_saturday").is(":checked")) {
						show_days.push(jQuery("#wpseo_oh_show_saturday").val())
					}
					var comment_string = jQuery("#wpseo_oh_comment").val();

					var hide_closed = jQuery("#wpseo_oh_hide_closed").is(":checked") ? ' hide_closed="1"' : '';

					var comment = '';
					if (comment_string != '') {
						comment = ' comment="' + comment_string + '"';
					}

					window.send_to_editor("[seobreeze_opening_hours " + id + hide_closed + "show_days=\"" + show_days + "\"" + comment + "]");
				}
				<?php if ( seobreeze_has_multiple_locations() ) { ?>
				function WPSEO_InsertStorelocator() {
					var show_map = jQuery("#wpseo_sl_show_map").is(":checked") ? ' show_map="1"' : ' show_map="0"';
					var scrollable = jQuery("#wpseo_sl_scrollable").is(":checked") ? ' scrollable="1"' : ' scrollable="0"';
					var draggable = jQuery("#wpseo_sl_draggable").is(":checked") ? ' draggable="1"' : ' draggable="0"';
					var marker_clustering = jQuery("#wpseo_sl_marker_clustering").is(":checked") ? ' marker_clustering="1"' : ' marker_clustering="0"';
					var show_radius = jQuery("#wpseo_sl_show_radius").is(":checked") ? ' show_radius="1"' : '';
					var show_nearest_suggestion = jQuery("#wpseo_sl_show_nearest_suggestion").is(":checked") ? ' show_nearest_suggestion="1"' : ' show_nearest_suggestion="0"';
					var show_filter = jQuery("#wpseo_sl_show_filter").is(":checked") ? ' show_filter="1"' : '';
					var radius = ' radius="' + jQuery("#wpseo_sl_radius").val() + '"';

					var map_style = jQuery('input[name=wpseo_sl_map_style]:checked', '.wpseo_map_style').val()
					var oneline = jQuery("#wpseo_sl_oneline").is(":checked") ? ' oneline="1"' : '';
					var show_state = jQuery("#wpseo_sl_show_state").is(":checked") ? ' show_state="1"' : '';
					var show_country = jQuery("#wpseo_sl_show_country").is(":checked") ? ' show_country="1"' : '';
					var show_phone = jQuery("#wpseo_sl_show_phone").is(":checked") ? ' show_phone="1"' : '';
					var show_phone_2 = jQuery("#wpseo_sl_show_phone_2nd").is(":checked") ? ' show_phone_2="1"' : '';
					var show_fax = jQuery("#wpseo_sl_show_fax").is(":checked") ? ' show_fax="1"' : '';
					var show_email = jQuery("#wpseo_sl_show_email").is(":checked") ? ' show_email="1"' : '';
					var show_url = jQuery("#wpseo_sl_show_url").is(":checked") ? ' show_url="1"' : '';
					var show_opening_hours = jQuery("#wpseo_sl_show_opening_hours").is(":checked") ? ' show_opening_hours="1"' : '';
					var hide_closed = jQuery("#wpseo_sl_hide_closed").is(":checked") ? ' hide_closed="1"' : '';
					var show_category_filter = jQuery("#wpseo_sl_show_category_filter").is(":checked") ? ' show_category_filter="1"' : '';

					window.send_to_editor("[seobreeze_storelocator " + show_map + scrollable + draggable + marker_clustering + show_radius + show_nearest_suggestion + radius + show_filter + " map_style=\"" + map_style + "\"" + oneline + show_state + show_country + show_phone + show_phone_2 + show_fax + show_email + show_url + show_opening_hours + hide_closed + show_category_filter + "]");
				}

				function WPSEO_Address_Change_Order(obj) {
					if (jQuery(obj).val() == 'all') {
						jQuery('#wpseo_address_all_locations_order_wrapper').slideDown();
						jQuery('#wpseo_address_term_id').removeAttr('disabled');
					}
					else {
						jQuery('#wpseo_address_all_locations_order_wrapper').slideUp();
						jQuery('#wpseo_address_term_id').val('');
						jQuery('#wpseo_address_term_id').attr('disabled', true);
					}
				}

				function WPSEO_Address_Change_Term_Order(obj) {
					if (jQuery(obj).val() != 'all' && jQuery(obj).val() != '') {
						jQuery('#wpseo_address_location_id').val('all');
					}
				}

				function WPSEO_Map_Change_Location(obj) {
					if (jQuery(obj).val() != 'all') {
						jQuery('#wpseo_map_term_id').val('');
						jQuery('#wpseo_map_center_location_id').val('');
						jQuery('#wpseo_map_term_id').attr('disabled', true);
						jQuery('#wpseo_map_center_location_id').attr('disabled', true);
						jQuery('#wpseo_show_popup_default').removeAttr('disabled');
						jQuery('#wpseo_map_marker_clustering').attr('disabled', true);
					} else {
						jQuery('#wpseo_map_term_id').removeAttr('disabled');
						jQuery('#wpseo_map_center_location_id').removeAttr('disabled');
						jQuery('#wpseo_show_popup_default').attr('disabled', true);
						jQuery('#wpseo_map_marker_clustering').removeAttr('disabled');
					}
				}

				function WPSEO_Map_Change_Term(obj) {
					if (jQuery(obj).val() != 'all' && jQuery(obj).val() != '') {
						jQuery('#wpseo_map_location_id').val('all');
					}
				}
				<?php } ?>
			</script>
			<div id="wpseo_add_map" style="display:none;">
				<div class="wrap">
					<div>
						<style>
							.wpseo-textfield {
								width: 60px;
								border: 1px solid #dfdfdf;
								-webkit-border-radius: 3px;
								border-radius: 3px;
							}

							.wpseo-select {
								width: 100px;
								margin: 0;
							}

							.wpseo-for-textfield {
								display: inline-block;
								width: 100px;
							}
						</style>
						<div style="padding:15px 15px 0 15px;">
							<h2><?php esc_html_e( 'Insert Google Map', 'seo-breeze-local' ); ?></h2>
						</div>
						<?php
						$location_args = array(
							'post_type'      => 'seobreeze_locations',
							'posts_per_page' => -1,
							'orderby'        => 'title',
							'order'          => 'ASC',
							'fields'         => 'ids',
							'post_status'    => 'publish',
						);
						if ( current_user_can( 'edit_posts' ) ) {
							$location_args['post_status'] = array(
								'publish',
								'draft',
							);
						}

						$locations = get_posts( $location_args );
						?>
						<?php if ( seobreeze_has_multiple_locations() && ! empty( $locations ) ) { ?>
							<div style="padding:15px 15px 0 15px;">
								<label for="wpseo_map_location_id" class="screen-reader-text"><?php esc_html_e( 'Location:', 'seo-breeze-local' ); ?></label>
								<select id="wpseo_map_location_id" onchange="WPSEO_Map_Change_Location( this )">
									<option value=""> -- <?php _e( 'Select a location', 'seo-breeze-local' ); ?> --</option>
									<?php
									if ( ! empty( $locations ) ) {
										echo '<option value="all">' . __( 'All locations', 'seo-breeze-local' ) . '</option>';

										foreach ( $locations as $location_id ) { ?>
											<option value="<?php echo $location_id; ?>" <?php selected( $location_id, get_the_ID(), true ); ?>><?php echo get_the_title( $location_id ); ?></option>
											<?php
										}
									}
									?>
								</select>
								<label for="wpseo_map_term_id" class="screen-reader-text"><?php esc_html_e( 'Category:', 'seo-breeze-local' ); ?></label>
								<select id="wpseo_map_term_id" onchange="WPSEO_Map_Change_Term( this )" <?php echo( in_array( get_the_ID(), $locations ) ? 'disabled' : '' ); ?>>
									<option value=""> -- <?php _e( 'Select a category', 'seo-breeze-local' ); ?> --</option>
									<?php
									$categories = get_terms( 'seobreeze_locations_category', array(
										'hide_empty' => false,
									) );

									foreach ( $categories as $category ) {
										?>
										<option value="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></option>
										<?php
									}
									?>
								</select> <br><br>
								<label for="wpseo_map_center_location_id"><?php esc_html_e( 'Center map on this location:', 'seo-breeze-local' ); ?></label><br>
								<select id="wpseo_map_center_location_id" <?php echo( in_array( get_the_ID(), $locations ) ? 'disabled' : '' ); ?>>
									<?php
									$location_args = array(
										'post_type'      => 'seobreeze_locations',
										'posts_per_page' => -1,
										'orderby'        => 'title',
										'order'          => 'ASC',
										'fields'         => 'ids',
										'post_status'    => 'publish',
									);

									if ( current_user_can( 'edit_posts' ) ) {
										$location_args['post_status'] = array(
											'publish',
											'draft',
										);
									}

									$locations = get_posts( $location_args );

									if ( ! empty( $locations ) ) {
										echo '<option value="">' . __( 'All locations', 'seo-breeze-local' ) . '</option>';

										foreach ( $locations as $location_id ) {
											?>
											<option value="<?php echo $location_id; ?>"><?php echo get_the_title( $location_id ); ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						<?php } ?>
						<?php if ( ( seobreeze_has_multiple_locations() && ! empty( $locations ) ) || ! seobreeze_has_multiple_locations() ) { ?>
							<div style="padding:15px 15px 0 15px;">
								<h2><?php esc_html_e( 'Map style', 'seo-breeze-local' ); ?></h2>
								<ul>
									<?php
									$map_styles = array(
										'ROADMAP'   => __( 'Roadmap', 'seo-breeze-local' ),
										'HYBRID'    => __( 'Hybrid', 'seo-breeze-local' ),
										'SATELLITE' => __( 'Satellite', 'seo-breeze-local' ),
										'TERRAIN'   => __( 'Terrain', 'seo-breeze-local' ),
									);

									foreach ( $map_styles as $key => $label ) {
										?>
										<li class="wpseo_map_style" style="display: inline-block; width: 120px; height: 150px; margin-right: 10px;text-align: center;">
											<label for="wpseo_map_style-<?php echo strtolower( $key ); ?>">
												<img src="<?php echo plugins_url( '/images/map-' . strtolower( $key ) . '.png', dirname( __FILE__ ) ); ?>" alt=""><br>
												<?php echo $label; ?><br>
												<input type="radio" name="wpseo_map_style" id="wpseo_map_style-<?php echo strtolower( $key ); ?>" value="<?php echo strtolower( $key ); ?>" <?php checked( 'ROADMAP', $key ); ?>>
											</label>
										</li>
										<?php
									}
									?>
								</ul>
							</div>

							<div style="padding:15px 15px 0 15px;">
								<label class="wpseo-for-textfield" for="wpseo_map_width"><?php esc_html_e( 'Width:', 'seo-breeze-local' ); ?></label>
								<input type="text" id="wpseo_map_width" class="wpseo-textfield" value="400"><br>
								<label class="wpseo-for-textfield" for="wpseo_map_height"><?php esc_html_e( 'Height:', 'seo-breeze-local' ); ?></label>
								<input type="text" id="wpseo_map_height" class="wpseo-textfield" value="300"><br>
								<label class="wpseo-for-textfield" for="wpseo_map_zoom"><?php esc_html_e( 'Zoom level:', 'seo-breeze-local' ); ?></label>
								<select id="wpseo_map_zoom" class="wpseo-select" value="300">
									<option value="-1"><?php _e( 'Auto', 'seo-breeze-local' ); ?></option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
								</select><br>
								<br>
								<input type="checkbox" id="wpseo_map_scrollable" checked="checked" />
								<label for="wpseo_map_scrollable"><?php _e( 'Allow scrolling of the map', 'seo-breeze-local' ); ?></label><br>
								<input type="checkbox" id="wpseo_map_draggable" checked="checked" />
								<label for="wpseo_map_draggable"><?php _e( 'Allow dragging of the map', 'seo-breeze-local' ); ?></label><br>
								<?php if ( seobreeze_has_multiple_locations() ) { ?>
									<input type="checkbox" id="wpseo_map_marker_clustering" />
									<label for="wpseo_map_marker_clustering"><?php _e( 'Marker clustering', 'seo-breeze-local' ); ?></label>
									<br>
								<?php } ?>
								<input type="checkbox" id="wpseo_map_show_route" />
								<label for="wpseo_map_show_route"><?php _e( 'Show route planner', 'seo-breeze-local' ); ?></label><br>
								<input type="checkbox" id="wpseo_show_popup_default" />
								<label for="wpseo_show_popup_default"><?php _e( 'Show info-popup by default', 'seo-breeze-local' ); ?></label><br>
								<input type="checkbox" id="wpseo_map_show_state" />
								<label for="wpseo_map_show_state"><?php _e( 'Show state in info-popup', 'seo-breeze-local' ); ?></label><br>
								<input type="checkbox" id="wpseo_map_show_country" />
								<label for="wpseo_map_show_country"><?php _e( 'Show country in info-popup', 'seo-breeze-local' ); ?></label><br>
								<input type="checkbox" id="wpseo_map_show_url" />
								<label for="wpseo_map_show_url"><?php _e( 'Show URL in info-popup', 'seo-breeze-local' ); ?></label><br>
								<input type="checkbox" id="wpseo_map_show_email" />
								<label for="wpseo_map_show_email"><?php _e( 'Show email in info popup', 'seo-breeze-local' ); ?></label><br>
								<?php if ( ! seobreeze_has_multiple_locations() ) { ?>
									<input type="checkbox" id="wpseo_map_show_category_filter" />
									<label for="wpseo_map_show_category_filter"><?php _e( 'Show a filter for location categories under the map', 'seo-breeze-local' ); ?></label>
									<br>
								<?php } ?>
							</div>
							<div style="padding:15px;">
								<input type="button" class="button button-primary" value="<?php _e( 'Insert map', 'seo-breeze-local' ); ?>" onclick="WPSEO_InsertMap();" />&nbsp;&nbsp;&nbsp;
								<a class="button" href="#" onclick="tb_remove(); return false;"><?php _e( 'Cancel', 'seo-breeze-local' ); ?></a>
							</div>
						<?php } ?>
						<?php if ( seobreeze_has_multiple_locations() && empty( $locations ) ) { ?>
							<p>In order to use this shortcode function, please
								<a href="<?php echo trailingslashit( admin_url() ) . 'edit.php?post_type=wpseo_loctions'; ?>">add one or more locations</a> first.
							</p>
						<?php } ?>
					</div>
				</div>
			</div>
			<div id="wpseo_add_address" style="display:none;">
				<div class="wrap">
					<div>
						<div style="padding:15px 15px 0 15px;">
							<h2><?php _e( 'Insert Address', 'seo-breeze-local' ); ?></h2>
						</div>
						<?php
						$location_args = array(
							'post_type'      => 'seobreeze_locations',
							'posts_per_page' => -1,
							'orderby'        => 'title',
							'order'          => 'ASC',
							'fields'         => 'ids',
							'post_status'    => 'publish',
						);
						if ( current_user_can( 'edit_posts' ) ) {
							$location_args['post_status'] = array(
								'publish',
								'draft',
							);
						}

						$locations = get_posts( $location_args );
						?>
						<?php if ( seobreeze_has_multiple_locations() && ! empty( $locations ) ) { ?>
							<div style="padding:15px 15px 0 15px;">
								<label for="wpseo_address_location_id" class="screen-reader-text"><?php esc_html_e( 'Location:', 'seo-breeze-local' ); ?></label>
								<select id="wpseo_address_location_id" onchange="WPSEO_Address_Change_Order( this );">
									<option value=""> -- <?php _e( 'Select a location', 'seo-breeze-local' ); ?> --</option>
									<?php
									if ( ! empty( $locations ) ) {
										echo '<option value="all">' . __( 'Show all locations', 'seo-breeze-local' ) . '</option>';

										foreach ( $locations as $location_id ) {
											?>
											<option value="<?php echo $location_id; ?>" <?php selected( $location_id, get_the_ID(), true ); ?>><?php echo get_the_title( $location_id ); ?></option>
											<?php
										}
									}
									?>
								</select>
								<?php
								$categories = get_terms( 'seobreeze_locations_category', array(
									'hide_empty' => false,
								) );
								if ( ! is_wp_error( $categories ) && ! empty( $categories ) ) {
									?>
									<label for="wpseo_address_term_id" class="screen-reader-text"><?php esc_html_e( 'Category:', 'seo-breeze-local' ); ?></label>
									<select id="wpseo_address_term_id" onchange="WPSEO_Address_Change_Term_Order( this );">
										<option value=""> -- <?php _e( 'Select a category', 'seo-breeze-local' ); ?> --</option>
										<?php
										foreach ( $categories as $category ) {
											?>
											<option value="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></option>
											<?php
										}
										?>
									</select>
								<?php } ?>

								<br />

								<div id="wpseo_address_all_locations_order_wrapper" style="display: none;">
									<label for="wpseo_address_all_locations_orderby"><?php esc_html_e( 'Order by:', 'seo-breeze-local' ); ?></label>
									<select name="wpseo_address_all_locations_orderby" id="wpseo_address_all_locations_orderby">
										<option value="title"><?php _e( 'Alphabetical', 'seo-breeze-local' ); ?></option>
										<option value="date"><?php _e( 'By publish date', 'seo-breeze-local' ); ?></option>
									</select><br>

									<label for="wpseo_address_all_locations_order"><?php esc_html_e( 'Order:', 'seo-breeze-local' ); ?></label>
									<select name="wpseo_address_all_locations_order" id="wpseo_address_all_locations_order">
										<option value="ASC"><?php _e( 'Ascending', 'seo-breeze-local' ); ?></option>
										<option value="DESC"><?php _e( 'Descending', 'seo-breeze-local' ); ?></option>
									</select>
								</div>
							</div>
						<?php } ?>
						<?php if ( ( seobreeze_has_multiple_locations() && ! empty( $locations ) ) || ! seobreeze_has_multiple_locations() ) { ?>
							<div style="padding:15px 15px 0 15px;">
								<label for="wpseo_hide_name"><input type="checkbox" id="wpseo_hide_name" /> <?php _e( 'Hide business name', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_hide_address"><input type="checkbox" id="wpseo_hide_address" /> <?php _e( 'Hide business address', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_oneline"><input type="checkbox" id="wpseo_oneline" /> <?php _e( 'Show address on one line', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_state"><input type="checkbox" id="wpseo_show_state" checked /> <?php _e( 'Show state', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_country"><input type="checkbox" id="wpseo_show_country" checked /> <?php _e( 'Show country', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_phone"><input type="checkbox" id="wpseo_show_phone" checked /> <?php _e( 'Show phone number', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_phone_2nd"><input type="checkbox" id="wpseo_show_phone_2nd" checked /> <?php _e( 'Show 2nd phone number', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_fax"><input type="checkbox" id="wpseo_show_fax" checked /> <?php _e( 'Show fax number', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_email"><input type="checkbox" id="wpseo_show_email" checked /> <?php _e( 'Show email', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_url"><input type="checkbox" id="wpseo_show_url" /> <?php _e( 'Show URL', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_logo"><input type="checkbox" id="wpseo_show_logo" /> <?php _e( 'Show logo', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_vat_id"><input type="checkbox" id="wpseo_show_vat_id" /> <?php _e( 'Show VAT ID', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_tax_id"><input type="checkbox" id="wpseo_show_tax_id" /> <?php _e( 'Show Tax ID', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_coc_id"><input type="checkbox" id="wpseo_show_coc_id" /> <?php _e( 'Show Chamber of Commerce ID', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_price_range"><input type="checkbox" id="wpseo_show_price_range" /> <?php _e( 'Show Price Range', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_show_opening_hours"><input type="checkbox" id="wpseo_show_opening_hours" /> <?php _e( 'Show opening hours', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_hide_closed"><input type="checkbox" id="wpseo_hide_closed" /> <?php _e( 'Hide closed days', 'seo-breeze-local' ); ?>
								</label><br>
							</div>
							<div style="padding:15px 15px 0 15px;">
								<label for="wpseo_comment"><?php esc_html_e( 'Extra comment', 'seo-breeze-local' ); ?></label><br>
								<textarea id="wpseo_comment" rows="5" cols="50"></textarea>
							</div>
							<div style="padding:15px;">
								<input type="button" class="button button-primary" value="<?php _e( 'Insert address', 'seo-breeze-local' ); ?>" onclick="WPSEO_InsertAddress();" />&nbsp;&nbsp;&nbsp;
								<a class="button" href="javascript:" onclick="tb_remove(); return false;"><?php _e( 'Cancel', 'seo-breeze-local' ); ?></a>
							</div>
						<?php } ?>
						<?php if ( seobreeze_has_multiple_locations() && empty( $locations ) ) { ?>
							<p>In order to use this shortcode function, please
								<a href="<?php echo trailingslashit( admin_url() ) . 'edit.php?post_type=wpseo_loctions'; ?>">add one or more locations</a> first.
							</p>
						<?php } ?>
					</div>
				</div>
			</div>
			<div id="wpseo_add_opening_hours" style="display:none;">
				<div class="wrap">
					<div>
						<div style="padding:15px 15px 0 15px;">
							<h2><?php esc_html_e( 'Insert Opening Hours', 'seo-breeze-local' ); ?></h2>
						</div>
						<?php
						$location_args = array(
							'post_type'      => 'seobreeze_locations',
							'posts_per_page' => -1,
							'orderby'        => 'title',
							'order'          => 'ASC',
							'fields'         => 'ids',
							'post_status'    => 'publish',
						);
						if ( current_user_can( 'edit_posts' ) ) {
							$location_args['post_status'] = array(
								'publish',
								'draft',
							);
						}

						$locations = get_posts( $location_args );
						?>
						<?php if ( seobreeze_has_multiple_locations() && ! empty( $locations ) ) { ?>
							<div style="padding:15px 15px 0 15px;">
								<label for="wpseo_oh_location_id" class="screen-reader-text"><?php esc_html_e( 'Location:', 'seo-breeze-local' ); ?></label>
								<select id="wpseo_oh_location_id">
									<option value=""> -- <?php _e( 'Select a location', 'seo-breeze-local' ); ?> --</option>
									<?php
									foreach ( $locations as $location_id ) {
										?>
										<option value="<?php echo $location_id; ?>" <?php selected( $location_id, get_the_ID(), true ); ?>><?php echo get_the_title( $location_id ); ?></option>
										<?php
									}
									?>
								</select> <br />

							</div>
						<?php } ?>
						<?php if ( ( seobreeze_has_multiple_locations() && ! empty( $locations ) ) || ! seobreeze_has_multiple_locations() ) { ?>
							<div style="padding:15px 15px 0 15px;">
								<h2><?php esc_html_e( 'Show Days', 'seo-breeze-local' ); ?></h2>
								<?php
								$days = array(
									'sunday'    => __( 'Sunday', 'seo-breeze-local' ),
									'monday'    => __( 'Monday', 'seo-breeze-local' ),
									'tuesday'   => __( 'Tuesday', 'seo-breeze-local' ),
									'wednesday' => __( 'Wednesday', 'seo-breeze-local' ),
									'thursday'  => __( 'Thursday', 'seo-breeze-local' ),
									'friday'    => __( 'Friday', 'seo-breeze-local' ),
									'saturday'  => __( 'Saturday', 'seo-breeze-local' ),
								);
								foreach ( $days as $key => $day ) {
									/* translators: %s extends to weekdays */
									echo '<label for="wpseo_oh_show_' . $key . '"><input type="checkbox" id="wpseo_oh_show_' . $key . '" value="' . $key . '" checked />' . sprintf( __( 'Show %s', 'seo-breeze-local' ), $day ) . '</label><br>';
								} ?>
							</div>
							<div style="padding:15px 15px 0 15px;">
								<label for="wpseo_oh_hide_closed"><input type="checkbox" id="wpseo_oh_hide_closed" /> <?php _e( 'Hide closed days', 'seo-breeze-local' ); ?>
								</label>
							</div>
							<div style="padding:15px 15px 0 15px;">
								<label for="wpseo_oh_comment"><?php esc_html_e( 'Extra comment', 'seo-breeze-local' ); ?></label><br>
								<textarea id="wpseo_oh_comment" rows="5" cols="50"></textarea>
							</div>
							<div style="padding:15px;">
								<input type="button" class="button button-primary" value="<?php _e( 'Insert opening hours', 'seo-breeze-local' ); ?>" onclick="WPSEO_InsertOpeningHours();" />&nbsp;&nbsp;&nbsp;
								<a class="button" href="javascript:" onclick="tb_remove(); return false;"><?php _e( 'Cancel', 'seo-breeze-local' ); ?></a>
							</div>
						<?php } ?>
						<?php if ( seobreeze_has_multiple_locations() && empty( $locations ) ) { ?>
							<p>In order to use this shortcode function, please
								<a href="<?php echo trailingslashit( admin_url() ) . 'edit.php?post_type=wpseo_loctions'; ?>">add one or more locations</a> first.
							</p>
						<?php } ?>
					</div>
				</div>
			</div>

			<?php if ( seobreeze_has_multiple_locations() ) { ?>
				<div id="wpseo_add_storelocator" style="display:none;">
					<div class="wrap">
						<div>
							<div style="padding:15px 15px 0 15px;">
								<h2><?php _e( 'Insert Store locator', 'seo-breeze-local' ); ?></h2>
							</div>

							<div style="padding:15px 15px 0 15px;">
								<label for="wpseo_sl_show_map"><input type="checkbox" id="wpseo_sl_show_map" checked="checked" /> <?php _e( 'Show Map with the search results', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_scrollable"><input type="checkbox" id="wpseo_sl_scrollable" checked="checked" /> <?php _e( 'Allow scrolling of the map', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_draggable"><input type="checkbox" id="wpseo_sl_draggable" checked="checked" /> <?php _e( 'Allow dragging of the map', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_marker_clustering"><input type="checkbox" id="wpseo_sl_marker_clustering" /> <?php _e( 'Marker clustering', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_filter"><input type="checkbox" id="wpseo_sl_show_filter" /> <?php _e( 'Show filter to narrow down search results', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_radius"><input type="checkbox" id="wpseo_sl_show_radius" /> <?php _e( 'Show radius to limit your search', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_nearest_suggestion"><input type="checkbox" id="wpseo_sl_show_nearest_suggestion" checked="checked" /> <?php _e( 'Show the nearest location, if none are found within radius', 'seo-breeze-local' ); ?>
								</label><br>

								<br>
								<?php /* translators: %s extends to the chosen unit system: km or mi */ ?>
								<label for="wpseo_sl_radius"><?php printf( __( 'Search radius (in %s)', 'seo-breeze-local' ), ( empty( $this->options['unit_system'] ) || $this->options['unit_system'] == 'METRIC' ) ? 'km' : 'mi' ); ?>
									<input type="text" id="wpseo_sl_radius" value="10" /></label><br>
							</div>
							<div style="padding:0 15px 0 15px;">
								<h2><?php esc_html_e( 'Map style', 'seo-breeze-local' ); ?></h2>
								<p><?php esc_html_e( 'Please specify below how the search results should look like.', 'seo-breeze-local' ); ?></p>
								<ul>
									<?php
									$map_styles = array(
										'ROADMAP'   => __( 'Roadmap', 'seo-breeze-local' ),
										'HYBRID'    => __( 'Hybrid', 'seo-breeze-local' ),
										'SATELLITE' => __( 'Satellite', 'seo-breeze-local' ),
										'TERRAIN'   => __( 'Terrain', 'seo-breeze-local' ),
									);

									foreach ( $map_styles as $key => $label ) {
										?>
										<li class="wpseo_map_style" style="display: inline-block; width: 120px; height: 150px; margin-right: 10px;text-align: center;">
											<label for="wpseo_sl_map_style-<?php echo strtolower( $key ); ?>">
												<img src="<?php echo plugins_url( '/images/map-' . strtolower( $key ) . '.png', dirname( __FILE__ ) ); ?>" alt=""><br>
												<?php echo $label; ?><br>
												<input type="radio" name="wpseo_sl_map_style" id="wpseo_sl_map_style-<?php echo strtolower( $key ); ?>" value="<?php echo strtolower( $key ); ?>" <?php checked( 'ROADMAP', $key ); ?>>
											</label>
										</li>
										<?php
									}
									?>
								</ul>
								<label for="wpseo_sl_oneline"><input type="checkbox" id="wpseo_sl_oneline" /> <?php _e( 'Show address on one line', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_state"><input type="checkbox" id="wpseo_sl_show_state" /> <?php _e( 'Show state', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_country"><input type="checkbox" id="wpseo_sl_show_country" /> <?php _e( 'Show country', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_phone"><input type="checkbox" id="wpseo_sl_show_phone" /> <?php _e( 'Show phone number', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_phone_2nd"><input type="checkbox" id="wpseo_sl_show_phone_2nd" /> <?php _e( 'Show 2nd phone number', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_fax"><input type="checkbox" id="wpseo_sl_show_fax" /> <?php _e( 'Show fax number', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_email"><input type="checkbox" id="wpseo_sl_show_email" /> <?php _e( 'Show email', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_url"><input type="checkbox" id="wpseo_sl_show_url" /> <?php _e( 'Show URL', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_opening_hours"><input type="checkbox" id="wpseo_sl_show_opening_hours" /> <?php _e( 'Show opening hours', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_hide_closed"><input type="checkbox" id="wpseo_sl_hide_closed" /> <?php _e( 'Hide closed days', 'seo-breeze-local' ); ?>
								</label><br>
								<label for="wpseo_sl_show_category_filter"><input type="checkbox" id="wpseo_sl_show_category_filter" /> <?php _e( 'Show category filter under the map', 'seo-breeze-local' ); ?>
								</label><br>
							</div>
							<div style="padding:15px;">
								<input type="button" class="button button-primary" value="<?php _e( 'Insert Store locator', 'seo-breeze-local' ); ?>" onclick="WPSEO_InsertStorelocator();" />&nbsp;&nbsp;&nbsp;
								<a class="button" href="javascript:" onclick="tb_remove(); return false;"><?php _e( 'Cancel', 'seo-breeze-local' ); ?></a>
							</div>
						</div>
					</div>
				</div>
				<?php // @codingStandardsIgnoreEnd ?>
			<?php } ?>

			<?php
		}
        
    }
}