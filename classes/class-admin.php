<?php

if ( ! defined( 'ABSPATH' ) ) {
	die;
}
if( ! class_exists( 'Seo_Breeze_Local_Admin' ) ) {
    
    class Seo_Breeze_Local_Admin{
        
        public function __construct() {
            add_action( 'update_option_use_multiple_locations', array( $this, 'update_multiple_locations' ), 10, 2 );
            add_action( 'update_option_locations_slug', array( $this, 'update_locations_slug' ), 10, 2 );
            add_action( 'update_option_locations_taxo_slug', array( $this, 'update_locations_taxo_slug' ), 10, 2 );
            
            add_action( 'admin_init', array( $this, 'flush_rewrite_rules' ) );
        }
        
        public function update_multiple_locations($old_option_value, $new_option_value){
           
            if ( $old_option_value != $old_option_value ) {
                    set_transient( 'seobreeze_local_permalinks_settings_changed', true, 60 );
            }
            
        }
        public function update_locations_slug($old_option_value, $new_option_value){
            $old_option_value = isset( $old_option_value ) ? esc_attr( $old_option_value ) : '';
            $new_option_value = isset( $new_option_value ) ? esc_attr( $new_option_value ) : '';

            if ( ( $old_option_value != $new_option_value ) ) {
                    set_transient( 'seobreeze_local_permalinks_settings_changed', true, 60 );
            }
            
        }
        
        public function update_locations_taxo_slug($old_option_value, $new_option_value){
           
            $old_option_value = isset( $old_option_value) ? esc_attr( $old_option_value ) : '';
            $new_option_value = isset( $new_option_value ) ? esc_attr( $new_option_value ) : '';

            if (  $old_option_value != $new_option_value  ) {
                    set_transient( 'seobreeze_local_permalinks_settings_changed', true, 60 );
            }
            
        }
        
        public function flush_rewrite_rules(){
            if ( get_transient( 'seobreeze_local_permalinks_settings_changed' ) == true ) {
                flush_rewrite_rules();

                delete_transient( 'plugin_settings_have_changed' );
            }
        }
    
    }    
}