<?php
/**
 * Plugin Name: SEOBreeze : Local
 * Version:     1.0
 * Plugin URI:  https://club.wpeka.com/product/seobreeze-local
 * Description: This Local SEO module adds all the needed functionality to get your site ready for Local Search Optimization.
 * Author:      WPeka Team
 * Author URI:  https://club.wpeka.com
 * Depends:     SEO Breeze
 * Text Domain: seo-breeze-local
 * Domain Path: /languages/
 *
 */

if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

require_once ( plugin_dir_path( __DIR__ )  . 'seobreeze/wds-files/wds-webmaster/core.class.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/class-core.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/class-general-settings.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/class-admin.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/class-metaboxes.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'functions.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/class-address-format.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/class-timezone-repository.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/class-storelocator.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/widgets/widget-show-address.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/widgets/widget-show-openinghours.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/widgets/widget-location-open-closed.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/widgets/widget-show-locations-by-category.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/widgets/widget-storelocator-form.php' );
require_once ( plugin_dir_path( __FILE__ ) . 'classes/class-frontend.php' );

/**
 * Throw an error if WordPress SEO is not installed.
 *
 * @since 1.0.1
 */

if ( function_exists( 'seobreeze_enqueue_geocoder' ) ) {
	add_action( 'wp_footer', 'seobreeze_enqueue_geocoder' );
	add_action( 'admin_footer', 'seobreeze_enqueue_geocoder' );
}

if( ! function_exists( 'seo_breeze_plugin_missing_error' )){
function seo_breeze_plugin_missing_error() {
	echo '<div class="error"><p>',
		sprintf(
			/* translators: %1$s resolves to the plugin search for Yoast SEO, %2$s resolves to the closing tag, %3$s resolves to Yoast SEO */
			__( 'Please %1$sinstall &amp; activate %3$s%2$s to allow the Local SEO module to work.', 'seo-breeze-woo' ),
			'<a href="' . esc_url( admin_url( 'plugin-install.php?tab=search&type=term&s=seo+breeze&plugin-search-input=Search+Plugins' ) ) . '">',
			'</a>',
			'SEO Breeze'
		), '</p></div>';
}
}
/**
 * Throw an error if WordPress is out of date.
 *
 * @since 1.0.1
 */
if( ! function_exists( 'seo_breeze_local_wordpress_upgrade_error' )){
function seo_breeze_local_wordpress_upgrade_error() {
	echo '<div class="error"><p>' . __( 'Please upgrade WordPress to the latest version to allow WordPress and the Local SEO module to work properly.', 'seo-breeze-woo' ) . '</p></div>';
}
}
/**
 * Throw an error if WordPress SEO is out of date.
 *
 * @since 1.0.1
 */
if( !function_exists('seo_breeze_local_upgrade_error') ){
function seo_breeze_local_upgrade_error() {
	echo '<div class="error"><p>',
		sprintf(
			/* translators: %1$s resolves to Yoast SEO */
			__( 'Please upgrade the %1$s plugin to the latest version to allow the Local SEO module to work.', 'seo-breeze-woo' ),
			'SEO Breeze'
		), '</p></div>';
}
}

/**
 * Initialize the plugin class, to make sure all the required functionality is loaded, do this after plugins_loaded.
 *
 * @since 1.0
 */
if( !function_exists( 'initialize_seobreeze_local_seo' ) ){
function initialize_seobreeze_local_seo() {
	global $yoast_woo_seo;
	global $wp_version;
        
	load_plugin_textdomain( 'seo-breeze-local', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
        $seo_breeze_version = get_option( 'seobreeze_version');
	if ( ! version_compare( $wp_version, '3.5', '>=' ) ) {
		add_action( 'all_admin_notices', 'seo_breeze_local_wordpress_upgrade_error' );
	} else if ( isset($seo_breeze_version) ) {
		if ( isset($seo_breeze_version) && $seo_breeze_version > '1.6'  ) {
			//$yoast_woo_seo = new SeoBreeze_Local_SEO();
                        $core = new Seo_Breeze_Local_Core();
                        $admin = new Seo_Breeze_Local_Admin();
                        $metabox = new Seo_Breeze_Local_Metaboxes();
                        $frontend = new Seobreeze_Local_Frontend();
                        $local_seo_setting = new seobreze_local_seo_settings();
                        $store_locator = new Seobreeze_Local_Storelocator();
		} else {
			add_action( 'all_admin_notices', 'seo_breeze_local_upgrade_error' );
		}
	} else {
		add_action( 'all_admin_notices', 'seo_breeze_plugin_missing_error' );
	}
}
}


if ( ! function_exists( 'wp_installing' ) ) {
	/**
	 * We need to define wp_installing in WordPress versions older than 4.4
	 *
	 * @return bool
	 */
	function wp_installing() {
		return defined( 'WP_INSTALLING' );
	}
}

if ( ! wp_installing() ) {
	add_action( 'plugins_loaded', 'initialize_seobreeze_local_seo', 20 );

	/*
	 * When the plugin is deactivated and activated again, the license have to be activated. This is mostly the case
	 * during a update of the plugin. To solve this, we hook into the activation process by calling a method that will
	 * activate the license.
	 */

}

if ( ! function_exists( 'seobreeze_local_seo_init_widgets' ) ) {
	/**
	 * Register all widgets used for Local SEO plugin
	 *
	 * @since 3.1
	 */
	function seobreeze_local_seo_init_widgets() {
		$widgets = array(
			'Seobreeze_Show_Address',
			//'Seobreeze_Show_Map',
			'Seobreeze_Show_OpeningHours',
			'Seobreeze_Show_Open_Closed',
		);

		if ( seobreeze_has_multiple_locations() ) {
                    
			$widgets[] = 'Seobreeze_Storelocator_Form';
			$widgets[] = 'Seobreeze_Show_Locations_By_Category';
		} 

		foreach ( $widgets as $widget ) {
			register_widget( $widget );
		}
	}
	add_action( 'widgets_init', 'seobreeze_local_seo_init_widgets' );
}